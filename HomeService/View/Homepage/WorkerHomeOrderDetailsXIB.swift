//
//  WorkerHomeOrderDetailsXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class WorkerHomeOrderDetailsXIB: UICollectionViewCell {

    @IBOutlet weak var imageview_check: UIImageView!
    @IBOutlet weak var Lbl_Customer: UILabel!
    @IBOutlet weak var Lbl_Order_date: UILabel!
    @IBOutlet weak var Lbl_Order_Time: UILabel!
    @IBOutlet weak var View_Background: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
