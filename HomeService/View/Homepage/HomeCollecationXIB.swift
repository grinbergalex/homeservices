//
//  HomeCollecationXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class HomeCollecationXIB: UICollectionViewCell {
    
    
    @IBOutlet weak var Lbl_Gen_Ratings: UILabel!
    @IBOutlet weak var imageview_online: UIImageView!
    @IBOutlet weak var Lbl_Distance: UILabel!
    @IBOutlet weak var Lbl_Online: UILabel!
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var btn_for_order_now: UIButton!
    @IBOutlet weak var main_image_view: UIImageView!
    @IBOutlet weak var lbl_for_worker_name: UILabel!
    @IBOutlet weak var lbl_for_sub_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      
        
        
        main_view.layer.cornerRadius = 4
        main_view.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
        main_view.layer.borderWidth = 1
        main_view.layer.masksToBounds = false
        
//        lbl_for_sub_title.text = ""
//        lbl_for_worker_name.text = ""
        // Initialization code
    }

}
