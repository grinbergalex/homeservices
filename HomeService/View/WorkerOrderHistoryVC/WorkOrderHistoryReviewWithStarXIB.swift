//
//  WorkOrderHistoryReviewWithStarXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Cosmos

class WorkOrderHistoryReviewWithStarXIB: UITableViewCell {

    @IBOutlet weak var Lbl_Review: UILabel!
    @IBOutlet weak var View_Cosmos: CosmosView!
    @IBOutlet weak var Lbl_Date: UILabel!
    @IBOutlet weak var Lbl_Description: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
