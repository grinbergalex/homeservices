//
//  WorkerOrderHistoryXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class WorkerOrderHistoryXIB: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- VARIABLES
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Tableview_Order_History: UITableView!
    //MARK:- VIEWDIDLOAD
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Tableview_Order_History.register(WorkerOrderHistoryHeaderXIB.self, forCellReuseIdentifier: "WorkerOrderHistoryHeaderXIB")
        Tableview_Order_History.register(UINib(nibName: "WorkerOrderHistoryHeaderXIB", bundle: nil), forCellReuseIdentifier: "WorkerOrderHistoryHeaderXIB")
        
        Tableview_Order_History.register(WorkOrderHistoryReviewWithoutStarXIB.self, forCellReuseIdentifier: "WorkOrderHistoryReviewWithoutStarXIB")
        Tableview_Order_History.register(UINib(nibName: "WorkOrderHistoryReviewWithoutStarXIB", bundle: nil), forCellReuseIdentifier: "WorkOrderHistoryReviewWithoutStarXIB")
        
        Tableview_Order_History.register(WorkOrderHistoryReviewWithStarXIB.self, forCellReuseIdentifier: "WorkOrderHistoryReviewWithStarXIB")
        Tableview_Order_History.register(UINib(nibName: "WorkOrderHistoryReviewWithStarXIB", bundle: nil), forCellReuseIdentifier: "WorkOrderHistoryReviewWithStarXIB")
        
        
        

        // Do any additional setup after loading the view.
    }


    //MARK:- ALL FUNCTIONS
    
    
    @objc func NAVIGAIGATIONMAP(sender:UIButton)
    {
        let Push = NavigationScreenXIB()
        self.navigationController?.pushViewController(Push, animated: true)
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- TABLEVIEW METOHDS
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       if section == 0
       {
            return 1
        }
        else if section == 1
       {
            return 5
        }
        else
       {
        return 1
        }
        
       
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cell:WorkerOrderHistoryHeaderXIB = Tableview_Order_History.dequeueReusableCell(withIdentifier: "WorkerOrderHistoryHeaderXIB", for: indexPath) as! WorkerOrderHistoryHeaderXIB
            
            cell.Btn_Title_Navigation.addTarget(self,action:#selector(self.NAVIGAIGATIONMAP(sender:)), for: .touchUpInside)
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell:WorkOrderHistoryReviewWithoutStarXIB = Tableview_Order_History.dequeueReusableCell(withIdentifier: "WorkOrderHistoryReviewWithoutStarXIB", for: indexPath) as! WorkOrderHistoryReviewWithoutStarXIB
            
            return cell
            
            
        }
        else
        {
            let cell:WorkOrderHistoryReviewWithStarXIB = Tableview_Order_History.dequeueReusableCell(withIdentifier: "WorkOrderHistoryReviewWithStarXIB", for: indexPath) as! WorkOrderHistoryReviewWithStarXIB
            
            return cell
            
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       if indexPath.section == 0
       {
        return 460
        }
        else if indexPath.section == 1
       {
        return 70
        }
        else
       {
            return 125
        }
    }
    
}
