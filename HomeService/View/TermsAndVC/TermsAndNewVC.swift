//
//  TermsAndNewVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class TermsAndNewVC: UIViewController {

    @IBOutlet weak var Webview_Request: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        GetTermsAndConditions()
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        ApiUtillity.sharedInstance.dismissSVProgressHUD()
        
    }
    
    //MARK:- All FUNCTIONS
    
    func GetTermsAndConditions()
    {
        let TempUrl = ApiUtillity.sharedInstance.API(Join: "user/content/terms_condition")
        let url = URL (string: TempUrl)
        let requestObj = URLRequest(url: url!)
        Webview_Request.loadRequest(requestObj)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
