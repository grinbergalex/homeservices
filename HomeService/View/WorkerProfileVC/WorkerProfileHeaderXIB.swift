//
//  WorkerProfileHeaderXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire
import ImageSlideshow
import Cosmos

class WorkerProfileHeaderXIB: UITableViewCell {

    @IBOutlet weak var Btn_Title_Slide_Show: UIButton!
    @IBOutlet weak var View_Ratings: CosmosView!
    @IBOutlet weak var Lbl_Overall_Ratings: UILabel!
    @IBOutlet weak var Lbl_Lets_Work_With: UILabel!
    @IBOutlet weak var Lbl_Customer_Review_About: UILabel!
    @IBOutlet weak var Lbl_Date_Time: UILabel!
    @IBOutlet weak var Lbl_Address: UILabel!
    @IBOutlet weak var Lbl_Proficiency: UILabel!
    @IBOutlet weak var Lbl_Name: UILabel!
    @IBOutlet weak var imageview_Profile: UIImageView!
    @IBOutlet weak var Btn_Title_Go: UIButton!
    @IBOutlet weak var Btn_Title_Date_Select: UIButton!
    @IBOutlet weak var View_Date: UIView!
    @IBOutlet weak var View_Imageview: UIView!
    @IBOutlet weak var Slide_Show: ImageSlideshow!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
