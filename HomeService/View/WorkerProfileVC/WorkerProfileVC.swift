//
//  WorkerProfileVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

import Alamofire
import ImageSlideshow
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import Kingfisher
import Alamofire
import AlamofireImage
import ImageSlideshow
import SafariServices
import GooglePlaces
import GoogleMaps
import CoreLocation
import Alamofire
import Kingfisher
import Cosmos
import SKPhotoBrowser
import SDWebImage
import CoreLocation

import CoreLocation
import MapKit





class WorkerProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,GMSPlacePickerViewControllerDelegate,GMSMapViewDelegate,SKPhotoBrowserDelegate,CLLocationManagerDelegate{
    
    
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Lbl_Header: UILabel!
    
    @IBOutlet weak var Lbl_Distance: UILabel!
    @IBOutlet weak var View_Google_Address: UIView!
    @IBOutlet weak var Txt_Specific_Address: UITextField!
    @IBOutlet weak var Txt_Google_Address: UITextField!
    @IBOutlet weak var Tableview_Worker: UITableView!
    @IBOutlet weak var View_Small_Address: UIView!
    
    @IBOutlet weak var View_Provide_Adress: UIView!
   
    @IBOutlet weak var Btn_Title_Contine: UIButton!
    
    //MARK:- VARIRABLES
      var slider_photo = NSArray()
    var Review_Array = NSArray()
    var WorkerProfileDict = NSMutableDictionary()
    var SelectedDate = String()
    var address = String()
    var specific_address = String()
    var apiToken = String()
    var Token =  String()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var currentlatitude =  Double()
    var currentlogitude =  Double()
    var currentLC = CLLocationCoordinate2D()
    var latitude = String()
    var logitude = String()
    
   
    
    //MARK:- VIEW DID LOAD
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Tableview_Worker.register(WorkerProfileHeaderXIB.self, forCellReuseIdentifier: "WorkerProfileHeaderXIB")
        Tableview_Worker.register(UINib(nibName: "WorkerProfileHeaderXIB", bundle: nil), forCellReuseIdentifier: "WorkerProfileHeaderXIB")
        
        Tableview_Worker.register(WorkerReviewCell.self, forCellReuseIdentifier: "WorkerReviewCell")
        Tableview_Worker.register(UINib(nibName: "WorkerReviewCell", bundle: nil), forCellReuseIdentifier: "WorkerReviewCell")
        
        Tableview_Worker.register(NoRatingsXIB.self, forCellReuseIdentifier: "NoRatingsXIB")
        Tableview_Worker.register(UINib(nibName: "NoRatingsXIB", bundle: nil), forCellReuseIdentifier: "NoRatingsXIB")
        
        self.Tableview_Worker.estimatedRowHeight = 80.0
        self.Tableview_Worker.rowHeight = UITableView.automaticDimension
        
        //NoRatingsXIB
        
        View_Provide_Adress.isHidden = true
        
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: Txt_Specific_Address, cornurRadius: 0, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: View_Google_Address, cornurRadius: 0, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
        
        Txt_Specific_Address.setLeftPaddingPoints(10)
        Txt_Specific_Address.setRightPaddingPoints(10)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        Txt_Google_Address.addGestureRecognizer(tap)
        
        view.isUserInteractionEnabled = true
        
       
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:mm a"
        //   print("Current date: \(formatter.string(from: Date()))"
      
        SelectedDate = formatter.string(from: Date())
      
        
        View_Small_Address.roundCorners([.topLeft , .topRight], radius:20)
        
      //  ApiUtillity.sharedInstance.setCornurRadius(obj: self.Btn_Title_Contine, cornurRadius: 15, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 1)
        
    
        View_Small_Address.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
        View_Small_Address.layer.shadowOpacity = 4
        View_Small_Address.layer.shadowOffset = CGSize.zero
        View_Small_Address.layer.shadowRadius = 2
        View_Small_Address.clipsToBounds = true
        View_Small_Address.layer.masksToBounds = false
        
        
        Btn_Title_Contine.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
        Btn_Title_Contine.layer.shadowOpacity = 4
        Btn_Title_Contine.layer.shadowOffset = CGSize.zero
        Btn_Title_Contine.layer.shadowRadius = 4
        Btn_Title_Contine.clipsToBounds = true
        Btn_Title_Contine.layer.masksToBounds = false
        Btn_Title_Contine.layer.borderWidth = 1.0
        Btn_Title_Contine.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
        Btn_Title_Contine.layer.cornerRadius = 10
        
        
        
        
        
        
        print(self.WorkerProfileDict)
        
      if self.WorkerProfileDict.count == 0
      {
        
        }
        else
      {
            self.Tableview_Worker.reloadData()
        }


        // Do any additional setup after loading the view.
    }


    // MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Btn_Handler_Bg_Black_Button(_ sender: Any)
    {
          View_Provide_Adress.isHidden = true
        
        self.address = String()
        self.specific_address = String()
        self.Txt_Google_Address.text = ""
        self.Txt_Specific_Address.text  = ""
        
    }
    @IBAction func Btn_Handler_Continue(_ sender: Any)
    {
        self.address = Txt_Google_Address.text!
        self.specific_address = Txt_Specific_Address.text!
        
        if self.address.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Select Google Address", ForNavigation: self.navigationController!)
            return
        }
        
        if self.specific_address.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Specific Address", ForNavigation: self.navigationController!)
            return
        }
        
        if self.SelectedDate.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Select Date", ForNavigation: self.navigationController!)
            return
            
        }
        
        PlaceOrder()
        
       

    }
    @IBAction func Btn_Handler_Select_Location(_ sender: Any)
    {
        
    }
    @IBAction func Btn_Handler_Google_Map_Address(_ sender: Any)
    {
      //if segue.identifier == "LocationPicker"
        
//        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        

        
        
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
 
        
    }
    
    // MARK:- ALL FUNCTIONS
    
    
    @objc func AddAdress(sender:UIButton)
    {
        
        View_Provide_Adress.isHidden = false

    }
    
    @objc func DateSelect(sender:UIButton)
    {
        let min = Date()
        // let max = min.addingTimeInterval(31536000) // 1 year
        DPPickerManager.shared.showPicker(title: "Select Date And Time", selected: Date(), min: min, max: nil) { (date, cancel) in
            if !cancel
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
                let SelectedDate1 = dateFormatter.string(from: date!)
                
                print(SelectedDate1)
                
                
                let date2 = Date()
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "dd/MM/yyyy hh:mm a"
                print(date2)
                let result = formatter2.string(from: date2)
                print(result)
                
                if SelectedDate1 == result
                {
               
                    self.SelectedDate = SelectedDate1
                    self.Tableview_Worker.reloadData()
                    print("EQual")
                }
                else
                {
                    self.SelectedDate = SelectedDate1
                    self.Tableview_Worker.reloadData()
                    print("Not EQual")
                    
                }
                
               
                
                
               
                
                debugPrint(date as Any)
            }
            else
            
            {
                print("CANCEL")
            }
        }
    }
    
    func fadedown(inAnimation aView: UIView)
    {
        let transition = CATransition()
        //        transition.type = kCATransitionFade
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        transition.duration = 0.6 as? CFTimeInterval ?? CFTimeInterval()
        transition.delegate = self as! CAAnimationDelegate
        aView.layer.add(transition, forKey: nil)
    }
    
    func fadeUP(inAnimation aView: UIView)
    {
        let transition = CATransition()
        //        transition.type = kCATransitionFade
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        transition.duration = 0.5 as? CFTimeInterval ?? CFTimeInterval()
        transition.delegate = self as! CAAnimationDelegate
        aView.layer.add(transition, forKey: nil)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
        
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        print(place)
         viewController.dismiss(animated: true, completion: nil)
        
        if place.formattedAddress == nil
        {
            
        }
        else
        {
            let String = place.formattedAddress
            print(String)
            self.Txt_Google_Address.text = "\(String!)"
            self.address = "\(String)"
            
            self.currentlatitude = place.coordinate.latitude
            self.currentlogitude = place.coordinate.longitude
            
        }
        
      
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController)
    {
        self.address = String()
        self.currentlatitude = Double()
        self.currentlogitude = Double()
        
         viewController.dismiss(animated: true, completion: nil)
    }
    
    @objc func Slideshow(sender:UIButton)
    {
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(0)
        browser.delegate = self
        
        present(browser, animated: true, completion: nil)
    }
    
    @objc func PlaceOrder()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            apiToken = "Bearer \(Token)"
            
            
            let worker_id = self.WorkerProfileDict.value(forKey: "id") as! String
            
              let headers = ["Vauthtoken":apiToken]
            
            let params = ["date_time":self.SelectedDate,"worker_id":worker_id,"address":self.address,"specific_address":self.specific_address,"latitude":self.currentlatitude,"longitude":self.currentlogitude] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/place_order"), method: .post, parameters: params, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                       
                        let Successdic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let success = Successdic.value(forKey: "success") as? String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: success!)
                       
                        
                         self.View_Provide_Adress.isHidden = true
                        
                        self.address = String()
                        self.specific_address = String()
                        self.Txt_Google_Address.text = ""
                        self.Txt_Specific_Address.text  = ""
                        
                        let Push = UserChatDetailVC()
                        let MainData = dict.value(forKey: "data") as! NSDictionary
                        Push.Order_id = MainData.value(forKey: "order_id") as! String
                        Push.name = MainData.value(forKey: "worker_name") as! String
                        Push.thread_id = MainData.value(forKey: "thread_id") as! String
                        self.navigationController?.pushViewController(Push, animated: true)
                        
                        
                        
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "email") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    
   
    
    
    //MARK:- TABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            
            if self.WorkerProfileDict.count == 0
            {
                return 0
            }
            else
            {
                
                let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
                
                if ratting_review.count == 0
                {
                    return 1
                }
                else
                    
                {
                    self.Review_Array = ratting_review
                    
                    return Review_Array.count
                }
                
                //ratting_review
            }
            
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if indexPath.section == 0
        {
            let cell:WorkerProfileHeaderXIB = Tableview_Worker.dequeueReusableCell(withIdentifier: "WorkerProfileHeaderXIB", for: indexPath) as! WorkerProfileHeaderXIB
            
            if self.WorkerProfileDict.count == 0
            {
                
            }
            else
            {
              
                
                let TempArray = self.WorkerProfileDict.value(forKey: "worker_images") as! NSArray
                
                self.slider_photo = TempArray.value(forKey: "image") as! NSArray
                
                
                var alamofireSource = [AlamofireSource]()
                for url in slider_photo
                {
                    alamofireSource.append(AlamofireSource(urlString: url as! String)!)
                }
                cell.Slide_Show.backgroundColor = UIColor.white
                cell.Slide_Show.slideshowInterval = 5.0
                cell.Slide_Show.pageControlPosition = PageControlPosition.insideScrollView
                cell.Slide_Show.pageControl.currentPageIndicatorTintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4")
                
                cell.Slide_Show.pageControl.sizeToFit()
                cell.Slide_Show.pageControl.pageIndicatorTintColor = UIColor.white
                cell.Slide_Show.contentScaleMode = UIView.ContentMode.scaleAspectFill
                cell.Slide_Show.clipsToBounds = true
                cell.Slide_Show.activityIndicator = DefaultActivityIndicator()
                cell.Slide_Show.currentPageChanged = { page in
                    
                }
                
                ApiUtillity.sharedInstance.setCornurRadius(obj: cell.View_Date, cornurRadius: 5, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
                
                
                
                if alamofireSource.count == 0
                {
                    //  cell.View_ImageSlideShow.setImageInputs(localSource)
                    
                }
                else
                {
                    cell.Slide_Show.setImageInputs(alamofireSource)
                }
                
                cell.View_Imageview.layer.cornerRadius = 56.0
                cell.View_Imageview.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "D3D3D3").cgColor
                cell.View_Imageview.layer.shadowOpacity = 4
                cell.View_Imageview.layer.shadowOffset = CGSize.zero
                cell.View_Imageview.layer.shadowRadius = 4
                cell.View_Imageview.clipsToBounds = true
                cell.View_Imageview.layer.masksToBounds = false
              
 
 
                
                
                let distance = self.WorkerProfileDict.value(forKey: "distance") as! String
                let gen_rattings = self.WorkerProfileDict.value(forKey: "gen_rattings") as! String
                let name = self.WorkerProfileDict.value(forKey: "name") as! String
                let proficiency = self.WorkerProfileDict.value(forKey: "proficiency") as! String
                let profile_image = self.WorkerProfileDict.value(forKey: "profile_image") as! String
                let working_city = self.WorkerProfileDict.value(forKey: "working_city") as! String
               
                let myDouble = Double(distance)
                print(myDouble)
                let text = String(format: "%.0f", arguments: [myDouble!])
                print(text)
                
                cell.Lbl_Name.text = "\(name)"
                cell.Lbl_Overall_Ratings.text = "\(gen_rattings)" + "/5"
                cell.View_Ratings.rating = Double(gen_rattings)!
                cell.Lbl_Proficiency.text = "\(proficiency)"
                cell.imageview_Profile.kf.indicatorType = .activity
                cell.imageview_Profile.kf.setImage(with: URL(string: profile_image ))
                cell.Lbl_Address.text = "\(working_city)"
                cell.Lbl_Lets_Work_With.text = "let's Work with " + "\(name)" + " !!"
                cell.Lbl_Customer_Review_About.text = "Customer Review's about " + "\(name)" + ":"
                
                
                self.Lbl_Header.text = "\(name)"
                self.Lbl_Distance.text = "\(text)" + " KM Away from you"
                    
            
                cell.Lbl_Date_Time.text = "\(SelectedDate)"
                cell.Btn_Title_Go.addTarget(self,action:#selector(self.AddAdress(sender:)), for: .touchUpInside)
                cell.Btn_Title_Date_Select.addTarget(self,action:#selector(self.DateSelect(sender:)), for: .touchUpInside)
                
                
                cell.Btn_Title_Slide_Show.addTarget(self, action: #selector(self.Slideshow(sender:)), for: .touchUpInside)
                
                
                
            }
            
           
            
            
            return cell
            
        }
        else
        {
            
            if self.WorkerProfileDict.count == 0
            {
                return UITableViewCell()
            }
            else
            {
                //ratting_review
                
                let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
                
                if ratting_review.count == 0
                {
                    let cell:NoRatingsXIB = Tableview_Worker.dequeueReusableCell(withIdentifier: "NoRatingsXIB", for: indexPath) as! NoRatingsXIB
                    
                    cell.imageview_Dot.isHidden = true
                    
                  return cell
                }
                else
                {
                    let cell:WorkerReviewCell = Tableview_Worker.dequeueReusableCell(withIdentifier: "WorkerReviewCell", for: indexPath) as! WorkerReviewCell
                    
                    cell.View_Background.layer.cornerRadius = 10
                    cell.View_Background.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "D3D3D3").cgColor
                    cell.View_Background.layer.shadowOpacity = 4
                    cell.View_Background.layer.shadowOffset = CGSize.zero
                    cell.View_Background.layer.shadowRadius = 4
                    cell.View_Background.clipsToBounds = true
                    cell.View_Background.layer.masksToBounds = false
                    
                    
                    print(self.Review_Array.object(at: indexPath.row)as! NSDictionary)
                    
                    let Ratings = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "ratting") as! String
                    
                    let ratting_date_time = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "ratting_date_time") as! String
                    
                    let review = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "review") as! String
                    
                    let username = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "username") as! String
                    
                
                    
                    //2018-10-17 12:49:49
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let date = dateFormatter.date (from: ratting_date_time)
                    
                    let String1 = date!.timeAgoSinceNow
                    print(String1)
                    
                    
                  cell.Lbl_Time.text = "\(String1)"
                    cell.Lbl_Review.text = "\(review)"
                    cell.Lbl_User_Name.text = "\(username)"
                    cell.Lbl_Ratings.text = "\(Ratings)" + "/5"
                    
                    
                    
                    
                //    ratting_date_time.timeAgoSinceDate()

                    
                 
                    
                    return cell

                }
                
            }
            
         
            
        }
        
        
       
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 500
        }
        else
        {
            let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
            
            if ratting_review.count == 0
            {
                
                return 50
            }
            else
            {
               
                
                return UITableView.automaticDimension
                
            }
            
        }
    }
    
   

}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension Date {
    
    var timeAgoSinceNow: String {
        return getTimeAgoSinceNow()
    }
    
    private func getTimeAgoSinceNow() -> String {
        
        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " year ago" : "\(interval)" + " years ago"
        }
        
        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " month ago" : "\(interval)" + " months ago"
        }
        
        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " day ago" : "\(interval)" + " days ago"
        }
        
        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " hour ago" : "\(interval)" + " hours ago"
        }
        
        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " minute ago" : "\(interval)" + " minutes ago"
        }
        
        return "a moment ago"
    }
}


private extension WorkerProfileVC {
    func createWebPhotos() -> [SKPhotoProtocol]
    {
        
        let FinalimageArray = self.WorkerProfileDict.value(forKey: "worker_images") as! NSArray
        
        
        print(FinalimageArray)
        
        
        
        return (0..<FinalimageArray.count).map { (i: Int) -> SKPhotoProtocol in
            
            let String =  (FinalimageArray.object(at: i)as! NSDictionary).value(forKey: "image") as! String
            
            
            let photo = SKPhoto.photoWithImageURL(String )
            
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
}

class CustomImageCache: SKImageCacheable {
    var cache: SDImageCache
    
    init() {
        let cache = SDImageCache(namespace: "com.suzuki.custom.cache")
        self.cache = cache
    }
    
    func imageForKey(_ key: String) -> UIImage? {
        guard let image = cache.imageFromDiskCache(forKey: key) else { return nil }
        
        return image
    }
    
    func setImage(_ image: UIImage, forKey key: String) {
        cache.store(image, forKey: key)
    }
    
    func removeImageForKey(_ key: String) {}
    
    func removeAllImages() {}
    
}
