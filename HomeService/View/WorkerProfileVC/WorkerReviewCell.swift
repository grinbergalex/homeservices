//
//  WorkerReviewCell.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Cosmos

class WorkerReviewCell: UITableViewCell {

    @IBOutlet weak var Lbl_Time: UILabel!
    @IBOutlet weak var Lbl_Review: UILabel!
    @IBOutlet weak var Lbl_Ratings: UILabel!
    @IBOutlet weak var Lbl_User_Name: UILabel!
    @IBOutlet weak var View_Background: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
