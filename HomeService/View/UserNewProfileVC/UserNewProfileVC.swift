//
//  UserNewProfileVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire
import ImageSlideshow
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import Kingfisher
import Alamofire
import AlamofireImage
import ImageSlideshow
import SafariServices
import GooglePlaces
import GoogleMaps
import CoreLocation
import Alamofire
import Kingfisher
import Cosmos
import SKPhotoBrowser
import SDWebImage
import CoreLocation

import CoreLocation
import MapKit
import CoreLocation
import MapKit



class UserNewProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,GMSPlacePickerViewControllerDelegate,GMSMapViewDelegate,SKPhotoBrowserDelegate,CLLocationManagerDelegate{
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Txt_Specific_Address: UITextField!
    @IBOutlet weak var Txt_Google_Address: UITextField!
    @IBOutlet weak var View_Google_Address: UIView!
    @IBOutlet weak var View_Provide_Address: UIView!
    @IBOutlet weak var Lbl_Header: UILabel!
    @IBOutlet weak var Tableview_Worker: UITableView!
    
    @IBOutlet weak var Btn_Title_Contine: UIButton!
    @IBOutlet weak var Lbl_Distance: UILabel!
    
    @IBOutlet weak var locationNameTextField: UITextField!
    @IBOutlet weak var locationAddressTextField: UITextField!
    
    
    //MARK:- VARIRABLES
    var slider_photo = NSArray()
    var Review_Array = NSArray()
    var WorkerProfileDict = NSMutableDictionary()
    var SelectedDate = String()
    var address = String()
    var specific_address = String()
    var apiToken = String()
    var Token =  String()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var currentlatitude =  Double()
    var currentlogitude =  Double()
    var currentLC = CLLocationCoordinate2D()
    var latitude = String()
    var logitude = String()
    
    var locManager1 = CLLocationManager()

    var currentLocation1: CLLocation!
    var currentlatitude1 =  Double()
    var currentlogitude1 =  Double()
    var currentLC1 = CLLocationCoordinate2D()
    var latitude1 = String()
    var logitude1 = String()
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        
        return _locationManager
    }()
    
    
    var historyLocationList: [LocationItem] {
        get {
            if let locationDataList = UserDefaults.standard.array(forKey: "HistoryLocationList") as? [Data] {
                // Decode NSData into LocationItem object.
                return locationDataList.map({ NSKeyedUnarchiver.unarchiveObject(with: $0) as! LocationItem })
            } else {
                return []
            }
        }
        set {
            // Encode LocationItem object.
            let locationDataList = newValue.map({ NSKeyedArchiver.archivedData(withRootObject: $0) })
            UserDefaults.standard.set(locationDataList, forKey: "HistoryLocationList")
        }
    }
    
    
   

    
    
  
    //MARK:- VIEW DID LOAD

    override func viewDidLoad() {
        super.viewDidLoad()
      

        
        Tableview_Worker.register(WorkerProfileHeaderXIB.self, forCellReuseIdentifier: "WorkerProfileHeaderXIB")
        Tableview_Worker.register(UINib(nibName: "WorkerProfileHeaderXIB", bundle: nil), forCellReuseIdentifier: "WorkerProfileHeaderXIB")
        
        Tableview_Worker.register(WorkerReviewCell.self, forCellReuseIdentifier: "WorkerReviewCell")
        Tableview_Worker.register(UINib(nibName: "WorkerReviewCell", bundle: nil), forCellReuseIdentifier: "WorkerReviewCell")
        
        self.navigationController?.navigationBar.isHidden = true

        
        Tableview_Worker.register(NoRatingsXIB.self, forCellReuseIdentifier: "NoRatingsXIB")
        Tableview_Worker.register(UINib(nibName: "NoRatingsXIB", bundle: nil), forCellReuseIdentifier: "NoRatingsXIB")
        
        self.Tableview_Worker.estimatedRowHeight = 80.0
        self.Tableview_Worker.rowHeight = UITableView.automaticDimension
        
        self.navigationController?.navigationBar.isHidden = true
        
        View_Provide_Address.isHidden = true
        
        
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: Txt_Specific_Address, cornurRadius: 0, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: View_Google_Address, cornurRadius: 0, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
        
        Txt_Specific_Address.setLeftPaddingPoints(10)
        Txt_Specific_Address.setRightPaddingPoints(10)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        Txt_Google_Address.addGestureRecognizer(tap)
        
        view.isUserInteractionEnabled = true
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:mm a"
        //   print("Current date: \(formatter.string(from: Date()))"
        
        SelectedDate = formatter.string(from: Date())
        
        self.navigationController?.navigationBar.backgroundColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4")
        
       
        
        self.navigationController?.navigationBar.tintColor = UIColor.black

        
        Btn_Title_Contine.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
        Btn_Title_Contine.layer.shadowOpacity = 4
        Btn_Title_Contine.layer.shadowOffset = CGSize.zero
        Btn_Title_Contine.layer.shadowRadius = 4
        Btn_Title_Contine.clipsToBounds = true
        Btn_Title_Contine.layer.masksToBounds = false
        Btn_Title_Contine.layer.borderWidth = 1.0
        Btn_Title_Contine.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
        Btn_Title_Contine.layer.cornerRadius = 10
        
        
        
        self.locManager1.delegate = self
        self.locManager1.startUpdatingLocation()
        
        
        
        self.currentLC1 = CLLocationCoordinate2DMake(23.032331,72.56208)
        print(self.currentLC1)
        
        locManager1.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways)
        {
            currentLocation1 = locManager.location
            print(currentLocation1)
            if currentLocation1 == nil {
                currentlatitude1 = 23.032331
                currentlogitude1 = 72.56208
            }
            else
            {
                currentlatitude1 = (currentLocation1.coordinate.latitude)
                currentlogitude1 = (currentLocation1.coordinate.longitude)
            }
            
        }
        
        
        
        
        
        
        currentLC1 = CLLocationCoordinate2DMake(currentlatitude1,currentlogitude1)
        
        
        print(self.WorkerProfileDict)
        
        if self.WorkerProfileDict.count == 0
        {
            
        }
        else
        {
            
            let distance = self.WorkerProfileDict.value(forKey: "distance") as! String
            let myDouble = Double(distance)
            print(myDouble)
            let text = String(format: "%.0f", arguments: [myDouble!])
            print(text)
            
            let name = self.WorkerProfileDict.value(forKey: "name") as! String
            let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
            label.backgroundColor = .clear
            label.numberOfLines = 2
            label.font = UIFont.boldSystemFont(ofSize: 16.0)
            label.textAlignment = .center
            label.textColor = .black
            label.text = "\(name)" + "\n" + "\(text)" + " KM Away from you"
            self.navigationItem.titleView = label
            
            UserDefaults.standard.set("", forKey: "address")
            self.Tableview_Worker.reloadData()
        }


        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        
        
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
      //  locationNameTextField.text = nil
      //  locationAddressTextField.text = nil
    }
    

    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Btn_Handler_Black_Bg_Address(_ sender: Any)
    {
        View_Provide_Address.isHidden = true
        
        self.address = String()
        self.specific_address = String()
        self.Txt_Google_Address.text = ""
        self.Txt_Specific_Address.text  = ""
    }
    @IBAction func Btn_Handler_Select_Google_Address(_ sender: Any)
    {
       
        let customLocationPicker = CustomLocationPicker()
        customLocationPicker.viewController = self
        let navigationController = UINavigationController(rootViewController: customLocationPicker)
        present(navigationController, animated: true, completion: nil)
    
    
    }
    
    
    
    
    
    
    @IBAction func Btn_Handler_Continue(_ sender: Any)
    {
        self.address = Txt_Google_Address.text!
        self.specific_address = Txt_Specific_Address.text!
        
        if self.address.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Select  Address", ForNavigation: self.navigationController!)
            return
        }
        
        if self.specific_address.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Specific Address", ForNavigation: self.navigationController!)
            return
        }
        
        if self.SelectedDate.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Select Date", ForNavigation: self.navigationController!)
            return
            
        }
        
        PlaceOrder()
    }
    //MARK:- ALL FUNCTIONS
    
    /*
 
     let locationPicker = LocationPickerViewController()
     
     // you can optionally set initial location
     let location = CLLocation(latitude: 35, longitude: 35)
     let initialLocation = Location(name: "My home", location: location)
     locationPicker.location = initialLocation
     
     // button placed on right bottom corner
     locationPicker.showCurrentLocationButton = true // default: true
     
     // default: navigation bar's `barTintColor` or `.whiteColor()`
     locationPicker.currentLocationButtonBackground = .blueColor()
     
     // ignored if initial location is given, shows that location instead
     locationPicker.showCurrentLocationInitially = true // default: true
     
     locationPicker.mapType = .Standard // default: .Hybrid
     
     // for searching, see `MKLocalSearchRequest`'s `region` property
     locationPicker.useCurrentLocationAsHint = true // default: false
     
     locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
     
     locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
     
     // optional region distance to be used for creation region when user selects place from search results
     locationPicker.resultRegionDistance = 500 // default: 600
     
     locationPicker.completion = { location in
     // do some awesome stuff with location
     }
     
     navigationController?.pushViewController(locationPicker, animated: true)
 
   */
   
    @objc func AddAdress(sender:UIButton)
    {
        
        View_Provide_Address.isHidden = false
        
    }
    
    @objc func DateSelect(sender:UIButton)
    {
        let min = Date()
        // let max = min.addingTimeInterval(31536000) // 1 year
        DPPickerManager.shared.showPicker(title: "Select Date And Time", selected: Date(), min: min, max: nil) { (date, cancel) in
            if !cancel
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
                let SelectedDate1 = dateFormatter.string(from: date!)
                
                print(SelectedDate1)
                
                
                let date2 = Date()
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "dd/MM/yyyy hh:mm a"
                print(date2)
                let result = formatter2.string(from: date2)
                print(result)
                
                if SelectedDate1 == result
                {
                    
                    self.SelectedDate = SelectedDate1
                    self.Tableview_Worker.reloadData()
                    print("EQual")
                }
                else
                {
                    self.SelectedDate = SelectedDate1
                    self.Tableview_Worker.reloadData()
                    print("Not EQual")
                    
                }
                
                
                
                
                
                
                debugPrint(date as Any)
            }
            else
                
            {
                print("CANCEL")
            }
        }
    }
    
    func fadedown(inAnimation aView: UIView)
    {
        let transition = CATransition()
        //        transition.type = kCATransitionFade
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        transition.duration = 0.6 as? CFTimeInterval ?? CFTimeInterval()
        transition.delegate = self as! CAAnimationDelegate
        aView.layer.add(transition, forKey: nil)
    }
    
    func fadeUP(inAnimation aView: UIView)
    {
        let transition = CATransition()
        //        transition.type = kCATransitionFade
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        transition.duration = 0.5 as? CFTimeInterval ?? CFTimeInterval()
        transition.delegate = self as! CAAnimationDelegate
        aView.layer.add(transition, forKey: nil)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        let customLocationPicker = CustomLocationPicker()
        customLocationPicker.viewController = self
        let navigationController = UINavigationController(rootViewController: customLocationPicker)
        present(navigationController, animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        print(place)
        viewController.dismiss(animated: true, completion: nil)
        
        if place.formattedAddress == nil
        {
            
        }
        else
        {
            let String = place.formattedAddress
            print(String)
            self.Txt_Google_Address.text = "\(String!)"
            self.address = "\(String)"
            
            self.currentlatitude = place.coordinate.latitude
            self.currentlogitude = place.coordinate.longitude
            
        }
        
        
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController)
    {
        self.address = String()
        self.currentlatitude = Double()
        self.currentlogitude = Double()
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    @objc func Slideshow(sender:UIButton)
    {
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(0)
        browser.delegate = self
        
        present(browser, animated: true, completion: nil)
    }
    
    
   
    
    
    @objc func PlaceOrder()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            apiToken = "Bearer \(Token)"
            
            
            let worker_id = self.WorkerProfileDict.value(forKey: "id") as! String
            
            let headers = ["Vauthtoken":apiToken]
            
            let params = ["date_time":self.SelectedDate,"worker_id":worker_id,"address":self.address,"specific_address":self.specific_address,"latitude":self.currentlatitude,"longitude":self.currentlogitude] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/place_order"), method: .post, parameters: params, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        
                        let Successdic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let success = Successdic.value(forKey: "success") as? String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: success!)
                        
                        
                        self.View_Provide_Address.isHidden = true
                        
                        self.address = String()
                        self.specific_address = String()
                        self.Txt_Google_Address.text = ""
                        self.Txt_Specific_Address.text  = ""
                        
                        let Push = UserChatDetailVC()
                        let MainData = dict.value(forKey: "data") as! NSDictionary
                        Push.Order_id = MainData.value(forKey: "order_id") as! String
                        Push.name = MainData.value(forKey: "worker_name") as! String
                        Push.thread_id = MainData.value(forKey: "thread_id") as! String
                        self.navigationController?.pushViewController(Push, animated: true)
                        
                        
                        
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "email") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    
   
    // MARK:- PLACE PICKER METHODS
    
    // Location Picker Delegate
    
    func locationDidSelect(locationItem: LocationItem) {
        print("Select delegate method: " + locationItem.name)
        
        if locationItem.name.isEmpty == true
        {
           self.address = String()
            self.currentlatitude = Double()
            self.currentlogitude = Double()
            Txt_Google_Address.text = String()
            print("YES")
        }
        else
        {
            
            print("NO")
            
            var Temp = NSDictionary()
            Temp = locationItem.addressDictionary! as NSDictionary
            print(locationItem.description)
            print(Temp)
            
            let FinalAddress = Temp.value(forKey: "FormattedAddressLines") as! NSArray
            print(FinalAddress)
            var stringWithCommas = String()
            
            
            let Array = FinalAddress as NSArray
            stringWithCommas = (Array ).componentsJoined(by: ",")
            print(stringWithCommas)
            
            self.address = stringWithCommas
            Txt_Google_Address.text = self.address
            
            self.currentlogitude = (locationItem.coordinate?.longitude)!
            self.currentlatitude = (locationItem.coordinate?.latitude)!
            
            print(locationItem.coordinate?.longitude)
            print(locationItem.coordinate?.latitude)
            
            
        }
        
       
        
    }
    
    func locationDidPick(locationItem: LocationItem) {
        showLocation(locationItem: locationItem)
        storeLocation(locationItem: locationItem)
    }
    
    func showLocation(locationItem: LocationItem) {
        
        if locationItem.name.isEmpty == true
        {
            self.address = String()
            self.currentlatitude = Double()
            self.currentlogitude = Double()
            Txt_Google_Address.text = String()
            print("YES")
        }
        else
        {
            
            print("NO")
            
            var Temp = NSDictionary()
            Temp = locationItem.addressDictionary! as NSDictionary
            print(locationItem.description)
            print(Temp)
            
            let FinalAddress = Temp.value(forKey: "FormattedAddressLines") as! NSArray
            print(FinalAddress)
            var stringWithCommas = String()
            
            
            let Array = FinalAddress as NSArray
            stringWithCommas = (Array ).componentsJoined(by: ",")
            print(stringWithCommas)
            
            self.address = stringWithCommas
            Txt_Google_Address.text = self.address
            
            self.currentlogitude = (locationItem.coordinate?.longitude)!
            self.currentlatitude = (locationItem.coordinate?.latitude)!
            
            print(locationItem.coordinate?.longitude)
            print(locationItem.coordinate?.latitude)
            
            
        }
        
        //  Txt_Google_Address.text = locationItem.formattedAddressString
    }
    
    func storeLocation(locationItem: LocationItem) {
        if let index = historyLocationList.index(of: locationItem) {
            historyLocationList.remove(at: index)
        }
        historyLocationList.append(locationItem)
        print(historyLocationList)
        
    }
    
    
    // Location Picker Data Source
    
    func numberOfAlternativeLocations() -> Int {
        return historyLocationList.count
    }
    
    func alternativeLocation(at index: Int) -> LocationItem {
        return historyLocationList.reversed()[index]
    }
    
    func commitAlternativeLocationDeletion(locationItem: LocationItem) {
        historyLocationList.remove(at: historyLocationList.index(of: locationItem)!)
    }
    
    
    
  
    
    //MARK:- TABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            
            if self.WorkerProfileDict.count == 0
            {
                return 0
            }
            else
            {
                
                let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
                
                if ratting_review.count == 0
                {
                    return 1
                }
                else
                    
                {
                    self.Review_Array = ratting_review
                    
                    return Review_Array.count
                }
                
                //ratting_review
            }
            
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if indexPath.section == 0
        {
            let cell:WorkerProfileHeaderXIB = Tableview_Worker.dequeueReusableCell(withIdentifier: "WorkerProfileHeaderXIB", for: indexPath) as! WorkerProfileHeaderXIB
            
            if self.WorkerProfileDict.count == 0
            {
                
            }
            else
            {
                
                
                let TempArray = self.WorkerProfileDict.value(forKey: "worker_images") as! NSArray
                
                self.slider_photo = TempArray.value(forKey: "image") as! NSArray
                
                
                var alamofireSource = [AlamofireSource]()
                for url in slider_photo
                {
                    alamofireSource.append(AlamofireSource(urlString: url as! String)!)
                }
                cell.Slide_Show.backgroundColor = UIColor.white
                cell.Slide_Show.slideshowInterval = 5.0
                cell.Slide_Show.pageControlPosition = PageControlPosition.insideScrollView
                cell.Slide_Show.pageControl.currentPageIndicatorTintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4")
                
                cell.Slide_Show.pageControl.sizeToFit()
                cell.Slide_Show.pageControl.pageIndicatorTintColor = UIColor.white
                cell.Slide_Show.contentScaleMode = UIView.ContentMode.scaleAspectFill
                cell.Slide_Show.clipsToBounds = true
                cell.Slide_Show.activityIndicator = DefaultActivityIndicator()
                cell.Slide_Show.currentPageChanged = { page in
                    
                }
                
                ApiUtillity.sharedInstance.setCornurRadius(obj: cell.View_Date, cornurRadius: 5, isClipToBound: true, borderColor: "9CC1D4", borderWidth: 2)
                
                
                
                if alamofireSource.count == 0
                {
                    //  cell.View_ImageSlideShow.setImageInputs(localSource)
                    
                }
                else
                {
                    cell.Slide_Show.setImageInputs(alamofireSource)
                }
                
                cell.View_Imageview.layer.cornerRadius = 56.0
                cell.View_Imageview.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "D3D3D3").cgColor
                cell.View_Imageview.layer.shadowOpacity = 4
                cell.View_Imageview.layer.shadowOffset = CGSize.zero
                cell.View_Imageview.layer.shadowRadius = 4
                cell.View_Imageview.clipsToBounds = true
                cell.View_Imageview.layer.masksToBounds = false
                
                
                
                
                
               
                let gen_rattings = self.WorkerProfileDict.value(forKey: "gen_rattings") as! String
                
                let proficiency = self.WorkerProfileDict.value(forKey: "proficiency") as! String
                let profile_image = self.WorkerProfileDict.value(forKey: "profile_image") as! String
                let working_city = self.WorkerProfileDict.value(forKey: "working_city") as! String
               
                let distance = self.WorkerProfileDict.value(forKey: "distance") as! String
                let myDouble = Double(distance)
                print(myDouble)
                let text = String(format: "%.0f", arguments: [myDouble!])
                print(text)
                
                let name = self.WorkerProfileDict.value(forKey: "name") as! String
                
                
                
                cell.Lbl_Name.text = "\(name)"
                cell.Lbl_Overall_Ratings.text = "\(gen_rattings)" + "/5"
                cell.View_Ratings.rating = Double(gen_rattings)!
                cell.Lbl_Proficiency.text = "\(proficiency)"
                cell.imageview_Profile.kf.indicatorType = .activity
                cell.imageview_Profile.kf.setImage(with: URL(string: profile_image ))
                cell.Lbl_Address.text = "\(working_city)"
                cell.Lbl_Lets_Work_With.text = "let's Work with " + "\(name)" + " !!"
                cell.Lbl_Customer_Review_About.text = "Customer Review's about " + "\(name)" + ":"
                
              
                
                
              //  self.title =  "\(name)" + "\(text)" + " KM Away from you"

                self.Lbl_Header.text = "\(name)"
                self.Lbl_Distance.text = "\(text)" + " KM Away from you"
                
                
                cell.Lbl_Date_Time.text = "\(SelectedDate)"
                cell.Btn_Title_Go.addTarget(self,action:#selector(self.AddAdress(sender:)), for: .touchUpInside)
                cell.Btn_Title_Date_Select.addTarget(self,action:#selector(self.DateSelect(sender:)), for: .touchUpInside)
                
                
                cell.Btn_Title_Slide_Show.addTarget(self, action: #selector(self.Slideshow(sender:)), for: .touchUpInside)
                
                
                
            }
            
            
            
            
            return cell
            
        }
        else
        {
            
            if self.WorkerProfileDict.count == 0
            {
                return UITableViewCell()
            }
            else
            {
                //ratting_review
                
                let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
                
                if ratting_review.count == 0
                {
                    let cell:NoRatingsXIB = Tableview_Worker.dequeueReusableCell(withIdentifier: "NoRatingsXIB", for: indexPath) as! NoRatingsXIB
                    
                    cell.imageview_Dot.isHidden = true
                    
                    return cell
                }
                else
                {
                    let cell:WorkerReviewCell = Tableview_Worker.dequeueReusableCell(withIdentifier: "WorkerReviewCell", for: indexPath) as! WorkerReviewCell
                    
                    cell.View_Background.layer.cornerRadius = 10
                    cell.View_Background.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "D3D3D3").cgColor
                    cell.View_Background.layer.shadowOpacity = 4
                    cell.View_Background.layer.shadowOffset = CGSize.zero
                    cell.View_Background.layer.shadowRadius = 4
                    cell.View_Background.clipsToBounds = true
                    cell.View_Background.layer.masksToBounds = false
                    
                    
                    print(self.Review_Array.object(at: indexPath.row)as! NSDictionary)
                    
                    let Ratings = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "ratting") as! String
                    
                    let ratting_date_time = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "ratting_date_time") as! String
                    
                    let review = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "review") as! String
                    
                    let username = (self.Review_Array.object(at: indexPath.row)as! NSDictionary).value(forKey: "username") as! String
                    
                    
                    
                    //2018-10-17 12:49:49
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let date = dateFormatter.date (from: ratting_date_time)
                    
                    let String1 = date!.timeAgoSinceNow
                    print(String1)
                    
                    
                    cell.Lbl_Time.text = "\(String1)"
                    cell.Lbl_Review.text = "\(review)"
                    cell.Lbl_User_Name.text = "\(username)"
                    cell.Lbl_Ratings.text = "\(Ratings)" + "/5"
                    
                    
                    
                    
                    //    ratting_date_time.timeAgoSinceDate()
                    
                    
                    
                    
                    return cell
                    
                }
                
            }
            
            
            
        }
        
        
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 500
        }
        else
        {
            let ratting_review = self.WorkerProfileDict.value(forKey: "ratting_review") as! NSArray
            
            if ratting_review.count == 0
            {
                
                return 50
            }
            else
            {
                
                
                return UITableView.automaticDimension
                
            }
            
        }
    }
    
   
    
}





private extension UserNewProfileVC {
    func createWebPhotos() -> [SKPhotoProtocol]
    {
        
        let FinalimageArray = self.WorkerProfileDict.value(forKey: "worker_images") as! NSArray
        
        
        print(FinalimageArray)
        
        
        
        return (0..<FinalimageArray.count).map { (i: Int) -> SKPhotoProtocol in
            
            let String =  (FinalimageArray.object(at: i)as! NSDictionary).value(forKey: "image") as! String
            
            
            let photo = SKPhoto.photoWithImageURL(String )
            
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
}


