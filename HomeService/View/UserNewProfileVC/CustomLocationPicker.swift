//
//  CustomLocationPicker.swift
//  LocationPickerExample
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class CustomLocationPicker: LocationPicker {
    
    var viewController: UserNewProfileVC!

    override func viewDidLoad() {
        super.addBarButtons()
        super.viewDidLoad()
    }
    
    @objc override func locationDidSelect(locationItem: LocationItem) {
        print("Select overrided method: " + locationItem.name)
        
        if locationItem.name.isEmpty == true
        {
            
          
                        print("YES")
        }
        else
        {

            print("NO")
            
            var Temp = NSDictionary()
            Temp = locationItem.addressDictionary! as NSDictionary
            
            
            print(locationItem.description)
            print(Temp)
            
            let FinalAddress = Temp.value(forKey: "FormattedAddressLines") as! NSArray
            print(FinalAddress)
            
           
            var stringWithCommas = String()

            
            let Array = FinalAddress as NSArray
            stringWithCommas = (Array ).componentsJoined(by: ",")
            print(stringWithCommas)
            
            print(locationItem.coordinate?.longitude)
            print(locationItem.coordinate?.latitude)


        }
    }
    
    @objc override func locationDidPick(locationItem: LocationItem) {
        viewController.showLocation(locationItem: locationItem)
        viewController.storeLocation(locationItem: locationItem)
    }
}
