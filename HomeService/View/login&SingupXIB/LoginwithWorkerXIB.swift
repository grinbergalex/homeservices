//
//  LoginwithWorkerXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class LoginwithWorkerXIB: UITableViewCell
{

    @IBOutlet weak var btn_for_login_worker: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        ApiUtillity.sharedInstance.setCornurRadius(obj: btn_for_login_worker
            , cornurRadius: 15, isClipToBound: true, borderColor: "ffffff", borderWidth: 0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
