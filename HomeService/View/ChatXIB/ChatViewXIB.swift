//
//  ChatViewXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class ChatViewXIB: UITableViewCell
{

    @IBOutlet weak var Lbl_Time_Duration: UILabel!
    @IBOutlet weak var user_image_view: UIImageView!
    @IBOutlet weak var lbl_for_user_name: UILabel!
    @IBOutlet weak var lbl_for_message: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ApiUtillity.sharedInstance.setCornurRadius(obj: user_image_view, cornurRadius: 24, isClipToBound: true, borderColor: "ffffff", borderWidth: 0)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
