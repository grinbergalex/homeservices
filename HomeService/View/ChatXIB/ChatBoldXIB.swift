//
//  ChatBoldXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class ChatBoldXIB: UITableViewCell {
    
    @IBOutlet weak var imageview_user: UIImageView!
    @IBOutlet weak var Lbl_User_name: UILabel!
    @IBOutlet weak var Lbl_Message: UILabel!
    @IBOutlet weak var Lbl_Time_Duration: UILabel!
    /*
 
     @IBOutlet weak var Lbl_Time_Duration: UILabel!
     @IBOutlet weak var user_image_view: UIImageView!
     @IBOutlet weak var lbl_for_user_name: UILabel!
     @IBOutlet weak var lbl_for_message: UILabel!
     
 
    */

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
