//
//  WorkerAboutVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class WorkerAboutVC: UIViewController {
    
    // MARK:- VARIABLES
    
    //MARK:- OUTLETS
    
    //MARK:- VIEW DID LOAD
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    // MARK:- ALL FUNCTIONS
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
