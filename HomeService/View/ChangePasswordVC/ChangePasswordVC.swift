//
//  ChangePasswordVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import PasswordTextField
import Alamofire

class ChangePasswordVC: UIViewController {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var View_Small_Change_Password_View: UIView!
    
    @IBOutlet weak var Txt_Confirm_Password: PasswordTextField!
    @IBOutlet weak var Txt_New_Password: PasswordTextField!
    @IBOutlet weak var Txt_Old_Password: PasswordTextField!
    
    //MARK:- VARIABLES
    var apiToken = String()
    var Token =  String()
    
    //MARK:- VIEW DIDLOAD

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        View_Small_Change_Password_View.clipsToBounds = true
        View_Small_Change_Password_View.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
        View_Small_Change_Password_View.layer.borderWidth = 1.0
        

        // Do any additional setup after loading the view.
    }


    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func Btn_Handler_Change_Password(_ sender: Any)
    {
        if Txt_Old_Password.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Old Password", ForNavigation: self.navigationController!)
            return
        }
        if Txt_New_Password.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter New Password", ForNavigation: self.navigationController!)
            return
        }
        if Txt_Confirm_Password.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Confirm Password", ForNavigation: self.navigationController!)
            return
        }
        if Txt_Confirm_Password.text == Txt_New_Password.text
        {
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Both Password Are Not Same", ForNavigation: self.navigationController!)
            return
        }
        
        ChangePassword()

    }
    //MARK:- ALL FUNCTIONS
    
    
    func ChangePassword()
    {
        
        
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        let paramiter = ["vOldPassword":self.Txt_Old_Password.text!, "vNewPassword":self.Txt_New_Password.text!] as [String : Any]
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        print(paramiter)
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/change_password"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                // print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    
                    let SuccessDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    
                    let SuccessMessage = SuccessDic.value(forKey: "success") as! String
                    
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: SuccessMessage)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2)
                    {
                        // your code here
                        self.Txt_New_Password.text = ""
                        self.Txt_Old_Password.text = ""
                        self.Txt_Confirm_Password.text = ""
                        self.view.endEditing(true)
                        
                    }
                    
                  
                    
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                  

                        
                    }
                    
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
    }

}
