//
//  ForgotPasswordVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{

    @IBOutlet weak var tbl_view: UITableView!
    
    var Email_address = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tbl_view.register(ForgotPasswordHeaderXIB.self, forCellReuseIdentifier: "ForgotPasswordHeaderXIB")
        tbl_view.register(UINib(nibName: "ForgotPasswordHeaderXIB", bundle: nil), forCellReuseIdentifier: "ForgotPasswordHeaderXIB")
        
        tbl_view.register(textFiledXIB.self, forCellReuseIdentifier: "textFiledXIB")
        tbl_view.register(UINib(nibName: "textFiledXIB", bundle: nil), forCellReuseIdentifier: "textFiledXIB")
        
        tbl_view.register(LoginButtonXIB.self, forCellReuseIdentifier: "LoginButtonXIB")
        tbl_view.register(UINib(nibName: "LoginButtonXIB", bundle: nil), forCellReuseIdentifier: "LoginButtonXIB")
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    MARK:- TABLEVIEW METHOD
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
              let cell:ForgotPasswordHeaderXIB = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordHeaderXIB", for: indexPath) as! ForgotPasswordHeaderXIB
            
            cell.selectionStyle = .none
            return cell
            
        }
        else if indexPath.section == 1
        {
            let cell:textFiledXIB = tableView.dequeueReusableCell(withIdentifier: "textFiledXIB", for: indexPath) as! textFiledXIB
            cell.txt_field.placeholder = "Enter your email"
            cell.icon_image.image = UIImage(named: "mail")
            cell.txt_field.delegate = self
            cell.txt_field.tag = 1
            cell.txt_field.placeHolderColor = UIColor.darkGray
            return cell
        }
        else
        {
            let cell:LoginButtonXIB = tableView.dequeueReusableCell(withIdentifier: "LoginButtonXIB", for: indexPath) as! LoginButtonXIB
            
            cell.btn_for_login.layer.cornerRadius = 10
            cell.btn_for_login.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
            cell.btn_for_login.layer.borderWidth = 1
            cell.btn_for_login.layer.masksToBounds = false
            
            
            cell.btn_for_login.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.btn_for_login.layer.shadowOpacity = 4
            cell.btn_for_login.layer.shadowOffset = CGSize.zero
            cell.btn_for_login.layer.shadowRadius = 4
            cell.btn_for_login.clipsToBounds = true
            cell.btn_for_login.layer.masksToBounds = false
            cell.btn_for_login.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.btn_for_login.layer.borderWidth = 1.0
            
           
            cell.btn_for_login.setTitleColor(.black, for: .normal)

            cell.btn_for_login.setTitle("FORGOT", for: .normal)
            cell.btn_for_login.addTarget(self, action: #selector(forgot_password), for: .touchUpInside)
            return cell
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 315
        }
        else if indexPath.section == 1
        {
            return 50
        }
        else
        {
            return 70
        }
    }
    
    
    
    
    //    MARK:- TEXTFIELD DELEGATE
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        var kActualText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        kActualText = kActualText.trimmingCharacters(in: .whitespaces)
        
        switch textField.tag
        {
        case 1:
            Email_address = kActualText;
        default:
            print("It is nothing");
        }
        return true;
    }
    
    
    //  MARK:-   API CALL
    
    @objc func forgot_password()
    {
        if Email_address == ""
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
            return
        }
        else if ApiUtillity.sharedInstance.isValidEmail(testStr: Email_address) == false
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Valid Email Address", ForNavigation: self.navigationController!)
            return
        }
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            let params = ["email":Email_address] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/forgot_password"), method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: "Reset password link sent successfully")
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "email") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    


}
