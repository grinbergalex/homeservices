//
//  ProfileImageXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class ProfileImageXIB: UITableViewCell {
    
    
    @IBOutlet weak var user_pf_image_view: UIImageView!
    @IBOutlet weak var lbl_for_name: UILabel!
    @IBOutlet weak var lbl_for_email_address: UILabel!
    @IBOutlet weak var lbl_for_mobile_number: UILabel!
    @IBOutlet weak var btn_for_upgrade: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: btn_for_upgrade, cornurRadius: 5, isClipToBound: true, borderColor: "fffff", borderWidth: 0)
        ApiUtillity.sharedInstance.setCornurRadius(obj: user_pf_image_view, cornurRadius: 40, isClipToBound: true, borderColor: "9cc1d4", borderWidth: 4)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
