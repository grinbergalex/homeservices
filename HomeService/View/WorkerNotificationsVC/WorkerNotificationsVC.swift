//
//  WorkerNotificationsVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire

class WorkerNotificationsVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    //MARK:- OUTLETS
    @IBOutlet weak var Tableview_Notifications: UITableView!
    
    @IBOutlet weak var Lbl_No_Notifications: UILabel!
    
    
    
    
    
    //MARK:- VARIRABLES
    var apiToken = String()
    var Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken") as! String
    var NotificationArray = NSMutableArray()
    
    
    //MARK:- VIEW DID LOAD

    override func viewDidLoad() {
        super.viewDidLoad()
        
          apiToken = "Bearer \(Token)"
        
        Tableview_Notifications.register(WorkerNotificationsXIB.self, forCellReuseIdentifier: "WorkerNotificationsXIB")
        Tableview_Notifications.register(UINib(nibName: "WorkerNotificationsXIB", bundle: nil), forCellReuseIdentifier: "WorkerNotificationsXIB")
        
        self.Lbl_No_Notifications.isHidden = true
        
        self.Tableview_Notifications.estimatedRowHeight = 80.0
        self.Tableview_Notifications.rowHeight = UITableView.automaticDimension
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            get_all_notifications()
            
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }

        // Do any additional setup after loading the view.
    }


    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- ALL FUNCTIONS
    func get_all_notifications()
    {
        let headers = ["Vauthtoken":apiToken]
        
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/get_all_notification"), method: .post, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    let UserData = dict.value(forKey: "data") as! NSArray
                    
                    if UserData.count == 0
                    {
                        self.Tableview_Notifications.isHidden = true
                        self.Lbl_No_Notifications.isHidden = false
                    }
                    else
                    {
                        
                        self.Tableview_Notifications.isHidden = false
                        self.Lbl_No_Notifications.isHidden = true
                        
                        self.NotificationArray = UserData.mutableCopy() as! NSMutableArray
                        self.Tableview_Notifications.reloadData()
                        
                    }
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    
                }
                    
                else if StatusCode==401
                {
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    UserDefaults.standard.set(false, forKey:"loggedin")
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    let push =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(push, animated: true)
                }
                    
                else if StatusCode==412
                {
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                }
                    
                else
                {
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
            }
        }
    }
    
    
    //MARK:- TABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         return NotificationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:WorkerNotificationsXIB = Tableview_Notifications.dequeueReusableCell(withIdentifier: "WorkerNotificationsXIB", for: indexPath) as! WorkerNotificationsXIB
        
        print(self.NotificationArray.object(at: indexPath.row))
        
        
        let insert_date = (self.NotificationArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "insert_date") as! String
        let push_from = (self.NotificationArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "push_from") as! String
        let push_message = (self.NotificationArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "push_message") as! String
        
        //2018-10-17 12:49:49
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date (from: insert_date)
        
        let String1 = date!.timeAgoSinceNow
        print(String1)
        cell.Lbl_Time_Ago.text = "\(String1)"
        cell.Lbl_Title_.text = "\(push_message)"
        cell.Lbl_Description.text = "\(push_from)"
        
        

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        
        
       
         let type = ApiUtillity.sharedInstance.getUserData(key: "type")
        
         if type == "user"
         {
            let Push = UserChatDetailVC()
            let MainData = (self.NotificationArray.object(at: indexPath.row))as! NSDictionary
            Push.Order_id = MainData.value(forKey: "order_id") as! String
            Push.name = MainData.value(forKey: "name") as! String
            Push.thread_id = MainData.value(forKey: "iThreadId") as! String
            self.navigationController?.pushViewController(Push, animated: true)
            
        }
        else
        
         {
            
            let MainData = (self.NotificationArray.object(at: indexPath.row))as! NSDictionary
            
            
            let push_from = MainData.value(forKey: "push_from") as! String
            
            if push_from == "User give you ratings"
            {
                let id = MainData.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = "\(id)"
                self.navigationController?.pushViewController(Push, animated: true)
            }
            else
            {
                let Push = UserChatDetailVC()
                Push.Order_id = MainData.value(forKey: "order_id") as! String
                Push.name = MainData.value(forKey: "name") as! String
                Push.thread_id = MainData.value(forKey: "iThreadId") as! String
                self.navigationController?.pushViewController(Push, animated: true)
            }
        }
        
        //push_from
        
    }
}
