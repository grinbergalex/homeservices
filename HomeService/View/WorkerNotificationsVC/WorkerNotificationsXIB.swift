//
//  WorkerNotificationsXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class WorkerNotificationsXIB: UITableViewCell {

    @IBOutlet weak var Lbl_Description: UILabel!
    @IBOutlet weak var Lbl_Title_: UILabel!
    @IBOutlet weak var Lbl_Time_Ago: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
