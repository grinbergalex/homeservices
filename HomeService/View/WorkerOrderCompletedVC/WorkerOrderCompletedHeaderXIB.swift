//
//  WorkerOrderCompletedHeaderXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class WorkerOrderCompletedHeaderXIB: UITableViewCell {

    @IBOutlet weak var imageview_Call: UIImageView!
    @IBOutlet weak var imageview_location: UIImageView!
    @IBOutlet weak var Lbl_Detail: UILabel!
    @IBOutlet weak var Lbl_Order_Numberr: UILabel!
    @IBOutlet weak var Btn_Handler_Call: UIButton!
    @IBOutlet weak var Btn_Title_Location: UIButton!
    // running - incomplete_process
    @IBOutlet weak var Lbl_Full_Address: UILabel!
    @IBOutlet weak var Lbl_Customer_Name: UILabel!
    @IBOutlet weak var Customer_Imageview: UIImageView!
    @IBOutlet weak var Lbl_Order_Date: UILabel!
    @IBOutlet weak var imageview_status: UIImageView!
    
    @IBOutlet weak var Lbl_Order_Status_Header: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
