//
//  WorkerOrderCompleted.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import Cosmos
import UserNotifications





class WorkerOrderCompleted: UIViewController,UITableViewDelegate,UITableViewDataSource,UNUserNotificationCenterDelegate,UITextFieldDelegate{
    
    //MARK:- OUTLETS
    
    
    @IBOutlet weak var Btn_Title_Cancel: UIButton!
    @IBOutlet weak var Lbl_Header_Name: UILabel!
    @IBOutlet weak var Tableview_Order_Complete: UITableView!
    
    //MARK:- VARIABLES
    var Order_id = String()
    var OrderdetailDict = NSMutableDictionary()
    var apiToken = String()
    var Token =  String()
    var CallNumber = String()
    var worker_status = String()
    var type = String()
    var Star_Ratings = String()
    var Review_Text = String()
    var User_Status = String()
    var is_Push_From = String()
    var Is_Push_Check = String()


    
    
    //MARK:- VIEW DID LOAD

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Tableview_Order_Complete.register(WorkerOrderCompletedHeaderXIB.self, forCellReuseIdentifier: "WorkerOrderCompletedHeaderXIB")
        Tableview_Order_Complete.register(UINib(nibName: "WorkerOrderCompletedHeaderXIB", bundle: nil), forCellReuseIdentifier: "WorkerOrderCompletedHeaderXIB")
        
        Tableview_Order_Complete.register(UserOrderReviewWithoutStarXIB.self, forCellReuseIdentifier: "UserOrderReviewWithoutStarXIB")
        Tableview_Order_Complete.register(UINib(nibName: "UserOrderReviewWithoutStarXIB", bundle: nil), forCellReuseIdentifier: "UserOrderReviewWithoutStarXIB")
        
        Tableview_Order_Complete.register(WorkerOrderCompletedXIB.self, forCellReuseIdentifier: "WorkerOrderCompletedXIB")
        Tableview_Order_Complete.register(UINib(nibName: "WorkerOrderCompletedXIB", bundle: nil), forCellReuseIdentifier: "WorkerOrderCompletedXIB")
        
        Tableview_Order_Complete.register(NoRatingsXIB.self, forCellReuseIdentifier: "NoRatingsXIB")
        Tableview_Order_Complete.register(UINib(nibName: "NoRatingsXIB", bundle: nil), forCellReuseIdentifier: "NoRatingsXIB")
        
        
   //     NoRatingsXIB
        
        Tableview_Order_Complete.register(UserOrderGiveReviewStarXIB.self, forCellReuseIdentifier: "UserOrderGiveReviewStarXIB")
        Tableview_Order_Complete.register(UINib(nibName: "UserOrderGiveReviewStarXIB", bundle: nil), forCellReuseIdentifier: "UserOrderGiveReviewStarXIB")
        
        Tableview_Order_Complete.register(WorkOrderHistoryReviewWithStarXIB.self, forCellReuseIdentifier: "WorkOrderHistoryReviewWithStarXIB")
        Tableview_Order_Complete.register(UINib(nibName: "WorkOrderHistoryReviewWithStarXIB", bundle: nil), forCellReuseIdentifier: "WorkOrderHistoryReviewWithStarXIB")
        
        
        
        //WorkOrderHistoryReviewWithStarXIB
        
        self.Tableview_Order_Complete.estimatedRowHeight = 80.0
        self.Tableview_Order_Complete.rowHeight = UITableView.automaticDimension
        
        
        UNUserNotificationCenter.current().delegate = self
        
        

        
        //UserOrderGiveReviewStarXIB
        
        type = ApiUtillity.sharedInstance.getUserData(key: "type")
        print(type)
        self.Btn_Title_Cancel.isHidden = true
        if type == "user"
        {
           
        }
        else
        {
           
        }
        
        
        print(Order_id)
        
        OrderDetails()
        
        

        // Do any additional setup after loading the view.
    }


    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Bac(_ sender: Any) {
        
        if self.Is_Push_Check.isEmpty == true
        {
            self.navigationController?.popViewController(animated: true)

        }
        else if self.Is_Push_Check == "1"
        {
            let push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
            self.navigationController?.pushViewController(push, animated: false)

        }
        
        
        
    }
    
    @IBAction func Btn_Handler_Cancel_Order(_ sender: Any)
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            
            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            self.apiToken = "Bearer \(self.Token)"
            let headers = ["Vauthtoken":self.apiToken]
            
            let currentTimeZone = getCurrentTimeZone()
            print(currentTimeZone)
            
            let paramiter = ["order_id":self.Order_id,"time_zone":currentTimeZone] as [String : Any]
            
            
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/cancel_order"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        
                        
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                        
                        
                    else if StatusCode==412
                    {
                        
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                        
                    else
                    {
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        
                        
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    //MARK:- ALL FUNCTIONS
    
    func OrderDetails()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            
            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            self.apiToken = "Bearer \(self.Token)"
            let headers = ["Vauthtoken":self.apiToken]
            
              let paramiter = ["order_id":self.Order_id] as [String : Any]
            
            
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/get_order_details"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        let user_Data = dict.value(forKey: "data") as! NSDictionary
                        self.OrderdetailDict = user_Data.mutableCopy() as! NSMutableDictionary
                        
                        let Or = user_Data.value(forKey: "order_id") as! String
                        self.Lbl_Header_Name.text = "#" + "\(Or)"
                        self.worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                        self.User_Status = self.OrderdetailDict.value(forKey: "user_status") as! String
                        
                        
                        self.Tableview_Order_Complete.reloadData()
                       //self.Tableview_Order_Complete.reloadSections(IndexSet(integersIn: 0...0), with: UITableView.RowAnimation.fade)
                        
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        
                        
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                        
                        
                    else if StatusCode==412
                    {
                        
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                        
                    else
                    {
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        
                        
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    
    @objc func ORDERCOMPLETE(sender:UIButton)
    {
        // online
       
        
        let headers = ["Vauthtoken":apiToken]
        
        let paramiter = ["order_id":self.Order_id] as [String : Any]
        print(paramiter)
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/complete_order"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                print(dict)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                if StatusCode==200
                {
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let successMessage = ErrorDic.value(forKey: "success") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: successMessage)
                    self.navigationController?.popViewController(animated: true)
                }
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
        
    }
    
    
    @objc func GETMAP(sender:UIButton)
    {
        print(self.OrderdetailDict)
        
        if self.OrderdetailDict.count == 0
        {
            
        }
        else
        {
            let Push = NavigationScreenXIB()
            Push.OrderdetailDict = self.OrderdetailDict.mutableCopy() as! NSMutableDictionary
            self.navigationController?.pushViewController(Push, animated: true)
        }
        
      
        
    }
    
    @objc func PostReview(sender:UIButton)
    {
        print(self.Star_Ratings)
        print(self.Review_Text)
        
        if self.Star_Ratings.isEmpty == true
        {
            self.Star_Ratings = "0"
        }
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            
            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            self.apiToken = "Bearer \(self.Token)"
            let headers = ["Vauthtoken":self.apiToken]
            
            let paramiter = ["order_id":self.Order_id,"rattings":self.Star_Ratings,"review":self.Review_Text] as [String : Any]
            
            
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/review_rattings"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {

                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "success") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: ErrorMessage)
                       
                        self.navigationController?.popViewController(animated: true)
                        
                        
                        
                        
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        
                        
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                        
                        
                    else if StatusCode==412
                    {
                        
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                        
                    else
                    {
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        
                        
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
        
        
    }
    
    @objc func GETCALL(sender:UIButton)
    {
        //self.Business_Detail_Dict
        print(self.CallNumber)
        let formattedString = self.CallNumber.replacingOccurrences(of: " ", with: "")
        
        if let url = URL(string: "tel://\(formattedString)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *)
            {
                UIApplication.shared.open(url)
            } else
            {
                UIApplication.shared.openURL(url)
            }
        }
        
        
    }
    
    
    private class func formatValue(_ value: Double) -> String
    {
        return String(format: "%.2f", value)
    }
    
    private func didTouchCosmos(_ rating: Double)
    {
        print(rating)
        
        self.Star_Ratings = "\(rating)"
        
        
    }
        
    
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        var kActualText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        kActualText = kActualText.trimmingCharacters(in: .whitespaces)
        
        if textField.tag == 10
        {
            self.Review_Text = kActualText
        }
        
        
        return true;
    }
    
    
    
        
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
         if (data_item.value(forKey: "push_type") as! String) == "5"
        {
            OrderDetails()
        }
        else if (data_item.value(forKey: "push_type") as! String) == "6"
        {
            OrderDetails()
        }
        else if (data_item.value(forKey: "push_type") as! String) == "7"
        {
            OrderDetails()
        }
        
        completionHandler([.alert, .sound])
    }
    

    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    func getCurrentTimeZone() -> String{
        
        return String (TimeZone.current.identifier)
        
        
        
    }
    
    
    
    
    //MARK:- TABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.OrderdetailDict.count == 0
        {
            return 0

        }
        else
        {
            if self.worker_status == "pending"
            {
               
                 if self.User_Status == "cancel"
                {
                    return 3
                }
                else
                 {
                     return 2
                }

            }
            
            else if self.worker_status == "accept"
            {
                if self.User_Status == "cancel"
                {
                    if self.type == "user"
                    {
                        return 3
                    }
                    else
                    {
                        return 3
                    }
                }
                else
                {
                    if self.type == "user"
                    {
                        return 3
                    }
                    else
                    {
                        return 4
                    }
                }
               
            }
            else if self.worker_status == "reject"
            {
                return 3
            }
                else if self.worker_status == "modify_date"
            {
                // NEEDED CHECK
                return 3
            }
                else if self.worker_status == "complete"
            {
                // worker status = "accept"
                
                let user_rattings_done = self.OrderdetailDict.value(forKey: "user_rattings_done") as! String
                if user_rattings_done == "0"
                {
                    return 5
                }
                else
                {
                    return 5
                }
                
                
                
            }
                else if self.worker_status == "cancel"
            {
                return 3
            }
            else
            {
                return 3

            }
            

        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.OrderdetailDict.count == 0
        {
                return 0
        }
        else
        {
            if self.worker_status == "pending"
            {
                
                
                self.Btn_Title_Cancel.isHidden = false
                
                if self.User_Status == "cancel"
                {
                    self.Btn_Title_Cancel.isHidden = true
                    if section == 0
                    {
                        
                        return 1
                    }
                        else if section == 1
                    {
                        return 1
                    }
                    else
                    {
                        return 1
                    }
                }
                else
                {
                    if section == 0
                    {
                        
                        return 1
                    }
                    else
                    {
                        return 1
                    }
                }
               
                
            }
            
            else if self.worker_status == "accept"
            {
                self.Btn_Title_Cancel.isHidden = false
                
                if self.User_Status == "cancel"
                {
                    self.Btn_Title_Cancel.isHidden = true

                    if self.type == "user"
                    {
                        
                        if section == 0
                        {
                            return 1
                        }
                        else if section == 1
                        {
                            return 1
                        }
                            
                        else
                        {
                            return 1
                        }
                    }
                    else
                    {
                        if section == 0
                        {
                            return 1
                        }
                        else if section == 1
                        {
                            return 1
                        }
                       
                        else
                        {
                            return 1
                        }
                    }
                }
                else
                {
                    if self.type == "user"
                    {
                        if section == 0
                        {
                            return 1
                        }
                        else if section == 1
                        {
                            return 1
                        }
                            
                        else
                        {
                            return 1
                        }
                    }
                    else
                    {
                        if section == 0
                        {
                            return 1
                        }
                        else if section == 1
                        {
                            return 1
                        }
                            else if section == 2
                        {
                            return 1
                            
                        }
                       
                        else
                        {
                            return 1
                        }
                    }
                }
     // worker status = "accept"
                // show completed button
            }
            else if self.worker_status == "reject"
            {
                self.Btn_Title_Cancel.isHidden = true
                if section == 0
                {
                    return 1
                }
                else if section == 1
                {
                    return 1
                }
               
                else
                {
                    return 1
                }
                
            }
            else if self.worker_status == "modify_date"
            {
                self.Btn_Title_Cancel.isHidden = true
                // NEEDED CHECK
                if section == 0
                {
                    return 1
                }
                else if section == 1
                {
                    return 1
                }
                else
                {
                    return 1
                }
                
            }
            else if self.worker_status == "complete"
            {
                // worker status = "accept"
                self.Btn_Title_Cancel.isHidden = true
                let user_rattings_done = self.OrderdetailDict.value(forKey: "user_rattings_done") as! String
                if user_rattings_done == "0"
                {
                    if section == 0
                    {
                        return 1
                    }
                    else if section == 1
                    {
                        return 1
                    }
                    else if section == 2
                    {
                        return 1
                    }
                        else if section == 3
                    {
                        return 1
                    }
                    else
                    {
                        return 1 
                    }
                }
                else
                {
                    if section == 0
                    {
                        return 1
                    }
                    else if section == 1
                    {
                        return 1
                    }
                    else if section == 2
                    {
                        return 1
                    }
                        else if section == 3
                    {
                        return 1
                    }
                    else
                    {
                        return 1
                    }
                }
                
                
                
            }
            else if self.worker_status == "cancel"
            {
               if section == 0
               {
                return 1
                }
                else if section == 1
               {
                return 1
                }
                else
               {
                return 1
                }
            }
            else
            {
                self.Btn_Title_Cancel.isHidden = true
                return 4
                
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.OrderdetailDict.count == 0
        {
            return UITableViewCell()
            
        }
        else
        {
            
            print(self.OrderdetailDict)
            
            if self.worker_status == "pending"
            {
                if self.User_Status == "cancel"
                {
                    if indexPath.section == 0
                    {
                        let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                        
                        cell.Btn_Handler_Call.isHidden = false
                        cell.Btn_Title_Location.isHidden = false
                        cell.imageview_Call.isHidden = false
                        cell.imageview_location.isHidden = false
                        
                        cell.imageview_status.image = UIImage(named: "incomplete_process-1")
                        print(self.OrderdetailDict)
                        
                        let user_status = self.OrderdetailDict.value(forKey: "user_status") as! String
                        let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                        let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                        let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                        let username = self.OrderdetailDict.value(forKey: "username") as! String
                        //address
                        let address = self.OrderdetailDict.value(forKey: "address") as! String
                        let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                        
                        let DataDate = self.UTCToLocal(date: date_time)
                        
                        if type == "user"
                        {
                            cell.Lbl_Detail.text = "Worker Detail:"
                            cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                            cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                            cell.Customer_Imageview.kf.indicatorType = .activity
                            cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                          
                        }
                        else
                        {
                            cell.Lbl_Detail.text = "Customer Detail:"
                            cell.Lbl_Customer_Name.text = "\(username)"
                            cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                             self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                            cell.Customer_Imageview.kf.indicatorType = .activity
                            cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                            
                            
                            //user_mobile
                        }
                      
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: DataDate)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        
                        //date_time
                        cell.Lbl_Order_Status_Header.text =  "Order" + " Cancelled "
                        cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                        
                        cell.Lbl_Order_Date.text = "\(resultString)"
                       
                        cell.Customer_Imageview.layer.cornerRadius = 32.5
                        cell.Customer_Imageview.clipsToBounds = true
                        cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                        cell.Customer_Imageview.layer.borderWidth = 3.0
                        
                        
                      
                        
                        cell.Btn_Handler_Call.tag = indexPath.row
                        cell.Btn_Title_Location.tag = indexPath.row
                        
                        //GETCALL
                        cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                        cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                        
                        return cell
                    }
                    else if indexPath.section == 1
                    {
                        let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                        
                        let username = self.OrderdetailDict.value(forKey: "username") as! String
                        let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                        
                        let DataDate = self.UTCToLocal(date: insert_date)

                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: DataDate)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        if type == "user"
                        {
                            cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                        }
                        else
                        {
                            cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                        }
                        cell.Lbl_Created_Date.text = "\(resultString)"
                        cell.imageview_line.isHidden = false
                        
                        return cell
                        
                    }
                    else
                    {
                        let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                        
                        let username = self.OrderdetailDict.value(forKey: "username") as! String
                        let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                        
                        let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: DataDate)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        
                        
                        
                        
                        
                        if type == "user"
                        {
                            cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "You"
                        }
                        else
                        {
                            cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "\(username)"
                        }
                        cell.Lbl_Created_Date.text = "\(resultString)"
                        cell.imageview_line.isHidden = true
                        
                        return cell
                    }
                }
                else
                {
                    if indexPath.section == 0
                    {
                        let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                        
                        cell.imageview_status.image = UIImage(named: "incomplete_process")
                        
                        cell.Btn_Handler_Call.isHidden = false
                        cell.Btn_Title_Location.isHidden = false
                        cell.imageview_Call.isHidden = false
                        cell.imageview_location.isHidden = false
                        
                        
                        print(self.OrderdetailDict)
                        
                        let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                        let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                        let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                        let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                        let username = self.OrderdetailDict.value(forKey: "username") as! String
                        //address
                        let address = self.OrderdetailDict.value(forKey: "address") as! String
                        let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                        self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                        
                        
                        
                        if type == "user"
                        {
                            cell.Lbl_Detail.text = "Worker Detail:"
                            cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                            cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                            cell.Customer_Imageview.kf.indicatorType = .activity
                            cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                           
                        }
                        else
                        {
                            cell.Lbl_Detail.text = "Customer Detail:"
                            cell.Lbl_Customer_Name.text = "\(username)"
                            cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                            self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                            cell.Customer_Imageview.kf.indicatorType = .activity
                            cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                            
                            //user_mobile
                        }
                        
                        //wlat
                        //wlng
                        //specific_address
                        
                        //worker_mobile
                        
                        let DataDate = self.UTCToLocal(date: date_time)

                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: DataDate)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        
                        //date_time
                        cell.Lbl_Order_Status_Header.text =  "Order" + " Pending "
                        cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                        cell.Lbl_Order_Date.text = "\(resultString)"
                       
                        
                        cell.Customer_Imageview.layer.cornerRadius = 32.5
                        cell.Customer_Imageview.clipsToBounds = true
                        cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                        cell.Customer_Imageview.layer.borderWidth = 3.0
                        
                        
                      
                        
                        cell.Btn_Handler_Call.tag = indexPath.row
                        cell.Btn_Title_Location.tag = indexPath.row
                        
                        //GETCALL
                        cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                        cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                        
                        return cell
                    }
                    else if indexPath.section == 1
                    {
                        let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                        
                        let username = self.OrderdetailDict.value(forKey: "username") as! String
                        let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                        
                        let DataDate = self.UTCToLocal(date: insert_date)

                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: DataDate)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        
                        if type == "user"
                        {
                            cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                        }
                        else
                        {
                            cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                        }
                        
                        cell.Lbl_Created_Date.text = "\(resultString)"
                        cell.imageview_line.isHidden = true
                        
                        return cell
                        
                    }
                    else
                    {
                        return UITableViewCell()
                    }
                    
                }
                
                
            }
            
            else if self.worker_status == "accept"
            {
                if self.User_Status == "cancel"
                {
                    if self.type == "user"
                    {
                        if indexPath.section == 0
                        {
                            
                            
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "incomplete_process-1")
                            
                            cell.Btn_Handler_Call.isHidden = false
                            cell.Btn_Title_Location.isHidden = false
                            cell.imageview_Call.isHidden = false
                            cell.imageview_location.isHidden = false
                            
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                //worker_name
                                //worker_mobile
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Cancelled. "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                           
                        else
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "\(username)"
                            }
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = true
                            
                            return cell
                        }
                    }
                    else
                    {
                        if indexPath.section == 0
                        {
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "incomplete_process-1")
                            
                            cell.Btn_Handler_Call.isHidden = false
                            cell.Btn_Title_Location.isHidden = false
                            cell.imageview_Call.isHidden = false
                            cell.imageview_location.isHidden = false
                            
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                //worker_name
                                //worker_mobile
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Cancelled "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                         
                        else
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "You"
                            }
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                        }
                       
                    
                }
                }
                else
                {
                    if self.type == "user"
                    {
                        if indexPath.section == 0
                        {
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "incomplete_process")
                            
                            cell.Btn_Handler_Call.isHidden = false
                            cell.Btn_Title_Location.isHidden = false
                            cell.imageview_Call.isHidden = false
                            cell.imageview_location.isHidden = false
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                //worker_name
                                //worker_mobile
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Running "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                        else
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = true
                            
                            return cell
                        }
                    }
                    else
                    {
                        if indexPath.section == 0
                        {
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "incomplete_process")
                            
                            cell.Btn_Handler_Call.isHidden = false
                            cell.Btn_Title_Location.isHidden = false
                            cell.imageview_Call.isHidden = false
                            cell.imageview_location.isHidden = false
                            
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                //worker_name
                                //worker_mobile
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Running "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                        else if indexPath.section == 2
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "You"
                            }
                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                        }
                        else
                        {
                            let cell:WorkerOrderCompletedXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedXIB", for: indexPath) as! WorkerOrderCompletedXIB
                            
                            cell.Btn_Title_Order_Completed.layer.cornerRadius = 5
                            cell.Btn_Title_Order_Completed.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Btn_Title_Order_Completed.layer.shadowOpacity = 4
                            cell.Btn_Title_Order_Completed.layer.shadowOffset = CGSize.zero
                            cell.Btn_Title_Order_Completed.layer.shadowRadius = 4
                            cell.Btn_Title_Order_Completed.clipsToBounds = true
                            //cell.user_pf_image_view.layer.masksToBounds = false
                            cell.Btn_Title_Order_Completed.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Btn_Title_Order_Completed.layer.borderWidth = 2.0
                            
                            
                            cell.Btn_Title_Order_Completed.tag = indexPath.row
                            cell.Btn_Title_Order_Completed.addTarget(self,action:#selector(self.ORDERCOMPLETE(sender:)), for: .touchUpInside)
                            
                            return cell
                            
                        }
                        
                        
                    }
                    
                }
                
                // worker status = "accept"
                // show completed button
            }
            else if self.worker_status == "reject"
            {
                if indexPath.section == 0
                {
                    let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                    
                    cell.imageview_status.image = UIImage(named: "incomplete_process-1")

                    cell.Btn_Handler_Call.isHidden = false
                    cell.Btn_Title_Location.isHidden = false
                    cell.imageview_Call.isHidden = false
                    cell.imageview_location.isHidden = false
                    
                    print(self.OrderdetailDict)
                    
                    let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                    let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                    let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                    let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    //address
                    let address = self.OrderdetailDict.value(forKey: "address") as! String
                    let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                    self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                    
                    
                    if type == "user"
                    {
                        cell.Lbl_Detail.text = "Worker Detail:"
                        cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                        cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                        self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                        let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                        cell.Customer_Imageview.kf.indicatorType = .activity
                        cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                        
                    }
                    else
                    {
                        cell.Lbl_Detail.text = "Customer Detail:"
                        cell.Lbl_Customer_Name.text = "\(username)"
                        cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                        self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                       
                        cell.Customer_Imageview.kf.indicatorType = .activity
                        cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                        
                        //user_mobile
                    }
                    
                    
                    //wlat
                    //wlng
                    //specific_address
                    
                    //worker_mobile
                      let DataDate = self.UTCToLocal(date: date_time)
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    print(resultString)
                    
                    
                    //date_time
                    cell.Lbl_Order_Status_Header.text =  "Order" + " Rejected "
                    cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                    cell.Lbl_Order_Date.text = "\(resultString)"
                    cell.Customer_Imageview.layer.cornerRadius = 32.5
                    cell.Customer_Imageview.clipsToBounds = true
                    cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                    cell.Customer_Imageview.layer.borderWidth = 3.0
                    
                    
                   
                    
                    cell.Btn_Handler_Call.tag = indexPath.row
                    cell.Btn_Title_Location.tag = indexPath.row
                    
                    
                    cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                    cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                    
                    
                    return cell
                }
                else if indexPath.section == 1
                {
                    let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                    
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                    
                    
                    
                    if type == "user"
                    {
                        cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                    }
                    else
                    {
                        cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                    }
                    
                    let DataDate = self.UTCToLocal(date: insert_date)

                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    print(resultString)
                    
                    cell.Lbl_Created_Date.text = "\(resultString)"
                    
                    
                    return cell
                    
                }
                else
                {
                    let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                    
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                    
                    //accept_rej_date_time
                    
                    if type == "user"
                    {
                        cell.Lbl_Creation_Name.text = "Order Rejected By" + " " + "Worker"
                    }
                    else
                    {
                        cell.Lbl_Creation_Name.text = "Order Rejected By" + " " + "You"
                    }
                    
                    let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    print(resultString)
                    
                    cell.Lbl_Created_Date.text = "\(resultString)"
                    
                    cell.imageview_line.isHidden = true
                    return cell
                }
                
            }
           
            else if self.worker_status == "complete"
            {
                // worker status = "accept"
                
                 let user_rattings_done = self.OrderdetailDict.value(forKey: "user_rattings_done") as! String
                
                if self.type == "user"
                {
                    if user_rattings_done == "0"
                    {
                        if indexPath.section == 0
                        {
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "Completed")
                            
                            cell.Btn_Handler_Call.isHidden = true
                            cell.Btn_Title_Location.isHidden = true
                            cell.imageview_Call.isHidden = true
                            cell.imageview_location.isHidden = true
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Completed "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                        else if indexPath.section == 2
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                        }
                        else if indexPath.section == 3
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let complete_date_time = self.OrderdetailDict.value(forKey: "complete_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                            }
                            
                            let DataDate = self.UTCToLocal(date: complete_date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                        }
                        else
                        {
                            let cell:UserOrderGiveReviewStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderGiveReviewStarXIB", for: indexPath) as! UserOrderGiveReviewStarXIB
                            
                            
                            
                            
                            cell.Txtfield_Review.tag = 10
                            cell.Txtfield_Review.delegate = self
                            
                            
                            cell.View_Textview.clipsToBounds = true
                            cell.View_Textview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.View_Textview.layer.borderWidth = 1.0
                            cell.View_Textview.layer.cornerRadius = 10
                            cell.View_Textview.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.View_Textview.layer.shadowOpacity = 4
                            cell.View_Textview.layer.shadowOffset = CGSize.zero
                            cell.View_Textview.layer.shadowRadius = 4
                            cell.View_Textview.clipsToBounds = true
                            cell.View_Textview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.View_Textview.layer.borderWidth = 2.0
                            cell.View_Textview.clipsToBounds = true
                            cell.View_Textview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.View_Textview.layer.borderWidth = 1.0
                            
                            

                            
                            cell.Btn_Title_Accept_Reject.layer.cornerRadius = 10
                            cell.Btn_Title_Accept_Reject.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
                            cell.Btn_Title_Accept_Reject.layer.shadowOpacity = 4
                            cell.Btn_Title_Accept_Reject.layer.shadowOffset = CGSize.zero
                            cell.Btn_Title_Accept_Reject.layer.shadowRadius = 2
                            cell.Btn_Title_Accept_Reject.clipsToBounds = true
                            cell.Btn_Title_Accept_Reject.layer.masksToBounds = false
                            cell.Btn_Title_Accept_Reject.clipsToBounds = true
                            
                            
                            cell.View_Ratings_Bar.didTouchCosmos = didTouchCosmos
                            
                            
                            
                            cell.Btn_Title_Accept_Reject.addTarget(self,action:#selector(self.PostReview(sender:)), for: .touchUpInside)
                            
                            return cell
                            
                            
                        }
                        
                        
                        // worker status = "accept"
                        // show completed button
                    }
                    else
                    {
                        
                            
                            
                            if indexPath.section == 0
                            {
                                let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                                
                                cell.imageview_status.image = UIImage(named: "incomplete_process")
                                
                                cell.Btn_Handler_Call.isHidden = true
                                cell.Btn_Title_Location.isHidden = true
                                cell.imageview_Call.isHidden = true
                                cell.imageview_location.isHidden = true
                                
                                
                                cell.imageview_status.image = UIImage(named: "Completed")
                                
                                print(self.OrderdetailDict)
                                
                                let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                                let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                                let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                                let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                //address
                                let address = self.OrderdetailDict.value(forKey: "address") as! String
                                let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                
                                
                                if type == "user"
                                {
                                    cell.Lbl_Detail.text = "Worker Detail:"
                                    cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                    cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                    self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                    let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                    cell.Customer_Imageview.kf.indicatorType = .activity
                                    cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                    //worker_name
                                    //worker_mobile
                                }
                                else
                                {
                                    cell.Lbl_Detail.text = "Customer Detail:"
                                    cell.Lbl_Customer_Name.text = "\(username)"
                                    cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                    self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                    cell.Customer_Imageview.kf.indicatorType = .activity
                                    cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                    
                                    //user_mobile
                                }
                                
                                
                                //wlat
                                //wlng
                                //specific_address
                                
                                //worker_mobile
                                let DataDate = self.UTCToLocal(date: date_time)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                
                                //date_time
                                cell.Lbl_Order_Status_Header.text =  "Order" + " Completed "
                                cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                                cell.Lbl_Order_Date.text = "\(resultString)"
                                cell.Customer_Imageview.layer.cornerRadius = 32.5
                                cell.Customer_Imageview.clipsToBounds = true
                                cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                                cell.Customer_Imageview.layer.borderWidth = 3.0
                                
                                
                                
                                cell.Btn_Handler_Call.tag = indexPath.row
                                cell.Btn_Title_Location.tag = indexPath.row
                                
                                
                                cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                                cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                                
                                return cell
                            }
                            else if indexPath.section == 1
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                                
                                
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                                }
                                
                                let DataDate = self.UTCToLocal(date: insert_date)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                cell.imageview_line.isHidden = false
                                
                                return cell
                                
                            }
                            else if indexPath.section == 2
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                                
                                //accept_rej_date_time
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                                }
                                
                                let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                
                                cell.imageview_line.isHidden = false
                                return cell
                            }
                            else if indexPath.section == 3
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let complete_date_time = self.OrderdetailDict.value(forKey: "complete_date_time") as! String
                                
                                //accept_rej_date_time
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                                }
                                
                                let DataDate = self.UTCToLocal(date: complete_date_time)

                                
                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                
                                cell.imageview_line.isHidden = false
                                return cell
                            }
                            else
                            {
                                let cell:WorkOrderHistoryReviewWithStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkOrderHistoryReviewWithStarXIB", for: indexPath) as! WorkOrderHistoryReviewWithStarXIB
                                
                                print(self.OrderdetailDict)
                                
                                
                                let ratting = self.OrderdetailDict.value(forKey: "ratting") as! String
                                let review = self.OrderdetailDict.value(forKey: "review") as! String
                                let ratting_date_time = self.OrderdetailDict.value(forKey: "ratting_date_time") as! String
                                
                                let DataDate = self.UTCToLocal(date: ratting_date_time)

                                
                                cell.View_Cosmos.rating = Double(ratting)!
                                cell.Lbl_Description.text = "\(review)"
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let date = dateFormatter.date (from: DataDate)
                                
                                let String1 = date!.timeAgoSinceNow
                                print(String1)
                                cell.Lbl_Date.text = "\(String1)"
                                
                                cell.Lbl_Review.text = "\(ratting)" + "/5"
                                
                                
                                return cell
                                
                                
                            }
                            
                            
                            // worker status = "accept"
                            // show completed button
                            
                            
                        
                    }
                }
                else
                {
                    if user_rattings_done == "0"
                    {
                        if indexPath.section == 0
                        {
                            let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                            
                            cell.imageview_status.image = UIImage(named: "incomplete_process")
                            
                            cell.Btn_Handler_Call.isHidden = true
                            cell.Btn_Title_Location.isHidden = true
                            cell.imageview_Call.isHidden = true
                            cell.imageview_location.isHidden = true
                            
                            cell.imageview_status.image = UIImage(named: "Completed")
                            
                            print(self.OrderdetailDict)
                            
                            let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                            let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                            let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                            let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            //address
                            let address = self.OrderdetailDict.value(forKey: "address") as! String
                            let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                            self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Detail.text = "Worker Detail:"
                                cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                //worker_name
                                //worker_mobile
                            }
                            else
                            {
                                cell.Lbl_Detail.text = "Customer Detail:"
                                cell.Lbl_Customer_Name.text = "\(username)"
                                cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                cell.Customer_Imageview.kf.indicatorType = .activity
                                cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                
                                //user_mobile
                            }
                            
                            
                            //wlat
                            //wlng
                            //specific_address
                            
                            //worker_mobile
                            
                            let DataDate = self.UTCToLocal(date: date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            
                            //date_time
                            cell.Lbl_Order_Status_Header.text =  "Order" + " Completed "
                            cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                            cell.Lbl_Order_Date.text = "\(resultString)"
                            cell.Customer_Imageview.layer.cornerRadius = 32.5
                            cell.Customer_Imageview.clipsToBounds = true
                            cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                            cell.Customer_Imageview.layer.borderWidth = 3.0
                            
                            
                            
                            cell.Btn_Handler_Call.tag = indexPath.row
                            cell.Btn_Title_Location.tag = indexPath.row
                            
                            
                            cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                            cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                            
                            return cell
                        }
                        else if indexPath.section == 1
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                            
                            
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                            }
                            
                            let DataDate = self.UTCToLocal(date: insert_date)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            cell.imageview_line.isHidden = false
                            
                            return cell
                            
                        }
                        else if indexPath.section == 2
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                            }
                            
                            let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            
                            cell.imageview_line.isHidden = false
                            return cell
                        }
                        else if indexPath.section == 3
                        {
                            let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                            
                            let username = self.OrderdetailDict.value(forKey: "username") as! String
                            let complete_date_time = self.OrderdetailDict.value(forKey: "complete_date_time") as! String
                            
                            //accept_rej_date_time
                            
                            if type == "user"
                            {
                                cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                            }
                            else
                            {
                                cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                            }
                            
                            let DataDate = self.UTCToLocal(date: complete_date_time)

                            
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let showDate = inputFormatter.date(from: DataDate)
                            inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                            let resultString = inputFormatter.string(from: showDate!)
                            print(resultString)
                            
                            cell.Lbl_Created_Date.text = "\(resultString)"
                            
                            cell.imageview_line.isHidden = false
                            return cell
                        }
                        else
                        {
                            let cell:NoRatingsXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "NoRatingsXIB", for: indexPath) as! NoRatingsXIB
                            
                          cell.imageview_Dot.isHidden = false
                            
                            return cell
                            
                            
                        }
                    }
                    else
                    {
                        
                            
                            
                            if indexPath.section == 0
                            {
                                let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                                
                                cell.imageview_status.image = UIImage(named: "incomplete_process")
                                
                                cell.Btn_Handler_Call.isHidden = true
                                cell.Btn_Title_Location.isHidden = true
                                cell.imageview_Call.isHidden = true
                                cell.imageview_location.isHidden = true
                                
                                cell.imageview_status.image = UIImage(named: "Completed")
                                
                                print(self.OrderdetailDict)
                                
                                let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                                let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                                let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                                let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                //address
                                let address = self.OrderdetailDict.value(forKey: "address") as! String
                                let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                                self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                
                                
                                if type == "user"
                                {
                                    cell.Lbl_Detail.text = "Worker Detail:"
                                    cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                                    cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                    self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                                    let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                                    cell.Customer_Imageview.kf.indicatorType = .activity
                                    cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                                    //worker_name
                                    //worker_mobile
                                }
                                else
                                {
                                    cell.Lbl_Detail.text = "Customer Detail:"
                                    cell.Lbl_Customer_Name.text = "\(username)"
                                    cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                                    self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                                    cell.Customer_Imageview.kf.indicatorType = .activity
                                    cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                                    
                                    //user_mobile
                                }
                                
                                
                                //wlat
                                //wlng
                                //specific_address
                                
                                //worker_mobile
                                let DataDate = self.UTCToLocal(date: date_time)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                
                                //date_time
                                cell.Lbl_Order_Status_Header.text =  "Order" + " Completed "
                                cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                                cell.Lbl_Order_Date.text = "\(resultString)"
                                cell.Customer_Imageview.layer.cornerRadius = 32.5
                                cell.Customer_Imageview.clipsToBounds = true
                                cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                                cell.Customer_Imageview.layer.borderWidth = 3.0
                                
                                
                                
                                cell.Btn_Handler_Call.tag = indexPath.row
                                cell.Btn_Title_Location.tag = indexPath.row
                                
                                
                                cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                                cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                                
                                return cell
                            }
                            else if indexPath.section == 1
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                                
                                
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                                }
                                let DataDate = self.UTCToLocal(date: insert_date)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                cell.imageview_line.isHidden = false
                                
                                return cell
                                
                            }
                            else if indexPath.section == 2
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                                
                                //accept_rej_date_time
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Accepted By" + " " + "Worker"
                                }
                                let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                
                                cell.imageview_line.isHidden = false
                                return cell
                            }
                            else if indexPath.section == 3
                            {
                                let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                                
                                let username = self.OrderdetailDict.value(forKey: "username") as! String
                                let complete_date_time = self.OrderdetailDict.value(forKey: "complete_date_time") as! String
                                
                                //accept_rej_date_time
                                
                                if type == "user"
                                {
                                    cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                                }
                                else
                                {
                                    cell.Lbl_Creation_Name.text = "Order Completed By" + " " + "Worker"
                                }
                                  let DataDate = self.UTCToLocal(date: complete_date_time)
                                
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let showDate = inputFormatter.date(from: DataDate)
                                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                                let resultString = inputFormatter.string(from: showDate!)
                                print(resultString)
                                
                                cell.Lbl_Created_Date.text = "\(resultString)"
                                
                                cell.imageview_line.isHidden = false
                                return cell
                            }
                            else
                            {
                                let cell:WorkOrderHistoryReviewWithStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkOrderHistoryReviewWithStarXIB", for: indexPath) as! WorkOrderHistoryReviewWithStarXIB
                                
                                print(self.OrderdetailDict)
                                
                                
                                let ratting = self.OrderdetailDict.value(forKey: "ratting") as! String
                                let review = self.OrderdetailDict.value(forKey: "review") as! String
                                let ratting_date_time = self.OrderdetailDict.value(forKey: "ratting_date_time") as! String
                                let DataDate = self.UTCToLocal(date: ratting_date_time)
                                
                                
                                cell.View_Cosmos.rating = Double(ratting)!
                                cell.Lbl_Description.text = "\(review)"
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let date = dateFormatter.date (from: DataDate)
                                
                                let String1 = date!.timeAgoSinceNow
                                print(String1)
                                cell.Lbl_Date.text = "\(String1)"
                                
                                cell.Lbl_Review.text = "\(ratting)" + "/5"
                                
                                
                                return cell
                                
                                
                            }
                            
                            
                            // worker status = "accept"
                            // show completed button
                            
                            
                        
                    }
                    
                }
                
                
                
               
             
                
                
                
            }
                
            else if self.worker_status == "cancel"
            {
                if indexPath.section == 0
                {
                    let cell:WorkerOrderCompletedHeaderXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "WorkerOrderCompletedHeaderXIB", for: indexPath) as! WorkerOrderCompletedHeaderXIB
                    
                    cell.imageview_status.image = UIImage(named: "incomplete_process-1")
                    
                    print(self.OrderdetailDict)
                    cell.Btn_Handler_Call.isHidden = false
                    cell.Btn_Title_Location.isHidden = false
                    cell.imageview_Call.isHidden = false
                    cell.imageview_location.isHidden = false
                    
                    
                    let worker_status = self.OrderdetailDict.value(forKey: "worker_status") as! String
                    let order_id = self.OrderdetailDict.value(forKey: "order_id") as! String
                    let date_time = self.OrderdetailDict.value(forKey: "date_time") as! String
                    let userimage = self.OrderdetailDict.value(forKey: "userimage") as! String
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    //address
                    let address = self.OrderdetailDict.value(forKey: "address") as! String
                    let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
                    self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                    
                    
                    if type == "user"
                    {
                        cell.Lbl_Detail.text = "Worker Detail:"
                        cell.Lbl_Customer_Name.text = self.OrderdetailDict.value(forKey: "worker_name") as? String
                        cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                        self.CallNumber = self.OrderdetailDict.value(forKey: "worker_mobile") as! String
                        let workerimage = self.OrderdetailDict.value(forKey: "workerimage") as! String
                        cell.Customer_Imageview.kf.indicatorType = .activity
                        cell.Customer_Imageview.kf.setImage(with: URL(string: workerimage ))
                    }
                    else
                    {
                        cell.Lbl_Detail.text = "Customer Detail:"
                        cell.Lbl_Customer_Name.text = "\(username)"
                        cell.Lbl_Full_Address.text = "\(specific_address)" + " , " + "\(address)"
                        self.CallNumber = self.OrderdetailDict.value(forKey: "user_mobile") as! String
                        
                        cell.Customer_Imageview.kf.indicatorType = .activity
                        cell.Customer_Imageview.kf.setImage(with: URL(string: userimage ))
                        
                        //user_mobile
                    }
                    
                    
                    //wlat
                    //wlng
                    //specific_address
                    
                    //worker_mobile
                    let DataDate = self.UTCToLocal(date: date_time)
                    
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    print(resultString)
                    
                    
                    //date_time
                    cell.Lbl_Order_Status_Header.text =  "Order" + " Completed "
                    cell.Lbl_Order_Numberr.text = "#" +  "\(order_id)"
                    cell.Lbl_Order_Date.text = "\(resultString)"
                    cell.Customer_Imageview.layer.cornerRadius = 32.5
                    cell.Customer_Imageview.clipsToBounds = true
                    cell.Customer_Imageview.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                    cell.Customer_Imageview.layer.borderWidth = 3.0
                    
                    
                    cell.Btn_Handler_Call.tag = indexPath.row
                    cell.Btn_Title_Location.tag = indexPath.row
                    
                    
                    cell.Btn_Handler_Call.addTarget(self,action:#selector(self.GETCALL(sender:)), for: .touchUpInside)
                    cell.Btn_Title_Location.addTarget(self,action:#selector(self.GETMAP(sender:)), for: .touchUpInside)
                    
                    return cell
                }
                else if indexPath.section == 1
                {
                    let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                    
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    let insert_date = self.OrderdetailDict.value(forKey: "insert_date") as! String
                    
                    
                    
                    if type == "user"
                    {
                        cell.Lbl_Creation_Name.text = "Order Created By" + " " + "You"
                    }
                    else
                    {
                        cell.Lbl_Creation_Name.text = "Order Created By" + " " + "\(username)"
                    }
                    
                    let DataDate = self.UTCToLocal(date: insert_date)
                    
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    print(resultString)
                    
                    cell.Lbl_Created_Date.text = "\(resultString)"
                    cell.imageview_line.isHidden = false
                    
                    return cell
                    
                }
                else
                {
                    let cell:UserOrderReviewWithoutStarXIB = Tableview_Order_Complete.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
                    
                    let username = self.OrderdetailDict.value(forKey: "username") as! String
                    let accept_rej_date_time = self.OrderdetailDict.value(forKey: "accept_rej_date_time") as! String
                    
                    
                    let DataDate = self.UTCToLocal(date: accept_rej_date_time)

                    
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: DataDate)
                    inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    let resultString = inputFormatter.string(from: showDate!)
                    if type == "user"
                    {
                        cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "Worker"
                    }
                    else
                    {
                        cell.Lbl_Creation_Name.text = "Order Cancelled By" + " " + "Worker"
                    }
                    cell.Lbl_Created_Date.text = "\(resultString)"
                    cell.imageview_line.isHidden = true
                    
                    return cell
                }
            }
            else
            {
                return UITableViewCell()
            }
           
            
            
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.worker_status == "pending"
        {
            if indexPath.section == 0
            {
                return UITableView.automaticDimension
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        }
        else if self.worker_status == "cancel"
        {
            // NEEDED CHECK
            if indexPath.section == 0
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 1
            {
                return UITableView.automaticDimension
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        }
        else if self.worker_status == "accept"
        {
            
            if self.User_Status == "cancel"
            {
                if self.type == "user"
                {
                    return UITableView.automaticDimension
                }
                else
                {
                    return UITableView.automaticDimension
                }
            }
            else
            {
                if self.type == "user"
                {
                    return UITableView.automaticDimension
                }
                else
                {
                    return UITableView.automaticDimension
                }
            }
            
            // worker status = "accept"
            // show completed button
        }
        else if self.worker_status == "reject"
        {
            if indexPath.section == 0
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 1
            {
                return UITableView.automaticDimension
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        }
        else if self.worker_status == "modify_date"
        {
            // NEEDED CHECK
            if indexPath.section == 0
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 1
            {
                return UITableView.automaticDimension
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        }
        else if self.worker_status == "complete"
        {
            // worker status = "accept"
            
            let user_rattings_done = self.OrderdetailDict.value(forKey: "user_rattings_done") as! String
            if user_rattings_done == "0"
            {
                if indexPath.section == 0
                {
                    return UITableView.automaticDimension
                }
                else if indexPath.section == 1
                {
                    return UITableView.automaticDimension
                }
                else if indexPath.section == 2
                {
                    return UITableView.automaticDimension
                }
                else
                {
                    return UITableView.automaticDimension
                }
            }
            else
            {
                if indexPath.section == 0
                {
                    return UITableView.automaticDimension
                }
                else if indexPath.section == 1
                {
                    return UITableView.automaticDimension
                }
                else if indexPath.section == 2
                {
                    return UITableView.automaticDimension
                }
                else
                {
                    return UITableView.automaticDimension
                }
            }
            
            
            
        }
            
        else if self.worker_status == "cancel"
        {
            if indexPath.section == 0
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 1
            {
                return UITableView.automaticDimension
            }
            else
            {
                return UITableView.automaticDimension
            }
        }
            
        else
        {
            return UITableView.automaticDimension
            
        }
    }
    
}
