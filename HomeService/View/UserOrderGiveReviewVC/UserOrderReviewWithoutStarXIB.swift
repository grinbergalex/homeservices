//
//  UserOrderReviewWithoutStarXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class UserOrderReviewWithoutStarXIB: UITableViewCell {

    @IBOutlet weak var imageview_line: UIImageView!
    @IBOutlet weak var Lbl_Created_Date: UILabel!
    @IBOutlet weak var Lbl_Creation_Name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
