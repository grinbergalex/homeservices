//
//  UserOrderGiveReviewVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class UserOrderGiveReviewVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- OUTLETS
    
    
    @IBOutlet weak var Tableview_Review: UITableView!
    
    //MARK:- VARIABLES
    var type = String()

    
    //MARK:- VIEW DID LOAD

  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Tableview_Review.register(UserOrderGIveReviewHeaderXIB.self, forCellReuseIdentifier: "UserOrderGIveReviewHeaderXIB")
        Tableview_Review.register(UINib(nibName: "UserOrderGIveReviewHeaderXIB", bundle: nil), forCellReuseIdentifier: "UserOrderGIveReviewHeaderXIB")
        
        Tableview_Review.register(UserOrderReviewWithoutStarXIB.self, forCellReuseIdentifier: "UserOrderReviewWithoutStarXIB")
        Tableview_Review.register(UINib(nibName: "UserOrderReviewWithoutStarXIB", bundle: nil), forCellReuseIdentifier: "UserOrderReviewWithoutStarXIB")
        
        Tableview_Review.register(UserOrderGiveReviewStarXIB.self, forCellReuseIdentifier: "UserOrderGiveReviewStarXIB")
        Tableview_Review.register(UINib(nibName: "UserOrderGiveReviewStarXIB", bundle: nil), forCellReuseIdentifier: "UserOrderGiveReviewStarXIB")
        

        type = ApiUtillity.sharedInstance.getUserData(key: "type")
        print(type)
        
        if type == "user"
        {
            
        }
        else
        {
            
        }
        
        // Do any additional setup after loading the view.
    }


    //MARK:- ALL FUNCTIONS
    
    @objc func NAVIGAIGATIONMAP(sender:UIButton)
    {
      let Push = NavigationScreenXIB()
        self.navigationController?.pushViewController(Push, animated: true)
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- TABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 5
        }
        else
        {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cell:UserOrderGIveReviewHeaderXIB = Tableview_Review.dequeueReusableCell(withIdentifier: "UserOrderGIveReviewHeaderXIB", for: indexPath) as! UserOrderGIveReviewHeaderXIB
            
              cell.Btn_Title_Navigation.addTarget(self,action:#selector(self.NAVIGAIGATIONMAP(sender:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell:UserOrderReviewWithoutStarXIB = Tableview_Review.dequeueReusableCell(withIdentifier: "UserOrderReviewWithoutStarXIB", for: indexPath) as! UserOrderReviewWithoutStarXIB
            
            return cell
            
            
        }
        else
        {
            let cell:UserOrderGiveReviewStarXIB = Tableview_Review.dequeueReusableCell(withIdentifier: "UserOrderGiveReviewStarXIB", for: indexPath) as! UserOrderGiveReviewStarXIB
            
            
            cell.Txtfield_Review.clipsToBounds = true
            cell.Txtfield_Review.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.Txtfield_Review.layer.borderWidth = 1.0
            
            
            
            
            cell.Btn_Title_Accept_Reject.layer.cornerRadius = 10
            cell.Btn_Title_Accept_Reject.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.Btn_Title_Accept_Reject.layer.shadowOpacity = 4
            cell.Btn_Title_Accept_Reject.layer.shadowOffset = CGSize.zero
            cell.Btn_Title_Accept_Reject.layer.shadowRadius = 1
   cell.Btn_Title_Accept_Reject.clipsToBounds = true
            cell.Btn_Title_Accept_Reject.layer.masksToBounds = false
            
            
            return cell
            
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 460
        }
        else if indexPath.section == 1
        {
            return 70
        }
        else
        {
            return 200
        }
    }
    

}
