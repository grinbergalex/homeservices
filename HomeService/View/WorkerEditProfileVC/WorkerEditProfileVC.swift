//
//  WorkerEditProfileVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import MobileCoreServices



class WorkerEditProfileVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK:- OUTLETS
    @IBOutlet weak var imageview_pic: UIImageView!
    
    @IBOutlet weak var Txt_Name: UITextField!
    @IBOutlet weak var Txt_Worker_Cities: UITextField!
    @IBOutlet weak var Txt_Email_Address: UITextField!
    @IBOutlet weak var Txt_Proficiency: UITextField!
    @IBOutlet weak var Txt_Mobile_Number: UITextField!
    @IBOutlet weak var Collectionview_Images: UICollectionView!
    
    
    
    
    //MARK:- VARIABLES
    var type = String()
    var imageData_pf_pic = Data()
    let picker = UIImagePickerController()
    var imageData = Data()
     var change_image = Bool()
    var change_index = Int()
    var lati = Double()
    var long = Double()
      //var imageArray = NSMutableArray()
    var imageVideoArray:NSMutableArray = NSMutableArray()
    var deletedImageIDArray = [String]()
     var Is_Edit_id = String()
    var apiToken = String()
    var Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
    var Is_Only_Profile_Picture = Bool()
    var FinalCommaaSeprated = String()
    
    
    

    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.Collectionview_Images.register(UINib(nibName: "ImageCellXIB", bundle: nil), forCellWithReuseIdentifier: "ImageCellXIB")
        
        lati = UserDefaults.standard.double(forKey: "LATTITUDE")
        long = UserDefaults.standard.double(forKey: "LONGITUDE")
        
          apiToken = "Bearer \(Token)"
        
        Is_Only_Profile_Picture = false
        MyProfile()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
      
    }

    //MARK:- ALL FUNCTIONS
    
    func openPhotoSelect() {
        
        
        let actionSheetController = UIAlertController(title: "Home Services", message: "Please Select Image", preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let takePictureAction = UIAlertAction(title: "Open Camera", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(takePictureAction)
        
        // Create and add a second option action
        let choosePictureAction = UIAlertAction(title: "Open Gallery", style: .default) { action -> Void in
            self.openGallary()
        }
        actionSheetController.addAction(choosePictureAction)
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func MyProfile()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            self.apiToken = "Bearer \(self.Token)"
            let headers = ["Vauthtoken":self.apiToken]
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/my_profile"), method: .get, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        let user_Data = dict.value(forKey: "data") as! NSDictionary
                        
                        
                        
                        self.Txt_Name.text = user_Data.value(forKey: "name") as? String
                        self.Txt_Proficiency.text = user_Data.value(forKey: "proficiency") as? String
                        self.Txt_Email_Address.text = user_Data.value(forKey: "email") as? String
                        self.Txt_Mobile_Number.text = user_Data.value(forKey: "mobile") as? String
                        self.Txt_Worker_Cities.text = user_Data.value(forKey: "working_city") as? String
                        
                        let image_url = user_Data.value(forKey: "profile_image")
                        self.imageview_pic.kf.indicatorType = .activity
                        self.imageview_pic.kf.setImage(with: URL(string: image_url as! String))
                        
                        self.imageview_pic.layer.cornerRadius = 45
                        self.imageview_pic.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                        self.imageview_pic.layer.shadowOpacity = 4
                        self.imageview_pic.layer.shadowOffset = CGSize.zero
                        self.imageview_pic.layer.shadowRadius = 4
                        self.imageview_pic.clipsToBounds = true
                        //cell.user_pf_image_view.layer.masksToBounds = false
                        self.imageview_pic.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                        self.imageview_pic.layer.borderWidth = 3.0
                        
                        
                        let TempImageArray = user_Data.value(forKey: "worker_images") as! NSArray
                        
                        self.imageVideoArray = TempImageArray.mutableCopy() as! NSMutableArray
                        
                        if self.imageVideoArray.count == 0
                        {
                            self.Collectionview_Images.isHidden = true
                        }
                        else
                        {
                            let tempArray = NSMutableArray()
                            for item in TempImageArray
                            {
                                let tempDic = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                tempDic.setValue("1", forKey: "isEdit")
                                /*
                                if (tempDic.value(forKey: "type") as! String) == "image" {
                                    tempDic.setValue((tempDic.value(forKey: "image") as! String), forKey: "data")
                                }
                                else
                                {
                                    
                                }
 */
                                
                                tempArray.add(tempDic)
                            }
                            
                            self.imageVideoArray = tempArray.mutableCopy() as! NSMutableArray
                            self.Collectionview_Images.isHidden = false
                            self.Collectionview_Images.reloadData()
                            
                        }
                        
                        
                        ApiUtillity.sharedInstance.setUserData(data: user_Data)
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        
                        
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                        
                        
                    else if StatusCode==412
                    {
                        
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                        
                    else
                    {
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    func openGallary()
    {
        //        self.navigationController?.isNavigationBarHidden = false
        //        self.navigationItem.hidesBackButton = false
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = ["public.image"]
        present(picker, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            picker.mediaTypes = ["public.image"]
            present(picker,animated: true,completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let type:String = info[UIImagePickerController.InfoKey.mediaType] as! String
        
        
        if self.Is_Only_Profile_Picture == true
        {
            let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            print("here image is \(String(describing: image))")
            
//            imageData = UIImageJPEGRepresentation(image!, 0.25)!
            print("here image data is \(String(describing: imageData))")
            print("Image Selected")
            
            self.imageview_pic.image = ApiUtillity.sharedInstance.imageOrientation(image!)
            
            self.imageview_pic.layer.cornerRadius = 45
            self.imageview_pic.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            self.imageview_pic.layer.shadowOpacity = 4
            self.imageview_pic.layer.shadowOffset = CGSize.zero
            self.imageview_pic.layer.shadowRadius = 4
            self.imageview_pic.clipsToBounds = true
            self.imageview_pic.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            self.imageview_pic.layer.borderWidth = 3.0
            
          //  imageData =  self.imageview_pic.image!.jpegData(compressionQuality: 0.75)!
            
            
            imageData =  ApiUtillity.sharedInstance.imageOrientation(image!).jpegData(compressionQuality: 0.75)!
            
            
            self.dismiss(animated: true, completion: nil)
            
          
            
        }
        else
        {
            if type == (kUTTypeMovie as String)
            {
                
            }
            else {
                
                
                var selectedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                print(selectedImage)
                
                selectedImage = ApiUtillity.sharedInstance.imageOrientation(selectedImage)
                
                let uploadData = selectedImage.jpegData(compressionQuality: 0.10)
                selectedImage = UIImage(data: uploadData!)!
                
                
                
                imageVideoArray.add(["isEdit":"0", "id":"", "type":"image", "data":selectedImage])
                
                
                // imageData = UIImageJPEGRepresentation(image!, 0.25)!
               
                Collectionview_Images.reloadData()
                Collectionview_Images.isHidden = false

                
            }
            
            
            
        }
      
        dismiss(animated: true, completion: nil)
    }
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    
    func EditProfile()
    {
        let url = ApiUtillity.sharedInstance.API(Join: "user/edit_profile")
        var stringWithCommas = String()
        if self.imageVideoArray.count == 0
        {
            stringWithCommas = String()
        }
        else
        {
            
            let Array = self.imageVideoArray.value(forKey: "id") as! NSArray
             stringWithCommas = (Array as NSArray).componentsJoined(by: ",")
            
        }
        
        let params = ["email":Txt_Email_Address.text!,"name":Txt_Name.text!,"mobile":Txt_Mobile_Number.text!,"address":"","latitude":"\(lati)","longitude":"\(long)","working_city":Txt_Worker_Cities.text!,"proficiency":Txt_Proficiency.text!,"wimg_ids":self.FinalCommaaSeprated] as [String : String]
        print(params)
        
        let heder:HTTPHeaders = ["Content-Type": "application/json","Vauthtoken":apiToken]

        print(heder)
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            DispatchQueue.main.sync {
                
                  multipartFormData.append(self.imageData, withName: "profile_image", fileName: "image.jpg", mimeType: "image/jpg")
                
              
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                
                for item in self.imageVideoArray.reversed() {
                    if ((item as! NSDictionary).value(forKey: "isEdit") as! String) == "1"
                    {
                        self.imageVideoArray.remove(item)
                    }
                }
                
                var imageInc = 0
                for item in self.imageVideoArray
                {
                    if ((item as! NSDictionary).value(forKey: "type") as! String) == "image"
                    {
                        multipartFormData.append(((item as! NSDictionary).value(forKey: "data") as! UIImage).pngData()!, withName: "work_images[\(imageInc)]", fileName: "work_images\(imageInc).png",mimeType: "image/png")
                        imageInc = imageInc + 1
                    }
                    else if ((item as! NSDictionary).value(forKey: "type") as! String) == "video" {
                        
                    }
                }
            }
        }, usingThreshold: UInt64(), to: url, method: .post, headers: heder) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil {
                        let Dic:NSDictionary = response.result.value as! NSDictionary
                        print(Dic)
                        if Dic.value(forKey: "status") as! Int == 200
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: "Success")
                            let user_Data = Dic.value(forKey: "data") as! NSDictionary
                            ApiUtillity.sharedInstance.setUserData(data: user_Data)
                            
                         self.navigationController?.popViewController(animated: true)
                            
                        }
                        else if Dic.value(forKey: "status") as! Int == 401
                        {
                            let ErrorDic:NSDictionary = Dic.value(forKey: "message") as! NSDictionary
                            let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                            
                        }
                        else {
                            let ErrorDic = Dic.value(forKey: "message") as! NSDictionary
                            let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        }
                    }
                    else {
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithAPIError(error: response.error! as NSError)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    @objc func selectRemoveImage(_ sender: UIButton)
    {
        let dic = (imageVideoArray.object(at: sender.tag) as! NSDictionary)
        print(dic)
        
        var stringWithCommas = String()

        
        //FinalCommaaSeprated
        
        
        
      //  stringWithCommas = (Array as NSArray).componentsJoined(by: ",")
        
        if (dic.value(forKey: "isEdit") as! String) == "1"
        {
          //  deletedImageIDArray.append((dic.value(forKey: "id") as! String))
            
            
            print(dic.value(forKey: "id") as! String)
            
            
            deletedImageIDArray.append((dic.value(forKey: "id") as! String))

            
           
            // self.FinalCommaaSeprated = ((imageVideoArray.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as! String).componentsJoined(by: ",")
            
            
            
        }
        imageVideoArray.removeObject(at: sender.tag)
        print(imageVideoArray)
        print(imageVideoArray.count)
        print(deletedImageIDArray)
        print(deletedImageIDArray.count)
        
        
        let Array = self.deletedImageIDArray as NSArray
        stringWithCommas = (Array ).componentsJoined(by: ",")
        print(stringWithCommas)
        
        self.FinalCommaaSeprated = stringWithCommas
        
        Collectionview_Images.reloadData()
    }
    
    
    //    MARK:- COLLECTION MATHOD
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageVideoArray.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:ImageCellXIB = Collectionview_Images.dequeueReusableCell(withReuseIdentifier: "ImageCellXIB", for: indexPath) as! ImageCellXIB
        
        print(imageVideoArray)
        
            if ((imageVideoArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "isEdit") as! String) == "0"
            {
                if ((imageVideoArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "type") as! String) == "image"{
                     cell.image_view.image = ((imageVideoArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "data") as! UIImage)
                }
                else
                {
                    
            }
            }
            else
            {
                  cell.image_view.kf.indicatorType = .activity
                    cell.image_view.kf.setImage(with: URL(string: ((imageVideoArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as! String))!)
                
                
            }
            
            cell.Btn_Handler_Remove_Image.addTarget(self, action: #selector(selectRemoveImage(_:)), for: .touchUpInside)
            cell.Btn_Handler_Remove_Image.tag = indexPath.row
        
        //    ApiUtillity.sharedInstance.setCornurRadius(obj: cell.View_Background, cornurRadius: 5, isClipToBound: true, borderColor: "00CE00", borderWidth: 1)
            
            return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            return CGSize(width: 75 , height: 75)
    }
  
    
    
    
    //MARK:- BUTTON  ACTIONS
    

    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Btn_Handler_Save_Continue(_ sender: Any)
    {
        if Txt_Name.text?.isEmpty == true
            {
             ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Name", ForNavigation: self.navigationController!)
                return
        }

         else if Txt_Email_Address.text?.isEmpty == true
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
                return
             }

        else if ApiUtillity.sharedInstance.isValidEmail(testStr: Txt_Email_Address.text!) == false
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Valid Email Address", ForNavigation: self.navigationController!)
            return
        }



       else if Txt_Mobile_Number.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Mobile Number", ForNavigation: self.navigationController!)
            return
        }

        else if Txt_Proficiency.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Proficiency", ForNavigation: self.navigationController!)
            return
        }

       else if Txt_Worker_Cities.text?.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter City Name", ForNavigation: self.navigationController!)
            return
        }
        
        EditProfile()
        
    }
    
    @IBAction func Btn_Handler_Select_Profile_Picture(_ sender: Any)
    {
        let alert = UIAlertController(title: "Please Select Image", message: "", preferredStyle: .actionSheet)
        // alert.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
        alert.addAction(UIAlertAction(title: "Open Camera", style: .default, handler: { (UIAlertAction) in
            
            let picker:UIImagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                //  picker.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
                picker.delegate = self
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.camera
                self.present(picker, animated: true, completion: nil)
                
                self.Is_Only_Profile_Picture = true
            }
        }))
        alert.addAction(UIAlertAction(title: "Open Gallary", style: .default, handler: { (UIAlertAction) in
            let picker:UIImagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                //   picker.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
                picker.delegate = self
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.present(picker, animated: true, completion: nil)
                
                self.Is_Only_Profile_Picture = true
                
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:
            { (UIAlertAction) in
               self.Is_Only_Profile_Picture = false
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
        
        
    
    @IBAction func Btn_Handler_Select_Image(_ sender: Any)
    {
        let alert = UIAlertController(title: "Choose Image ", message: "", preferredStyle: .actionSheet)
        // alert.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            
            let picker:UIImagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                //  picker.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
                picker.delegate = self
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.camera
                self.present(picker, animated: true, completion: nil)
                
                self.Is_Only_Profile_Picture = false
            }
        }))
        alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (UIAlertAction) in
            let picker:UIImagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                //   picker.view.tintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "000000")
                picker.delegate = self
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.present(picker, animated: true, completion: nil)
                
                self.Is_Only_Profile_Picture = false
                
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
}
