//
//  UserOrderRunningVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class UserOrderRunningVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Tableview_Orderrunning: UITableView!
    
    //MARK:- VARIABLES
    
    //MARK:- VIEW DID LOAD

    override func viewDidLoad() {
        super.viewDidLoad()

        Tableview_Orderrunning.register(UserOrderRunningHeaderXIB.self, forCellReuseIdentifier: "UserOrderRunningHeaderXIB")
        Tableview_Orderrunning.register(UINib(nibName: "UserOrderRunningHeaderXIB", bundle: nil), forCellReuseIdentifier: "UserOrderRunningHeaderXIB")
        
        Tableview_Orderrunning.register(UserOrderRunningWithoutStar.self, forCellReuseIdentifier: "UserOrderRunningWithoutStar")
        Tableview_Orderrunning.register(UINib(nibName: "UserOrderRunningWithoutStar", bundle: nil), forCellReuseIdentifier: "UserOrderRunningWithoutStar")
        
        
        // Do any additional setup after loading the view.
    }


  
    // MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            return 2
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cell:UserOrderRunningHeaderXIB = Tableview_Orderrunning.dequeueReusableCell(withIdentifier: "UserOrderRunningHeaderXIB", for: indexPath) as! UserOrderRunningHeaderXIB
            
            cell.Btn_Title_Navigation.addTarget(self,action:#selector(self.NAVIGAIGATIONMAP(sender:)), for: .touchUpInside)
            
            return cell
        }
     
        else
        {
            let cell:UserOrderRunningWithoutStar = Tableview_Orderrunning.dequeueReusableCell(withIdentifier: "UserOrderRunningWithoutStar", for: indexPath) as! UserOrderRunningWithoutStar
            
            if indexPath.row == 0
            {
                cell.imageview_line.isHidden = false
            }
            else
            {
                cell.imageview_line.isHidden = true
            }
            return cell
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 460
        }
        else
        {
            return 70
        }
        
    }
    
    
    
    
    
    //MARK:- ALL FUNCTIONS
    
    
    @objc func NAVIGAIGATIONMAP(sender:UIButton)
    {
        let Push = NavigationScreenXIB()
        self.navigationController?.pushViewController(Push, animated: true)
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
