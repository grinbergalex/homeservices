//
//  OrderListXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class OrderListXIB: UITableViewCell {
    
    
    
    
    @IBOutlet weak var imageview_check: UIImageView!
    @IBOutlet weak var Lbl_Customer_Name: UILabel!
    @IBOutlet weak var Lbl_Order_Date: UILabel!
    @IBOutlet weak var Lbl_Order_Number: UILabel!
    
    @IBOutlet weak var View_Background: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
