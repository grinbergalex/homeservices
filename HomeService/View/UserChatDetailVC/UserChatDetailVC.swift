//
//  UserChatDetailVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications



class UserChatDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UNUserNotificationCenterDelegate
{
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Btn_Title_Accept: UIButton!
    
    @IBOutlet weak var View_Accpet_Rejectview: UIView!
    @IBOutlet weak var Btn_Title_Reject: UIButton!
    
    @IBOutlet weak var Btn_Title_Modify_Time: UIButton!
    @IBOutlet weak var Tableview_Chat: UITableView!
    @IBOutlet weak var lbl_for_title: UILabel!
    //MARK:- VARIABLES
    
    var Order_id = String()
    var chat_array = NSMutableArray()
    let Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
    var apiToken = String()
    var OrderdetailDict = NSDictionary()
    var name = String()
    var type = String()
    var thread_id = String()
    var SelectedDate = String()
    var is_Push_From = String()
 
    
    //MARK:- VIEW DID LOAD
    

    //99 157 185
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_Chat.isHidden = true
        apiToken = "Bearer \(Token)"
        lbl_for_title.text = name
        ApiUtillity.sharedInstance.setCornurRadius(obj: self.Btn_Title_Accept, cornurRadius: 10, isClipToBound: true, borderColor: "639DB9", borderWidth: 3)
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: self.Btn_Title_Reject, cornurRadius: 10, isClipToBound: true, borderColor: "639DB9", borderWidth: 3)
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: self.Btn_Title_Modify_Time, cornurRadius: 10, isClipToBound: true, borderColor: "639DB9", borderWidth: 3)
        
        Tableview_Chat.register(UserSendChatDetailVC.self, forCellReuseIdentifier: "UserSendChatDetailVC")
        Tableview_Chat.register(UINib(nibName: "UserSendChatDetailVC", bundle: nil), forCellReuseIdentifier: "UserSendChatDetailVC")
        
        Tableview_Chat.register(UserRecieveChatDetailXIB.self, forCellReuseIdentifier: "UserRecieveChatDetailXIB")
        Tableview_Chat.register(UINib(nibName: "UserRecieveChatDetailXIB", bundle: nil), forCellReuseIdentifier: "UserRecieveChatDetailXIB")
        
        Tableview_Chat.register(UserChatDetailPleasewaitXIB.self, forCellReuseIdentifier: "UserChatDetailPleasewaitXIB")
        Tableview_Chat.register(UINib(nibName: "UserChatDetailPleasewaitXIB", bundle: nil), forCellReuseIdentifier: "UserChatDetailPleasewaitXIB")
        
        View_Accpet_Rejectview.isHidden = true
        
        UNUserNotificationCenter.current().delegate = self
        
        

        
        
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        OrderDetails()
        
    }
    //MARK:- BUTTON ACTIONS
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Btn_Handler_Modify_Time(_ sender: Any)
    {
        let min = Date()
        // let max = min.addingTimeInterval(31536000) // 1 year
        DPPickerManager.shared.showPicker(title: "Select Date And Time", selected: Date(), min: min, max: nil) { (date, cancel) in
            if !cancel
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
                let SelectedDate1 = dateFormatter.string(from: date!)
                
                print(SelectedDate1)
                
                
                let date2 = Date()
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "dd/MM/yyyy hh:mm a"
                print(date2)
                let result = formatter2.string(from: date2)
                print(result)
                
                if SelectedDate1 == result
                {
                    
                    self.SelectedDate = SelectedDate1
                    self.type = "3"
                    self.Accept_reject_order()
                    print("EQual")
                }
                else
                {
                    self.SelectedDate = SelectedDate1
                    self.type = "3"
                    self.Accept_reject_order()
                    print("Not EQual")
                    
                }
                
                
                
                
                
                
                debugPrint(date as Any)
            }
            else
                
            {
                print("CANCEL")
            }
        }
    }
    @IBAction func Btn_Handler_Reject(_ sender: Any)
    {
        type = "2"
        Accept_reject_order()
    }
    @IBAction func Btn_Handler_Accept(_ sender: Any)
    {
        type = "1"
        Accept_reject_order()
        
    }
    //MARK:- ALL FUNCTIONS
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    //    MARK:- NOTIFICATION
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
        if (data_item.value(forKey: "push_type") as! String) == "3"
        {
            OrderDetails()
        }
        else if (data_item.value(forKey: "push_type") as! String) == "4"
        {
            OrderDetails()
        }
        
        
        //push_type = 1 for place order
        //push_type = 2 for order place for user
        //push_type = 3 for modify date accept by user
        //push_type = 4 for order accepted by worker
        //push_type = 5 order completed
        //push_type = 7 order cancel
        //push_type = 6 review
        
          //OrderDetails()
        
        
        completionHandler([.alert, .sound])
    }

    
    // MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if ApiUtillity.sharedInstance.getUserData(key: "type") == "worker"
        {
            if OrderdetailDict.count == 0
            {
               
                    return 1
                
            }
            else
            {
                if OrderdetailDict.value(forKey: "worker_status") as! String == "pending"
                {
                    return 1
                }
                else
                {
                    return 2
                }
            }
            
            
        }
        else
        {
            return 2

        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
            else if self.OrderdetailDict.count == 0
        {
            return 1
        }
        else
        {
             if OrderdetailDict.value(forKey: "worker_status") as! String == "pending"
             {
                 if ApiUtillity.sharedInstance.getUserData(key: "type") == "worker"
                 {
                    return 1
                }
                else
                 {
                    return 1
                }
            }
             else if OrderdetailDict.value(forKey: "worker_status") as! String == "reject"
             {
                return 1
                
                
            }
             else if OrderdetailDict.value(forKey: "worker_status") as! String == "complete"
             {
               
                return 1
                
            }
             else if OrderdetailDict.value(forKey: "worker_status") as! String == "cancel"
             {
               return 1
                
                
            }
            else
             {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
                let cell:UserSendChatDetailVC = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserSendChatDetailVC", for: indexPath) as! UserSendChatDetailVC
            if chat_array.count != 0
            {
                let text = (chat_array.object(at: 0) as! NSDictionary).value(forKey: "vMessage") as? String
              let data = text!.data(using: String.Encoding.unicode)! // mind "!"
                let attrStr = try? NSAttributedString( // do catch
                    data: data,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                let dCreatedDate = (self.chat_array.object(at: 0)as! NSDictionary).value(forKey: "dCreatedDate") as! String
                //2018-10-17 12:49:49
                
                
                let FinalDataDisplay = self.UTCToLocal(date: dCreatedDate)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date (from: FinalDataDisplay)
                
                let String1 = date!.timeAgoSinceNow
                print(String1)
                cell.Lbl_Time.text = "\(String1)"
                
                cell.lbl_for_msg.attributedText = attrStr
                
             
            }
            else
            {
                cell.lbl_for_msg.text = ""
            }
           
            
                return cell
        }
            
            
        else
        {
            if OrderdetailDict.count != 0
            {
                if OrderdetailDict.value(forKey: "worker_status") as! String == "pending"
                {
                    
                    
                    if OrderdetailDict.value(forKey: "user_status") as! String == "cancel"
                    {
                        
                            let cell:UserSendChatDetailVC = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserSendChatDetailVC", for: indexPath) as! UserSendChatDetailVC
                        
                        cell.Lbl_Time.isHidden = true
                        cell.lbl_for_msg.text = "Order Had Been Cancelled From User."
                        
                            
                            
                            return cell
                        
                        
                    }
                    else
                    {
                        
                        if ApiUtillity.sharedInstance.getUserData(key: "type") == "worker"
                        {
                            let cell:UserChatDetailPleasewaitXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserChatDetailPleasewaitXIB", for: indexPath) as! UserChatDetailPleasewaitXIB
                            
                            if self.name.isEmpty == true
                            {
                                //Please wait for the john reply...
                                //Please wait for the john reply...
                                //  cell.Lbl_Client.text = "Please wait for the" + "\(name)"  + "'s reply..."
                            }
                            else
                            {
                                
                                if self.type == "user"
                                {
                                    
                                }
                                else
                                {
                                    
                                }
                                
                                cell.Lbl_Client.text = ""
                            }
                            return cell
                        }
                        
                        else
                        {
                            let cell:UserChatDetailPleasewaitXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserChatDetailPleasewaitXIB", for: indexPath) as! UserChatDetailPleasewaitXIB
                            
                            if self.name.isEmpty == true
                            {
                                //Please wait for the john reply...
                                //Please wait for the john reply...
                                //  cell.Lbl_Client.text = "Please wait for the" + "\(name)"  + "'s reply..."
                            }
                            else
                            {
                                
                                if self.type == "user"
                                {
                                    
                                }
                                else
                                {
                                    
                                }
                                
                                cell.Lbl_Client.text = "Please wait for the" + " " + "Worker"  + "'s reply..."
                            }
                            return cell
                        }
                        }
                        
                        
                      
                    
                   
                }
                
                else if OrderdetailDict.value(forKey: "worker_status") as! String == "reject"
                {
                    let cell:UserRecieveChatDetailXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserRecieveChatDetailXIB", for: indexPath) as! UserRecieveChatDetailXIB
                    cell.lbl_for_msg.text = "Your Order is Rejected From Our Worker."
                  

                    
                    return cell
                    
                    
                }
                    
                else if OrderdetailDict.value(forKey: "worker_status") as! String == "complete"
                {
                    let cell:UserRecieveChatDetailXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserRecieveChatDetailXIB", for: indexPath) as! UserRecieveChatDetailXIB
                    cell.lbl_for_msg.text = "Your Order is Completed."
                    
                    return cell
                    
                    
                }
                
                else if OrderdetailDict.value(forKey: "worker_status") as! String == "cancel"
                {
                    let cell:UserRecieveChatDetailXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserRecieveChatDetailXIB", for: indexPath) as! UserRecieveChatDetailXIB
                    cell.lbl_for_msg.text = "Your Order is Cancelled From Worker."
                    
                    return cell
                    
                    
                }
                
                else // accept
                {
                    let cell:UserRecieveChatDetailXIB = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserRecieveChatDetailXIB", for: indexPath) as! UserRecieveChatDetailXIB
                    cell.lbl_for_msg.text = "Your Order is Accepted.We Will Contact You Soon."
                    
                   
                    
                    return cell
                }
                
            }
            else
            {
                let cell:UserSendChatDetailVC = Tableview_Chat.dequeueReusableCell(withIdentifier: "UserSendChatDetailVC", for: indexPath) as! UserSendChatDetailVC
                return cell
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    //    MARK:- API CALL
    
    func OrderDetails()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let headers = ["Vauthtoken":apiToken]
            
            let paramiter = ["order_id":Order_id] as [String : Any]
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/get_order_details"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                       self.OrderdetailDict = dict.value(forKey: "data") as! NSDictionary
                        
                        if self.OrderdetailDict.count != 0
                        {
                            if ApiUtillity.sharedInstance.getUserData(key: "type") == "user"
                            {
                                self.View_Accpet_Rejectview.isHidden = true

                            }
                            else
                            {
                                if self.OrderdetailDict.value(forKey: "worker_status") as! String == "pending"
                                {
                                    self.View_Accpet_Rejectview.isHidden = false
                                }
                                else
                                {
                                    self.View_Accpet_Rejectview.isHidden = true
                                }
                            }
                        }
                        self.chat_history()
                        self.Tableview_Chat.reloadData()
                        self.Tableview_Chat.reloadData()
                     //   self.Tableview_Chat.reloadSections(IndexSet(integersIn: 0...0), with: UITableView.RowAnimation.fade)
                        self.Tableview_Chat.isHidden = false

                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                    else if StatusCode==412
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    
    func chat_history()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let headers = ["Vauthtoken":apiToken]
            
            let paramiter = ["thread_id":thread_id] as [String : Any]
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/chat_history"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        self.Tableview_Chat.isHidden = false
                        self.chat_array = (dict.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        self.Tableview_Chat.reloadData()
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                    }
                   
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                    else if StatusCode==412
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    func Accept_reject_order()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let headers = ["Vauthtoken":apiToken]
            
            var paramiter = [String : Any]()
            if self.type == "3"
            {
                   paramiter = ["order_id":Order_id,"action":type,"modify_date_time":self.SelectedDate] as [String : Any]
            }
            else
            {
                   paramiter = ["order_id":Order_id,"action":type,"modify_date_time":""] as [String : Any]
            }
            
          
            print(paramiter)
            
            
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/accept_reject_modify"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: "Order accepted successfully")
                        self.OrderDetails()
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                    else if StatusCode==412
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    
    

}


extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}


extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

