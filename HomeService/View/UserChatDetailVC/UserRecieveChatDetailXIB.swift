//
//  UserRecieveChatDetailXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class UserRecieveChatDetailXIB: UITableViewCell {

    @IBOutlet weak var View_Background: UIView!
   
    @IBOutlet weak var lbl_for_msg: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
         ApiUtillity.sharedInstance.setCornurRadius(obj: View_Background, cornurRadius: 10, isClipToBound: true, borderColor: "ffffff", borderWidth: 0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
