//
//  WorkerSingupVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire

class WorkerSingupVC: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var border_view: UIView!
    @IBOutlet weak var txt_for_city: UITextField!
    @IBOutlet weak var txt_for_proficiency: UITextField!
    @IBOutlet weak var collection_view: UICollectionView!
    
    var yourViewBorder = CAShapeLayer()
    var imageData_pf_pic = Data()
    let picker = UIImagePickerController()
    var deletedImageIDArray = [String]()

    
    var Worker_name = String()
    var email = String()
    var password = String()
    var work_number = String()
    var imageArray = NSMutableArray()
    
    var lati = Double()
    var long = Double()
    var change_image = Bool()
    var change_index = Int()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.collection_view.register(UINib(nibName: "ImageCellXIB", bundle: nil), forCellWithReuseIdentifier: "ImageCellXIB")
        picker.delegate = self
        lati = UserDefaults.standard.double(forKey: "LATTITUDE")
        long = UserDefaults.standard.double(forKey: "LONGITUDE")
        
        print(lati,long)
        
        txt_for_city.placeHolderColor = UIColor.darkGray
        txt_for_proficiency.placeHolderColor = UIColor.darkGray
        
        
//        yourView.layer.addSublayer(yourViewBorder)

        // Do any additional setup after loading the view.
    }

    @IBAction func btn_for_choice_image(_ sender: Any)
    {
        openPhotoSelect()
    }
    @IBAction func btn_for_save_and_contuies(_ sender: Any)
    {
        
        if txt_for_proficiency.text! == ""
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter proficiency Name", ForNavigation: self.navigationController!)
            return
        }
        else if txt_for_city.text! == ""
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter city name", ForNavigation: self.navigationController!)
            return
        }
        
        
        Add_details()
    }
    @IBAction func btn_for_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func openPhotoSelect() {
        
        
        let actionSheetController = UIAlertController(title: "Home Services", message: "Please Select Image", preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let takePictureAction = UIAlertAction(title: "Open Camera", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(takePictureAction)
        
        // Create and add a second option action
        let choosePictureAction = UIAlertAction(title: "Open Gallery", style: .default) { action -> Void in
            self.openGallary()
        }
        actionSheetController.addAction(choosePictureAction)
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //    MARK:- ALL FUNCTION
    
    @objc func selectRemoveImage(_ sender: UIButton)
    {
        let dic = (imageArray.object(at: sender.tag) as! NSDictionary)
        if (dic.value(forKey: "isEdit") as! String) == "1"
        {
            deletedImageIDArray.append((dic.value(forKey: "id") as! String))
        }
        imageArray.removeObject(at: sender.tag)
        collection_view.reloadData()
    }
    
    
    func openGallary()
    {
        //        self.navigationController?.isNavigationBarHidden = false
        //        self.navigationItem.hidesBackButton = false
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = ["public.image"]
        present(picker, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            picker.mediaTypes = ["public.image"]
            present(picker,animated: true,completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        if (info[UIImagePickerController.InfoKey.mediaType] as? String) != nil
        {
            
            var image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            print("here image is \(String(describing: image))")
            
            image = ApiUtillity.sharedInstance.imageOrientation(image!)
            
            
            let uploadData = image!.jpegData(compressionQuality: 0.10)
            image = UIImage(data: uploadData!)!
            
            
            
            if change_image == true
            {
                imageArray.removeObject(at: change_index)
                imageArray.insert(["isEdit":"0", "id":"", "type":"image", "data":ApiUtillity.sharedInstance.imageOrientation(image!)], at: change_index)
                change_image = false
            }
            else
            {
                imageArray.add(["isEdit":"0", "id":"", "type":"image", "data":image])
            }
            print("here image data is \(String(describing: imageData_pf_pic))")
            collection_view.reloadData()
        }
        
        self.dismiss(animated: true, completion: nil)
        //        self.navigationController?.isNavigationBarHidden = true
        //        self.navigationItem.hidesBackButton = true
    }
    
    
    //    MARK:- COLLECTION MATHOD
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:ImageCellXIB = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCellXIB", for: indexPath) as! ImageCellXIB
        cell.image_view.image = ((imageArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "data") as! UIImage)
        
        cell.Btn_Handler_Remove_Image.tag = indexPath.row
        cell.Btn_Handler_Remove_Image.addTarget(self, action: #selector(selectRemoveImage(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        change_image = true
        change_index = indexPath.row
        openPhotoSelect()

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 75 , height: 75)
    }
    
    
    
    
    //  MARK:-   API CALL
    
    func Add_details()
    {
        let push_token = ApiUtillity.sharedInstance.getIphoneData(key: "Vtoken")
        let url = ApiUtillity.sharedInstance.API(Join: "user/register")
        let params = ["email":email,"password":password,"eDeviceType":"","vPushTokens":push_token,"type":"worker","name":Worker_name,"mobile":work_number,"address":"","latitude":"\(lati)","longitude":"\(long)","working_city":txt_for_city.text!,"proficiency":txt_for_proficiency.text!] as [String : String]
        print(params)
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            DispatchQueue.main.sync {
                
                for (key, value) in params {
                      multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                var imageInc = 0
                print(self.imageArray)
                for item in self.imageArray
                {
                    multipartFormData.append(((item as! NSDictionary).value(forKey: "data") as! UIImage).pngData()!, withName: "work_images[\(imageInc)]", fileName: "work_images\(imageInc).png",mimeType: "image/png")
                    imageInc = imageInc + 1
                }
            }
        }, usingThreshold: UInt64(), to: url, method: .post, headers: nil) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil {
                        let Dic:NSDictionary = response.result.value as! NSDictionary
                        print(Dic)
                        if Dic.value(forKey: "status") as! Int == 200
                        {
                            //ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: "Success")
                            //let user_Data = Dic.value(forKey: "data") as! NSDictionary
                            //ApiUtillity.sharedInstance.setUserData(data: user_Data)
                            //let push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
                            //self.navigationController?.pushViewController(push, animated: true)
                            let push = workerLoginVC()
                            push.type = "worker"
                            self.navigationController?.pushViewController(push, animated: true)
                            ApiUtillity.sharedInstance.dismissSVProgressHUD()

                        }
                        else if Dic.value(forKey: "status") as! Int == 401
                        {
                            let ErrorDic:NSDictionary = Dic.value(forKey: "message") as! NSDictionary
                            let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                            
                        }
                        else {
                            let ErrorDic = Dic.value(forKey: "message") as! NSDictionary
                            let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        }
                    }
                    else {
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithAPIError(error: response.error! as NSError)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    


}
