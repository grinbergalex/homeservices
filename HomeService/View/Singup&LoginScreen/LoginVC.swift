//
//  LoginVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{

    @IBOutlet weak var View_Pop_Up_Main: UIView!
    @IBOutlet weak var tbl_view: UITableView!
    
    @IBOutlet weak var Webview_Request: UIWebView!
    var Email_address = String()
    var password = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl_view.register(HeaderXIB.self, forCellReuseIdentifier: "HeaderXIB")
        tbl_view.register(UINib(nibName: "HeaderXIB", bundle: nil), forCellReuseIdentifier: "HeaderXIB")
        
        tbl_view.register(textFiledXIB.self, forCellReuseIdentifier: "textFiledXIB")
        tbl_view.register(UINib(nibName: "textFiledXIB", bundle: nil), forCellReuseIdentifier: "textFiledXIB")
        
        tbl_view.register(PasswordXIB.self, forCellReuseIdentifier: "PasswordXIB")
        tbl_view.register(UINib(nibName: "PasswordXIB", bundle: nil), forCellReuseIdentifier: "PasswordXIB")
        
        tbl_view.register(LoginButtonXIB.self, forCellReuseIdentifier: "LoginButtonXIB")
        tbl_view.register(UINib(nibName: "LoginButtonXIB", bundle: nil), forCellReuseIdentifier: "LoginButtonXIB")
        
        tbl_view.register(ForgotPasswordXIB.self, forCellReuseIdentifier: "ForgotPasswordXIB")
        tbl_view.register(UINib(nibName: "ForgotPasswordXIB", bundle: nil), forCellReuseIdentifier: "ForgotPasswordXIB")
        
        tbl_view.register(alreadyAccountXIB.self, forCellReuseIdentifier: "alreadyAccountXIB")
        tbl_view.register(UINib(nibName: "alreadyAccountXIB", bundle: nil), forCellReuseIdentifier: "alreadyAccountXIB")
        
        tbl_view.register(LoginwithWorkerXIB.self, forCellReuseIdentifier: "LoginwithWorkerXIB")
        tbl_view.register(UINib(nibName: "LoginwithWorkerXIB", bundle: nil), forCellReuseIdentifier: "LoginwithWorkerXIB")
        
       

        // Do any additional setup after loading the view.
    }
    
    //    MARK:- TABLEVIEW METHOD
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 7
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        else if section == 2
        {
            return 1
        }
        else if section == 3
        {
            return 1
        }
        else if section == 4
        {
            return 1
        }
        else if section == 5
        {
            return 1
        }
        else
        {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell:HeaderXIB = tableView.dequeueReusableCell(withIdentifier: "HeaderXIB", for: indexPath) as! HeaderXIB
            
            cell.Lbl_Text.text = "Login to your account"
            cell.Lbl_Text.textColor = UIColor.darkGray
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell:textFiledXIB = tableView.dequeueReusableCell(withIdentifier: "textFiledXIB", for: indexPath) as! textFiledXIB
            cell.icon_image.image = UIImage(named: "mail")
            cell.txt_field.placeholder = "Enter your email here"
            cell.txt_field.placeHolderColor = UIColor.darkGray
            cell.txt_field.delegate = self
            cell.txt_field.tag = 1
            return cell
        }
        else if indexPath.section == 2
        {
            let cell:PasswordXIB = tableView.dequeueReusableCell(withIdentifier: "PasswordXIB", for: indexPath) as! PasswordXIB
            cell.txt_for_password.placeholder = "Enter your password here"
            cell.txt_for_password.placeHolderColor = UIColor.darkGray
            cell.txt_for_password.delegate = self
            cell.txt_for_password.tag = 2
            return cell
        }
        else if indexPath.section == 3
        {
            let cell:LoginButtonXIB = tableView.dequeueReusableCell(withIdentifier: "LoginButtonXIB", for: indexPath) as! LoginButtonXIB
            cell.btn_for_login.setTitleColor(.black, for: .normal)

            cell.btn_for_login.addTarget(self, action: #selector(Login), for: .touchUpInside)
            
            
//            cell.btn_for_login.layer.cornerRadius = 10
//            cell.btn_for_login.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
//            cell.btn_for_login.layer.shadowOpacity = 4
//            cell.btn_for_login.layer.shadowOffset = CGSize.zero
//            cell.btn_for_login.layer.shadowRadius = 4
//            cell.btn_for_login.clipsToBounds = true
//            //cell.user_pf_image_view.layer.masksToBounds = false
//            cell.btn_for_login.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
//         //   cell.btn_for_login.layer.borderWidth = 3.0
//
            cell.btn_for_login.layer.cornerRadius = 10
            cell.btn_for_login.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
            cell.btn_for_login.layer.shadowOpacity = 4
            cell.btn_for_login.layer.shadowOffset = CGSize.zero
            cell.btn_for_login.layer.shadowRadius = 2
            cell.btn_for_login.clipsToBounds = true
            cell.btn_for_login.layer.masksToBounds = false
            //cell.btn_for_login.layer.borderWidth = 0.1
            
            
            
            return cell
        }
        else if indexPath.section == 4
        {
            let cell:ForgotPasswordXIB = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordXIB", for: indexPath) as! ForgotPasswordXIB
            cell.btn_for_forgotpasswod.addTarget(self, action: #selector(forgot_password), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 5
        {
            let cell:alreadyAccountXIB = tableView.dequeueReusableCell(withIdentifier: "alreadyAccountXIB", for: indexPath) as! alreadyAccountXIB
            cell.btn_for_new_user.addTarget(self, action: #selector(Already_Account), for: .touchUpInside)
            cell.btn_for_new_user.setTitle("New User ? Create Account", for: .normal)
            
            cell.View_Already.isHidden = true
            cell.Btn_New_Button.addTarget(self, action: #selector(Already_Account), for: .touchUpInside)
            
            //New User ? Create Account
            
            return cell
        }
        else
        {
            let cell:LoginwithWorkerXIB = tableView.dequeueReusableCell(withIdentifier: "LoginwithWorkerXIB", for: indexPath) as! LoginwithWorkerXIB
            
            cell.btn_for_login_worker.addTarget(self, action: #selector(login_With_worker), for: .touchUpInside)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 234
        }
        else if indexPath.section == 1
        {
            return 50
        }
        else if indexPath.section == 2
        {
            return 50
        }
        else if indexPath.section == 3
        {
            return 70
        }
        else if indexPath.section == 4
        {
            return 50
        }
        else if indexPath.section == 5
        {
            return 40
        }
        else
        {
            return 140
        }
    }
    
    
    //    MARK:- BUTTON ACTION FUNCTION
    
    @objc func Already_Account()
    {
        let push = SignupVC()
        push.type = "user"
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func login_With_worker()
    {
        let push = workerLoginVC()
        push.type = "worker"
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func Login()
    {
        if Email_address == ""
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
            return
        }
        else if ApiUtillity.sharedInstance.isValidEmail(testStr: Email_address) == false
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Valid Email Address", ForNavigation: self.navigationController!)
            return
        }
        else if password == ""
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter password", ForNavigation: self.navigationController!)
            return
        }
        
        login_Api()

    }
    
    @objc func forgot_password()
    {
        let push = ForgotPasswordVC()
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @IBAction func Btn_Handler_Close_Pop_Up(_ sender: Any)
    {
        View_Pop_Up_Main.isHidden = true
    }
    
    
    //    MARK:- TEXTFIELD DELEGATE
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        var kActualText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        kActualText = kActualText.trimmingCharacters(in: .whitespaces)
        
        switch textField.tag
        {
        case 1:
            Email_address = kActualText;
        case 2:
            password = kActualText;
        default:
            print("It is nothing");
        }
        return true;
    }
    
    
    //  MARK:-   API CALL
    
    @objc func login_Api()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let push_token = ApiUtillity.sharedInstance.getIphoneData(key: "Vtoken")
            let params = ["email":Email_address,"password":password,"eDeviceType":"","vPushToken":push_token as Any] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/login"), method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        let user_Data = dict.value(forKey: "data") as! NSDictionary
                        ApiUtillity.sharedInstance.setUserData(data: user_Data)
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
//                        let type = user_Data.value(forKey: "type") as! String
                        
                        
                        let push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
                        self.navigationController?.pushViewController(push, animated: true)

                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "email") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    

}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
