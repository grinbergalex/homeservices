//
//  SignupVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Alamofire

class SignupVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIWebViewDelegate
{

    @IBOutlet weak var tbl_view: UITableView!
    
    @IBOutlet weak var Webview_Request: UIWebView!
    var type = String()
    
    
    
    @IBOutlet weak var View_Pop_Up_Web_View: UIView!
    var placeholder_array = ["Enter your name","Enter your email","Enter your mobile number"]
     var icon_array = ["user","mail","password"]
    
    var Email_address = String()
    var password = String()
    var name = String()
    var mobile_number = String()
    var confirm_Password = String()
    var Is_Privacy_Policy = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl_view.register(HeaderXIB.self, forCellReuseIdentifier: "HeaderXIB")
        tbl_view.register(UINib(nibName: "HeaderXIB", bundle: nil), forCellReuseIdentifier: "HeaderXIB")
        
        tbl_view.register(textFiledXIB.self, forCellReuseIdentifier: "textFiledXIB")
        tbl_view.register(UINib(nibName: "textFiledXIB", bundle: nil), forCellReuseIdentifier: "textFiledXIB")
        
        tbl_view.register(PasswordXIB.self, forCellReuseIdentifier: "PasswordXIB")
        tbl_view.register(UINib(nibName: "PasswordXIB", bundle: nil), forCellReuseIdentifier: "PasswordXIB")
        
        tbl_view.register(LoginButtonXIB.self, forCellReuseIdentifier: "LoginButtonXIB")
        tbl_view.register(UINib(nibName: "LoginButtonXIB", bundle: nil), forCellReuseIdentifier: "LoginButtonXIB")
        
        tbl_view.register(ForgotPasswordXIB.self, forCellReuseIdentifier: "ForgotPasswordXIB")
        tbl_view.register(UINib(nibName: "ForgotPasswordXIB", bundle: nil), forCellReuseIdentifier: "ForgotPasswordXIB")
        
        tbl_view.register(TermsAndConditionsXIB.self, forCellReuseIdentifier: "TermsAndConditionsXIB")
        tbl_view.register(UINib(nibName: "TermsAndConditionsXIB", bundle: nil), forCellReuseIdentifier: "TermsAndConditionsXIB")
        
        tbl_view.register(alreadyAccountXIB.self, forCellReuseIdentifier: "alreadyAccountXIB")
        tbl_view.register(UINib(nibName: "alreadyAccountXIB", bundle: nil), forCellReuseIdentifier: "alreadyAccountXIB")
        
        View_Pop_Up_Web_View.isHidden = true

        print(type)
        
        Is_Privacy_Policy = "0"

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btn_for_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    //    MARK:- TABLEVIEW METHOD
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 6
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 3
        }
        else if section == 2
        {
            return 1
        }
        else if section == 3
        {
            return 1
        }
            else if section == 4
        {
            return 1
            
        }
        else
        {
            return 1
        }

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell:HeaderXIB = tableView.dequeueReusableCell(withIdentifier: "HeaderXIB", for: indexPath) as! HeaderXIB
            
            if type == "worker"
            {
                cell.Lbl_Text.text = "Create Worker Account"
                cell.Lbl_Text.textColor = UIColor.darkGray

            }
            else
            {
                cell.Lbl_Text.text = "Create Account"
                cell.Lbl_Text.textColor = UIColor.darkGray

            }
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell:textFiledXIB = tableView.dequeueReusableCell(withIdentifier: "textFiledXIB", for: indexPath) as! textFiledXIB
            cell.txt_field.placeholder = placeholder_array[indexPath.row]
            cell.txt_field.delegate = self
            cell.txt_field.tag = indexPath.row
            cell.icon_image.image = UIImage(named: icon_array[indexPath.row])
            cell.txt_field.placeHolderColor = UIColor.darkGray
            
            
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell:PasswordXIB = tableView.dequeueReusableCell(withIdentifier: "PasswordXIB", for: indexPath) as! PasswordXIB
            cell.txt_for_password.placeholder = "Enter your password"
            cell.txt_for_password.delegate = self
            cell.txt_for_password.tag = 100
            cell.txt_for_password.placeHolderColor = UIColor.darkGray

            return cell
        }
            
        else if indexPath.section == 3
        {
            let cell:TermsAndConditionsXIB = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionsXIB", for: indexPath) as! TermsAndConditionsXIB
            
            cell.Btn_Title_Big_Privacy.tag = indexPath.row
            cell.Btn_Title_Small_Privacy.tag = indexPath.row
            
            if self.Is_Privacy_Policy == "0"
            {
                cell.Btn_Title_Small_Privacy.setImage(UIImage(named: "New_uncheck"), for: .normal)
            }
            else
            {
                
                cell.Btn_Title_Small_Privacy.setImage(UIImage(named: "New_check"), for: .normal)
            }
            
             cell.Btn_Title_Big_Privacy.addTarget(self, action: #selector(PrivacyPolicy), for: .touchUpInside)
              cell.Btn_Title_Small_Privacy.addTarget(self, action: #selector(PrivacyPolicy), for: .touchUpInside)
            
            
            return cell
            
        }
        else if indexPath.section == 4
        {
            let cell:LoginButtonXIB = tableView.dequeueReusableCell(withIdentifier: "LoginButtonXIB", for: indexPath) as! LoginButtonXIB
            cell.btn_for_login.setTitle("REGISTER", for: .normal)
             cell.btn_for_login.setTitleColor(.black, for: .normal)
            cell.btn_for_login.addTarget(self, action: #selector(Resister), for: .touchUpInside)
            
            cell.btn_for_login.layer.cornerRadius = 10
            cell.btn_for_login.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
            cell.btn_for_login.layer.shadowOpacity = 4
            cell.btn_for_login.layer.shadowOffset = CGSize.zero
            cell.btn_for_login.layer.shadowRadius = 2
            cell.btn_for_login.clipsToBounds = true
            cell.btn_for_login.layer.masksToBounds = false
            
            return cell
        }
        else
        {
            let cell:alreadyAccountXIB = tableView.dequeueReusableCell(withIdentifier: "alreadyAccountXIB", for: indexPath) as! alreadyAccountXIB
            cell.btn_for_new_user.setTitle("", for: .normal)
            cell.btn_for_new_user.addTarget(self, action: #selector(back_to_login), for: .touchUpInside)
            
             cell.View_Already.isHidden = false
            cell.Btn_New_Button.addTarget(self, action: #selector(back_to_login), for: .touchUpInside)
            
            
            
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 200
        }
        else if indexPath.section == 1
        {
            return 50
        }
        else if indexPath.section == 2
        {
            return 50
        }
        else if indexPath.section == 3
        {
            return 50
        }
            else if indexPath.section == 4
        {
            return 70 
        }
        else
        {
            return 125
        }
    }
    
    
    //    MARK:- ACTION
    
    @objc func PrivacyPolicy(sender:UIButton)
    {
        View_Pop_Up_Web_View.isHidden = false
        
       
        let TempUrl = ApiUtillity.sharedInstance.API(Join: "user/content/terms_condition")
        let url = URL (string: TempUrl)
            let requestObj = URLRequest(url: url!)
            Webview_Request.loadRequest(requestObj)
        self.Is_Privacy_Policy = "1"
        

        
        
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        ApiUtillity.sharedInstance.dismissSVProgressHUD()
        
    }
    
    @objc func back_to_login()
    {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Btn_Handler_Black_Pop(_ sender: Any)
    {
         View_Pop_Up_Web_View.isHidden = true
        self.Is_Privacy_Policy = "1"
        self.tbl_view.reloadData()
    }
    @objc func Resister()
    {

       
        
        if type == "user"
        {
            if name == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Name", ForNavigation: self.navigationController!)
                return
            }
            else if Email_address == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
                return
            }
            else if ApiUtillity.sharedInstance.isValidEmail(testStr: Email_address) == false
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Valid Email Address", ForNavigation: self.navigationController!)
                return
            }
            else if mobile_number == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter mobile number", ForNavigation: self.navigationController!)
                return
            }
            else if password == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter password", ForNavigation: self.navigationController!)
                return
            }
            else if password.count < 6
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter minimum 6 character password", ForNavigation: self.navigationController!)
                return
            }
            
            if self.Is_Privacy_Policy == "0"
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Accept Terms and privacy policy", ForNavigation: self.navigationController!)
                return
            }
            
            
            
            Sing_up()
        }
        
        else
        {
            
            if name == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Name", ForNavigation: self.navigationController!)
                return
            }
            else if Email_address == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
                return
            }

            else if ApiUtillity.sharedInstance.isValidEmail(testStr: Email_address) == false
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Valid Email Address", ForNavigation: self.navigationController!)
                return
            }
            else if mobile_number == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Mobile Number", ForNavigation: self.navigationController!)
                return
            }
            else if password == ""
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Password", ForNavigation: self.navigationController!)
                return
            }
            else if password.count < 6
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Enter Minimum 6 Character Password", ForNavigation: self.navigationController!)
                return
            }
            
            if self.Is_Privacy_Policy == "0"
            {
                ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Accept Terms And Privacy Policy", ForNavigation: self.navigationController!)
                return
            }
            
            
            
            check_email_address()
        }
    }
    
    
    
    //     MARK:- TEXTFILED METHOD
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        var kActualText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        kActualText = kActualText.trimmingCharacters(in: .whitespaces)
        
        switch textField.tag
        {
        case 0:
            name = kActualText;
        case 1:
            Email_address = kActualText;
        case 2:
             mobile_number = kActualText;
        case 100:
            password = kActualText;
        default:
            print("It is nothing");
        }
        return true;
    }
    
    
    
    //  MARK:-   API CALL
    
    func Sing_up()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let push_token = ApiUtillity.sharedInstance.getIphoneData(key: "Vtoken")
            let params = ["email":Email_address,"password":password,"eDeviceType":"","vPushTokens":push_token,"type":"user","name":name,"mobile":mobile_number] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/register"), method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        
                        
                        if self.type == "worker"
                        {
                            let push = LoginVC()
                            self.navigationController?.pushViewController(push, animated: true)
                            ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        }
                        else
                        {
                            let user_date = dict.value(forKey: "data") as! NSDictionary
                            ApiUtillity.sharedInstance.setUserData(data: user_date)
                            ApiUtillity.sharedInstance.dismissSVProgressHUD()
                            let push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
                            self.navigationController?.pushViewController(push, animated: true)
                            ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        }
                        
                     
                        
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_email = ErrorDic.value(forKey: "email") as? String
                        let Errormobile_no = ErrorDic.value(forKey: "mobile") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_email!)
                            return
                        }
                    }
                }
                else
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    
    func check_email_address()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let params = ["email":Email_address] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/email_exist"), method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        self.check_mobile()
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "email") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }
    
    func check_mobile()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            let params = ["mobile":mobile_number] as [String : Any]
            print(params)
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join:"user/mobile_exist"), method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    print(dict)
                    print(response)
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        let push = WorkerSingupVC()
                        push.Worker_name = self.name
                        push.email = self.Email_address
                        push.password = self.password
                        push.work_number = self.mobile_number
                        self.navigationController?.pushViewController(push, animated: true)
                    }
                    else
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let Errormobile_no = ErrorDic.value(forKey: "mobile") as? String
                        let Erroremail = ErrorDic.value(forKey: "error") as? String
                        if Errormobile_no?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Errormobile_no!)
                            return
                        }
                        if Erroremail?.count == nil
                        {}
                        else
                        {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: Erroremail!)
                            return
                        }
                        
                    }
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                }
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Services", SubTitle: "No Internet connection", ForNavigation: self.navigationController!)
            return
        }
        
    }

}
