//
//  EditProfileVC.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import CoreLocation
import GoogleMaps
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import Kingfisher
import Alamofire
import AlamofireImage
import ImageSlideshow
import SafariServices
import GooglePlaces
import GoogleMaps
import CoreLocation
import Alamofire
import Kingfisher
import Cosmos
import SKPhotoBrowser
import SDWebImage


class EditProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,GMSPlacePickerViewControllerDelegate,GMSMapViewDelegate
{
    
    @IBOutlet weak var tbl_view: UITableView!
    
    var edit_name = String()
    var edit_email = String()
    var edit_mobile_number = String()
    var edit_address = String()
    var picker:UIImagePickerController?=UIImagePickerController()
    var imageData = Data()
    let picker123 = UIImagePickerController()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var currentlatitude =  Double()
    var currentlogitude =  Double()
    var currentLC = CLLocationCoordinate2D()
    var latitude = String()
    var logitude = String()
    var apiToken = String()
    var Token =  String()
    
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        
        return _locationManager
    }()
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tbl_view.register(profileViewXIB.self, forCellReuseIdentifier: "profileViewXIB")
        tbl_view.register(UINib(nibName: "profileViewXIB", bundle: nil), forCellReuseIdentifier: "profileViewXIB")
        
        tbl_view.register(EditprofilelistXIB.self, forCellReuseIdentifier: "EditprofilelistXIB")
        tbl_view.register(UINib(nibName: "EditprofilelistXIB", bundle: nil), forCellReuseIdentifier: "EditprofilelistXIB")
        
        tbl_view.register(LoginButtonXIB.self, forCellReuseIdentifier: "LoginButtonXIB")
        tbl_view.register(UINib(nibName: "LoginButtonXIB", bundle: nil), forCellReuseIdentifier: "LoginButtonXIB")
        
        
        edit_name = ApiUtillity.sharedInstance.getUserData(key: "name")
        edit_email = ApiUtillity.sharedInstance.getUserData(key: "email")
        edit_mobile_number = ApiUtillity.sharedInstance.getUserData(key: "mobile")
        edit_address = ApiUtillity.sharedInstance.getUserData(key: "address")

        
        picker?.delegate = self
        picker123.delegate = self
        
        
        self.locManager.delegate = self
        self.locManager.startUpdatingLocation()
        
      
        
        self.currentLC = CLLocationCoordinate2DMake(23.032331,72.56208)
        print(self.currentLC)
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways)
        {
            currentLocation = locManager.location
            print(currentLocation)
            if currentLocation == nil {
                currentlatitude = 23.032331
                currentlogitude = 72.56208
            }
            else
            {
                currentlatitude = (currentLocation.coordinate.latitude)
                currentlogitude = (currentLocation.coordinate.longitude)
            }
            
        }
        
        currentLC = CLLocationCoordinate2DMake(currentlatitude,currentlogitude)
        
        
        
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func back_btn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //    MARK:- TABELVIEW METHDO
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 4
        }
        else
        {
            return 1
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell:profileViewXIB = tableView.dequeueReusableCell(withIdentifier: "profileViewXIB", for: indexPath) as! profileViewXIB
            let image_url = ApiUtillity.sharedInstance.getUserData(key: "profile_image")
            cell.user_image_view.kf.indicatorType = .activity
            cell.user_image_view.kf.setImage(with: URL(string: image_url))
            
            cell.user_image_view.layer.cornerRadius = 45
            cell.user_image_view.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.user_image_view.layer.shadowOpacity = 4
            cell.user_image_view.layer.shadowOffset = CGSize.zero
            cell.user_image_view.layer.shadowRadius = 4
            cell.user_image_view.clipsToBounds = true
            //cell.user_pf_image_view.layer.masksToBounds = false
            cell.user_image_view.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.user_image_view.layer.borderWidth = 3.0
            
            
            if self.imageData.count == 0
            {
                
            }
            else
            {
                let image : UIImage = UIImage(data: self.imageData)!
                cell.user_image_view.image = image
                cell.user_image_view.layer.cornerRadius = 45
                cell.user_image_view.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                cell.user_image_view.layer.shadowOpacity = 4
                cell.user_image_view.layer.shadowOffset = CGSize.zero
                cell.user_image_view.layer.shadowRadius = 4
                cell.user_image_view.clipsToBounds = true
                //cell.user_pf_image_view.layer.masksToBounds = false
                cell.user_image_view.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                cell.user_image_view.layer.borderWidth = 3.0
                

            }
            
            cell.Btn_Title_Select_image.tag = indexPath.row
           
            
             cell.Btn_Title_Select_image.addTarget(self, action: #selector(SELECTIMAGE(_:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell:EditprofilelistXIB = tableView.dequeueReusableCell(withIdentifier: "EditprofilelistXIB", for: indexPath) as! EditprofilelistXIB
            
            if indexPath.row == 0
            {
                cell.Btn_Title_Address.isHidden = true
                cell.Btn_Title_Address.isUserInteractionEnabled = false
                cell.lbl_for_title.text = "Your name:"
                cell.txt_for_title_name.text = ApiUtillity.sharedInstance.getUserData(key: "name")
                cell.txt_for_title_name.delegate = self
                cell.txt_for_title_name.tag = indexPath.row
                
                if self.edit_name.isEmpty == true
                {
                    
                }
                else
                {
                    
                }
                
            }
            if indexPath.row == 1
            {
                cell.lbl_for_title.text = "Your email:"
                cell.Btn_Title_Address.isHidden = true
                cell.Btn_Title_Address.isUserInteractionEnabled = false
                cell.txt_for_title_name.delegate = self
                cell.txt_for_title_name.tag = indexPath.row
                
                if self.edit_email.isEmpty == true
                {
                    cell.txt_for_title_name.text = ApiUtillity.sharedInstance.getUserData(key: "email")
                }
                else
                {
                    cell.txt_for_title_name.text = self.edit_email
                }
                
            }
            if indexPath.row == 2
            {
                cell.lbl_for_title.text = "Your mobile:"
                cell.Btn_Title_Address.isHidden = true
                cell.Btn_Title_Address.isUserInteractionEnabled = false
                cell.txt_for_title_name.delegate = self
                cell.txt_for_title_name.tag = indexPath.row
                
                if self.edit_mobile_number.isEmpty == true
                {
                    cell.txt_for_title_name.text = ApiUtillity.sharedInstance.getUserData(key: "mobile")
                    
                }
                else
                {
                    cell.txt_for_title_name.text = self.edit_mobile_number
                    
                }
                
            }
            if indexPath.row == 3
            {
                cell.lbl_for_title.text = "Address:"
                cell.Btn_Title_Address.isHidden = false
                cell.Btn_Title_Address.isUserInteractionEnabled = true
                cell.txt_for_title_name.delegate = self
                cell.txt_for_title_name.tag = indexPath.row
                cell.Btn_Title_Address.addTarget(self, action: #selector(GetAddress(sender:)), for: .touchUpInside)
                
                
                if self.edit_address.isEmpty == true
                {
                    cell.txt_for_title_name.text = ApiUtillity.sharedInstance.getUserData(key: "address")
                    cell.txt_for_title_name.placeholder = "Add address"
                }
                else
                {
                    cell.txt_for_title_name.text = self.edit_address
                    
                }
            }
            
            return cell
        }
        else
        {
            let cell:LoginButtonXIB = tableView.dequeueReusableCell(withIdentifier: "LoginButtonXIB", for: indexPath) as! LoginButtonXIB
            cell.btn_for_login.setTitle("UPDATE", for: .normal)
            cell.btn_for_login.addTarget(self, action: #selector(udpdate_profile), for: .touchUpInside)
            
            cell.btn_for_login.layer.cornerRadius = 10
            cell.btn_for_login.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
            cell.btn_for_login.layer.shadowOpacity = 4
            cell.btn_for_login.layer.shadowOffset = CGSize.zero
            cell.btn_for_login.layer.shadowRadius = 2
            cell.btn_for_login.clipsToBounds = true
            cell.btn_for_login.layer.masksToBounds = false
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 200
        }
        else if indexPath.section == 1
        {
            return 65
        }
        else
        {
            return 200
        }
        
    }
    
   
    // MARK:- ALL FUNCTIONS
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var kActualText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        kActualText = kActualText.trimmingCharacters(in: .whitespaces)
        
        switch textField.tag
        {
        case 0:
            edit_name = kActualText;
        case 1:
            edit_email = kActualText;
        case 2:
            edit_mobile_number = kActualText;
        case 3:
            edit_address = kActualText;
        default:
            print("It is nothing");
        }
        return true;
    }
    
    
    //MARK:- ALL FUNCTIONS
    
    @objc func SELECTIMAGE(_ sender: UIButton)
    {
        let actionSheetController = UIAlertController(title: "", message: "Please Select Image", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction = UIAlertAction(title: "Open Camera", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(takePictureAction)
        let choosePictureAction = UIAlertAction(title: "Open Gallery", style: .default) { action -> Void in
            self.openGallary()
        }
        actionSheetController.addAction(choosePictureAction)
        actionSheetController.popoverPresentationController?.sourceView = sender as! UIView
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        print("here image is \(String(describing: image))")
 //imageData = UIImageJPEGRepresentation(image!, 0.25)!
        
        imageData =  ApiUtillity.sharedInstance.imageOrientation(image!).jpegData(compressionQuality: 0.10)!
        print("here image data is \(String(describing: imageData))")
        print("Image Selected")
        
        
        
        self.dismiss(animated: true, completion: nil)
        
        if self.imageData.count == 0
        {
            self.tbl_view.reloadData()

        }
        else
        {
            self.tbl_view.reloadData()
        }
        
        
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker123.mediaTypes = ["public.image"]
            
            picker123.sourceType = UIImagePickerController.SourceType.camera
            
            self .present(picker123, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker123.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        
        picker123.mediaTypes = ["public.image"]
        
        picker123.sourceType = .photoLibrary
        picker123.delegate = self
        self.present(picker123, animated: true, completion: nil)
        
        
        
    }
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        print(place)
        viewController.dismiss(animated: true, completion: nil)
        
        if place.formattedAddress == nil
        {
            
        }
        else
        {
            let String = place.formattedAddress
            print(String)
            
            var FInalString = String
            
            FInalString = String!
        
            //self.Txt_Google_Address.text = "\(String!)"
            self.edit_address = FInalString!
            self.tbl_view.reloadData()
        }
        
        
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController)
    {
        self.edit_address = String()
        viewController.dismiss(animated: true, completion: nil)
        self.tbl_view.reloadData()
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    
    @objc func GetAddress(sender:UIButton)
    {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    
    @objc func udpdate_profile()
    {
        print(edit_name,edit_email,edit_mobile_number,edit_address)
        
        if self.edit_name.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Name", ForNavigation: self.navigationController!)
            return
        }
     
        if self.edit_email.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Email Address", ForNavigation: self.navigationController!)
            return
        }
     
        if self.edit_mobile_number.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Mobile Number", ForNavigation: self.navigationController!)
            return
        }
        
        if self.edit_address.isEmpty == true
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Enter Address", ForNavigation: self.navigationController!)
            return
        }
       
        if ApiUtillity.sharedInstance.isReachable()
        {
            
            UpdateProfile()
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title: "Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
        
        
        
    }
    
    func UpdateProfile()
    {
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        var parameters = [String:String]()
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        
        let headers = ["Vauthtoken":apiToken]
        parameters = ["name":self.edit_name,"email":self.edit_email,"mobile":self.edit_mobile_number,"address":self.edit_address,"latitude":"\(self.latitude)","longitude":"\(self.logitude)","wimg_ids":""]
        
        print(parameters)
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(self.imageData, withName: "profile_image", fileName: "image.jpg", mimeType: "image/jpg")
            for (key, value) in parameters
            {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                print(key)
                print(value)
                
            }
            
        },usingThreshold: UInt64(), to: ApiUtillity.sharedInstance.API(Join: "user/edit_profile"), method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil {
                        let Dic:NSDictionary = response.result.value as! NSDictionary
                        print(Dic)
                        if Dic.value(forKey: "status") as! Int == 200
                        {
                            
                            let SuccessDic = Dic.value(forKey: "message") as! NSDictionary
                            let SuccessMessage = SuccessDic.value(forKey: "success") as! String
                            
                            let UserData = Dic.value(forKey: "data") as! NSDictionary
                            ApiUtillity.sharedInstance.setUserData(data: UserData )
                            
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithSuccess(success: SuccessMessage)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                            {
                                self.navigationController?.popViewController(animated: true)
                                
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                
                            }
                            self.view.endEditing(true)
                        } else {
                            let ErrorDic = Dic.value(forKey: "message") as! NSDictionary
                            let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        }
                    }
                    else {
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithAPIError(error: response.error! as NSError)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
        
    }



}
