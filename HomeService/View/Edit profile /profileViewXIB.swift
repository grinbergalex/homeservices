//
//  profileViewXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class profileViewXIB: UITableViewCell
{

    @IBOutlet weak var user_image_view: UIImageView!
    
    @IBOutlet weak var Btn_Title_Select_image: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        ApiUtillity.sharedInstance.setCornurRadius(obj: user_image_view, cornurRadius: 45, isClipToBound: true, borderColor: "9cc1d4", borderWidth: 4)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
