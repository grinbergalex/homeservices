//
//  NavigationScreenXIB.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import Alamofire
import Kingfisher

class NavigationScreenXIB: UIViewController,GMSMapViewDelegate, CLLocationManagerDelegate ,CAAnimationDelegate{
    
    //MARK:- OUTLETS

    @IBOutlet weak var Google_Map_View: GMSMapView!
    
    @IBOutlet weak var Lbl_Header_Txt: UILabel!
    //Google_Map_View
    
    //MARK:- VARIABLES
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var currentlatitude =  Double()
    var currentlogitude =  Double()
    var currentLC = CLLocationCoordinate2D()
    var latitude = String()
    var logitude = String()
    var OrderdetailDict = NSMutableDictionary()
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        
        return _locationManager
    }()
    
    
    //MARK:- VIEW DID LOAD
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.Google_Map_View.settings.myLocationButton = true
        self.Google_Map_View.padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 20)
        
        self.Google_Map_View.delegate  = self
        self.locManager.delegate = self
        self.locManager.startUpdatingLocation()
        
        self.currentLC = CLLocationCoordinate2DMake(23.032331,72.56208)
        print(self.currentLC)
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways)
        {
            currentLocation = locManager.location
            print(currentLocation)
            if currentLocation == nil {
                currentlatitude = 23.032331
                currentlogitude = 72.56208
            }
            else
            {
                currentlatitude = (currentLocation.coordinate.latitude)
                currentlogitude = (currentLocation.coordinate.longitude)
            }
            
        }
        
        let specific_address = self.OrderdetailDict.value(forKey: "specific_address") as! String
        print(self.OrderdetailDict)
        
        let type = ApiUtillity.sharedInstance.getUserData(key: "type") as! String
        
        if type == "worker"
        {
            //latitude
            //longitude
            
            let username = self.OrderdetailDict.value(forKey: "worker_name") as! String
            let wlat = Double(self.OrderdetailDict.value(forKey: "latitude") as! String)
            let wlng = Double(self.OrderdetailDict.value(forKey: "longitude") as! String)
        
            self.Lbl_Header_Txt.text = "\(username)"
            //wlat
            //wlng
            
            
            currentLC = CLLocationCoordinate2DMake(currentlatitude,currentlogitude)
            let camera = GMSCameraPosition.camera(withLatitude: wlat!,longitude: wlng!, zoom: 15)
            self.Google_Map_View.camera = camera
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: wlat!, longitude: wlng!)
            marker.icon = UIImage(named : "placeholder")
            marker.map = self.Google_Map_View
            
        
            
        }
        else
        {
            let username = self.OrderdetailDict.value(forKey: "username") as! String
            let wlat = Double(self.OrderdetailDict.value(forKey: "latitude") as! String)
            let wlng = Double(self.OrderdetailDict.value(forKey: "longitude") as! String)
            
            self.Lbl_Header_Txt.text = "\(username)"
            //wlat
            //wlng
            
            
            currentLC = CLLocationCoordinate2DMake(currentlatitude,currentlogitude)
            let camera = GMSCameraPosition.camera(withLatitude: wlat!,longitude: wlng!, zoom: 15)
            self.Google_Map_View.camera = camera
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: wlat!, longitude: wlng!)
            marker.icon = UIImage(named : "placeholder")
            marker.map = self.Google_Map_View
            
        }
        
     
        
      
        
        
        // Do any additional setup after loading the view.
    }


    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- ALL FUNCTIONS
    
    

}
