//
//  ViewController.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import DLAutoSlidePageViewController


class ViewController: UIViewController
{

    var currentPageIndex = Int()
    
     @IBOutlet weak var containerView: UIView!
    
    public var pageControl: UIPageControl? {
        return UIPageControl.appearance(whenContainedInInstancesOf: [UIPageViewController.self])
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    // MARK: - Privatex
    
    private func setupElements() {
        setupPageViewController()
    }
    
    private func setupPageViewController() {
        let pages: [UIViewController] = setupPages()
        let pageViewController = DLAutoSlidePageViewController(pages: pages,
                                                               timeInterval: 10000000000000.0,
                                                               transitionStyle: .scroll,
                                                               interPageSpacing: 0.0)
        addChild(pageViewController)
       
        containerView.addSubview(pageViewController.view)
        
        
        
        pageControl?.currentPageIndicatorTintColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9fcee5")
        pageViewController.view.frame = containerView.bounds
    }
    
   private func viewControllerAtIndex(_ index: Int) -> UIViewController
   {
    guard index < setupPages().count else { return UIViewController() }
        currentPageIndex = index
    
    print(currentPageIndex)
    
    
    
    return setupPages()[index]
    }
    private func setupPages() -> [UIViewController]
    {
        let first = storyboard?.instantiateViewController(withIdentifier: "ZeroVC") as! ZeroVC
        let second = storyboard?.instantiateViewController(withIdentifier: "FirstVC") as! FirstVC
        let third = storyboard?.instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
        let forth = storyboard?.instantiateViewController(withIdentifier: "thirdVC") as! thirdVC
        
        
        
        return [first, second, third, forth]
    }

    
    
    
}

