//
//  AppDelegate.swift
//  HomeService
//
//  Created by Alex Grinberg on 06/12/17.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import CoreLocation
import GoogleMaps
import CoreLocation
import GooglePlaces
import GoogleMapsBase
import GooglePlacePicker
import Whisper
import IQKeyboardManagerSwift
import UserNotifications





@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,CLLocationManagerDelegate, MessagingDelegate
{

    var window: UIWindow?
     var locManager = CLLocationManager()
      var location1 = CLLocation()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        let User_id = ApiUtillity.sharedInstance.getUserData(key: "id")
        GMSServices.provideAPIKey("AIzaSyCzI9LgUsgIV_4Z5ZgVTohDvScUuvaoCiQ")
        GMSPlacesClient.provideAPIKey("AIzaSyCzI9LgUsgIV_4Z5ZgVTohDvScUuvaoCiQ")
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0


        
        if User_id.isEmpty == true
        {
            let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav:UINavigationController = UINavigationController(rootViewController: Push)
            nav.isNavigationBarHidden = true
            self.window?.rootViewController = nav
            
        }
            
        else
        {
            let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
            let nav:UINavigationController = UINavigationController(rootViewController: Push)
            nav.isNavigationBarHidden = true
            self.window?.rootViewController = nav
        }
        
        //        LOCATION
        
        self.locManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locManager.requestWhenInUseAuthorization()
        self.locManager.delegate = self
        self.locManager.distanceFilter = kCLDistanceFilterNone
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled()
        {
            locManager.delegate = self
            locManager.desiredAccuracy = kCLLocationAccuracyBest
            //set the amount of metres travelled before location update is made
            //            locManager.distanceFilter = CLLocationDistance(1)
            print(locManager.distanceFilter)
            
            locManager.startUpdatingLocation()
        }
        

        
        
        
        //        MARK:- NOTIFIACATION
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            //            Messaging.messaging().remoteMessageDelegate = self as? MessagingDelegate
        }
        else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        
        
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        UserDefaults.standard.set((locValue.latitude), forKey: "LATTITUDE")
        UserDefaults.standard.set((locValue.longitude), forKey: "LONGITUDE")
        UserDefaults.standard.synchronize()

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //    MARK:- NOTIFICATION
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        if let refreshedToken = InstanceID.instanceID().token()
        {
            print("InstanceID token: \(refreshedToken)")
            UserDefaults.standard.set(refreshedToken, forKey: "Vtoken")
            //            UserDefaults.standard.synchronize()
        }
    }
    
    
    @objc(applicationReceivedRemoteMessage:) func application(received remoteMessage: MessagingRemoteMessage)
    {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        //        print("InstanceID token: \(refreshedToken)")
        UserDefaults.standard.set(fcmToken, forKey: "Vtoken")
        UserDefaults.standard.synchronize()
        
        let dic:NSMutableDictionary = NSMutableDictionary.init(dictionary: ["Vtoken":fcmToken])
        ApiUtillity.sharedInstance.setIphoneData(data: dic)
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Print notification payload data
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
        //        let nav = self.window?.rootViewController as! UINavigationController
        //        let title = (data_item.value(forKey: "aps") as! NSDictionary).value(forKey: "alert") as! String
        //        let announcement = Announcement(title: title, subtitle: "", image: nil)
        //        Whisper.show(shout: announcement, to: nav, completion: {
        //
        
        //        })
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        
        let data_item = data as NSDictionary
        print(data_item)
        UIApplication.shared.applicationIconBadgeNumber = 0

        let push_type = data_item.value(forKey: "push_type") as! String
        
        let type = ApiUtillity.sharedInstance.getUserData(key: "type")
        print(type)
        
        if type == "user"
        {
 
             //push_type = 1 for place order
             //push_type = 2 for order place for user
             //push_type = 3 for modify date accept by user
             //push_type = 4 for order accepted by worker
             //push_type = 5 order completed
            //push_type = 6 review
             //push_type = 7 order cancel
             //
            
           // Push.name = MainData.value(forKey: "worker_name") as! String
           // Push.thread_id = MainData.value(forKey: "thread_id") as! String
            
            //order_id
            if push_type == "1"
            {
                
            }
             
            if push_type == "2"
            {
                let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                Push.selectedIndex = 2
                nav.isNavigationBarHidden = true
                
                self.window?.rootViewController = nav
            }
            else if push_type == "3"
            {
               
            }
            else if push_type == "4"
            {
                let Push = UserChatDetailVC()
                //user_name
                //thread_id
                //worker_name
                let thread_id = data_item.value(forKey: "thread_id") as! String
                let worker_name = data_item.value(forKey: "worker_name") as! String

                Push.thread_id = "\(thread_id)"
                Push.name = "\(worker_name)"
                
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "5"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "6"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "7"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }

        }
        else
        {
 
           
             
 
 
            
            if push_type == "1"
            {
                let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! tabbarVC
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                Push.selectedIndex = 1
                nav.isNavigationBarHidden = true
                
                self.window?.rootViewController = nav
            }
            
            if push_type == "2"
            {
               
            }
            else if push_type == "3"
            {
                let Push = UserChatDetailVC()
                //user_name
                //thread_id
                //worker_name
                let thread_id = data_item.value(forKey: "thread_id") as! String
                let worker_name = data_item.value(forKey: "user_name") as! String
                
                Push.thread_id = "\(thread_id)"
                Push.name = "\(worker_name)"
                
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "4"
            {
               
            }
            else if push_type == "5"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "6"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }
            else if push_type == "7"
            {
                let order_id = data_item.value(forKey: "order_id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = order_id
                Push.Is_Push_Check = "1"
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
            }

            
        }
 

        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
        //        let rootViewController = self.window!.rootViewController as! UINavigationController
        //        let profileViewController = NotificationVC()
        //        rootViewController.pushViewController(profileViewController, animated: true)
        print("Push notification received: \(data)")
    }


}

