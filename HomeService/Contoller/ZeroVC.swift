//
//  ZeroVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 18/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit

class ZeroVC: UIViewController {

    @IBOutlet weak var View_Status: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        View_Status.backgroundColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9dcce2")
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
