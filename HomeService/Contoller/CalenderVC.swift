//
//  CalenderVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 17/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire
import UserNotifications



class CalenderVC: UIViewController,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance,UITableViewDataSource,UITableViewDelegate,UITabBarDelegate,UNUserNotificationCenterDelegate
{
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var tbl_view: UITableView!
    @IBOutlet weak var Lbl_No_Results: UILabel!
     var type = String()
    
    

    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        //2018-07-25 06:03:51
        //2018-07-25 06:03:30
        return formatter
    }()
    
    //MARK:- VARIRABLES
    
    var CalendarEventsArray = NSMutableArray()
    var apiToken = String()
    var Token =  String()
    var FinalCalendarDataDisplayArray = NSMutableArray()
    var FinalDataDisplay = NSMutableArray()
    var NewThirdArray = NSMutableArray()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbl_view.isHidden = true
        
       // CalendarEventsArray = ["2018-10-25","2018-07-25","2018-07-21"]
        tbl_view.register(CalenderXIB.self, forCellReuseIdentifier: "CalenderXIB")
        tbl_view.register(UINib(nibName: "CalenderXIB", bundle: nil), forCellReuseIdentifier: "CalenderXIB")
        
        tbl_view.register(OrderListXIB.self, forCellReuseIdentifier: "OrderListXIB")
        tbl_view.register(UINib(nibName: "OrderListXIB", bundle: nil), forCellReuseIdentifier: "OrderListXIB")
        
        tbl_view.register(NoOrdersFoundTableviewCell.self, forCellReuseIdentifier: "NoOrdersFoundTableviewCell")
        tbl_view.register(UINib(nibName: "NoOrdersFoundTableviewCell", bundle: nil), forCellReuseIdentifier: "NoOrdersFoundTableviewCell")
        
        
        
        
        UNUserNotificationCenter.current().delegate = self
        

     
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        OrderListing()

    }


    
    //    MARK:- TABLEVIEW METHOD
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            if self.FinalDataDisplay.count == 0
            {
                return 1 

            }
            else
            {
                return FinalDataDisplay.count

            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cell:CalenderXIB = tableView.dequeueReusableCell(withIdentifier: "CalenderXIB", for: indexPath) as! CalenderXIB
            cell.calender_view.delegate = self
            cell.calender_view.dataSource = self
            cell.calender_view.reloadData()
            cell.calender_view.layer.borderColor = UIColor.white.cgColor
            cell.calender_view.appearance.titleFont = UIFont(name: "Proxima Nova Semibold", size: 17.0)
            cell.calender_view.appearance.weekdayFont = UIFont(name: "Proxima Nova Semibold", size: 17.0)
            cell.calender_view.appearance.subtitleFont = UIFont(name: "Proxima Nova Semibold", size: 17.0)
            cell.calender_view.appearance.headerTitleFont = UIFont(name: "Proxima Nova Semibold", size: 17.0)
            
            return cell
        }
        else
        {
            
            if self.FinalDataDisplay.count == 0
            {
               
                //NoOrdersFoundTableviewCell
                
                 let cell2:NoOrdersFoundTableviewCell = tableView.dequeueReusableCell(withIdentifier: "NoOrdersFoundTableviewCell", for: indexPath) as! NoOrdersFoundTableviewCell
                
                return cell2
                
            }
            else
            {
                let cell2:OrderListXIB = tableView.dequeueReusableCell(withIdentifier: "OrderListXIB", for: indexPath) as! OrderListXIB
                
                
                cell2.View_Background.layer.cornerRadius = 5.0
                cell2.View_Background.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                cell2.View_Background.layer.shadowOpacity = 4
                cell2.View_Background.layer.shadowOffset = CGSize.zero
                cell2.View_Background.layer.shadowRadius = 2
                cell2.View_Background.clipsToBounds = true
                cell2.View_Background.layer.masksToBounds = false
                cell2.View_Background.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
                cell2.View_Background.layer.borderWidth = 1.0
                
                print(self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary)
                
                let order_id = (self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary).value(forKey: "order_id") as! String
                
                let date_time = (self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary).value(forKey: "date_time") as! String
                
                let FinalDatadisplay = self.UTCToLocal(date: date_time)
                
                type = ApiUtillity.sharedInstance.getUserData(key: "type")
                print(type)
                
                if type == "user"
                {
                    
                }
                else
                {
                    
                }
                
                let username = (self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary).value(forKey: "username") as! String
                
                
                let worker_status = (self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary).value(forKey: "worker_status") as! String
                
                
                //worker_status pending
                
                if worker_status == "complete"
                {
                    cell2.imageview_check.isHidden = false
                }
                else
                {
                    cell2.imageview_check.isHidden = true
                }
                
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let showDate = inputFormatter.date(from: FinalDatadisplay)
                inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                let resultString = inputFormatter.string(from: showDate!)
                print(resultString)
                
                cell2.Lbl_Customer_Name.text = "Customer:" + " " + "\(username)" + " "
                cell2.Lbl_Order_Date.text = "Order date: " + "\(resultString)"
                cell2.Lbl_Order_Number.text = "#" + "\(order_id)"
                
                return cell2
                
            }
            
          
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0
        {
            return 314
        }
        else
        {
            return 100
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            
        }
        else
        {
            //id
            
            if FinalDataDisplay.count == 0
            {
                
            }
            else
            {
                let id = (self.FinalDataDisplay.object(at: indexPath.row)as! NSDictionary).value(forKey: "id") as! String
                let Push = WorkerOrderCompleted()
                Push.Order_id = "\(id)"
                self.navigationController?.pushViewController(Push, animated: true)
            }
            
           
            
            
        }
    }
    
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        //2018-07-25 06:03:30
        
        let dateString = self.dateFormatter2.string(from: date)
        
        //FinalDataDisplay
        
          self.CalendarEventsArray = (self.NewThirdArray.value(forKey: "order_date") as! NSArray).mutableCopy() as! NSMutableArray
        
        //self.CalendarEventsArray = (self.FinalDataDisplay.value(forKey: "order_date") as! NSArray).mutableCopy() as! NSMutableArray
        
        print(self.CalendarEventsArray)
        
        
        
        if self.CalendarEventsArray.count == 0
        {
            
        }
        else
        {
            print(self.CalendarEventsArray)
        }
        if self.CalendarEventsArray.contains(dateString)
        {
            return 1
        }
        else
        {
            return 0
        }
        
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition)
    {
        
        
    }
    
    
    //func minimumDate(for calendar: FSCalendar) -> Date
    //{
    //    let date = Date()
    
    
    //      return date
    
    //    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar)
    {
        //  print("change page to \(self.dateFormatter2.string(from: calendar.currentPage))")
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        
        let dateString = self.dateFormatter2.string(from: date)
        let FinalimageArray = NSMutableArray()

        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1)
        {
            // your code here
            
            self.FinalDataDisplay = self.NewThirdArray.mutableCopy() as! NSMutableArray
            
            for item in self.FinalDataDisplay
            {
                let P_Z = item as! NSDictionary
                print(P_Z)
                let date1display = P_Z.value(forKey: "date_time") as! String
                
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let showDate = inputFormatter.date(from: date1display)
                inputFormatter.dateFormat = "yyyy-MM-dd"
                let resultString = inputFormatter.string(from: showDate!)
                print(resultString)
                
                if resultString == dateString
                {
                    FinalimageArray.add(P_Z)
                }
                else
                {
                    // Badecountdisplay = Badecountdisplay + 1
                }
                
                print(FinalimageArray.count)
                print(FinalimageArray)
                
                if FinalimageArray.count == 0
                {
                    self.FinalDataDisplay = NSMutableArray()
                    
                    self.tbl_view.reloadData()
                    
                }
                else
                {
                    self.FinalDataDisplay = NSMutableArray()
                    self.FinalDataDisplay = FinalimageArray.mutableCopy() as! NSMutableArray
                    self.tbl_view.reloadData()
                }
                
                
        }
      
            ApiUtillity.sharedInstance.dismissSVProgressHUD()
        }
        
        
        /*
        if self.CalendarEventsArray.contains(dateString)
        {
           // let sectionToReload = 1
            //let indexSet: IndexSet = [sectionToReload]
            //self.tbl_view.reloadSections(indexSet, with: .automatic)
            //return

        }
        else
        {
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let showDate = inputFormatter.date(from: dateString)
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let resultString = inputFormatter.string(from: showDate!)
            print(resultString)
            
           
            
        }
 */
        
 }
    
    //MARK:- ALL FUNCTIONS
    
    func OrderListing()
    
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        let paramiter = ["date":result] as [String : Any]
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        print(paramiter)
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/get_order_list"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let Tempdict = dict.value(forKey: "data") as! NSArray
                    print(Tempdict.count)
                    print(Tempdict)
                    self.tbl_view.isHidden = false
                    if Tempdict.count == 0
                    {
                        self.FinalDataDisplay = Tempdict.mutableCopy() as! NSMutableArray
                        self.NewThirdArray = Tempdict.mutableCopy() as! NSMutableArray
                        self.tbl_view.reloadData()
                    }
                    else
                    {
                        self.FinalDataDisplay = Tempdict.mutableCopy() as! NSMutableArray
                        self.NewThirdArray = Tempdict.mutableCopy() as! NSMutableArray
                        self.tbl_view.reloadData()
                    }
                    
                    /*
                    let WorkerServicestoday_orderArray = (Tempdict.value(forKey: "today_order") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let WorkerServicestomorrow_order = (Tempdict.value(forKey: "tomorrow_order") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let WorkerServicesweek_order = (Tempdict.value(forKey: "week_order") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    
                    var FinalTodayDateArray =  NSMutableArray()
                    var FinalTomooroowDateArray =  NSMutableArray()
                    var FinalWeekDateArray =  NSMutableArray()
                    let FinalThirdArray = NSMutableArray()
                   
                     if WorkerServicestoday_orderArray.count == 0
                     {
                         FinalTodayDateArray =  NSMutableArray()
                        print(FinalTodayDateArray.count)
                        FinalThirdArray.addObjects(from: FinalTodayDateArray as! [Any])
                        self.FinalDataDisplay.addObjects(from: FinalTodayDateArray as! [Any])

                    }
                    else
                     {
                         FinalTodayDateArray = (WorkerServicestoday_orderArray.value(forKey: "date_time") as! NSArray).mutableCopy() as! NSMutableArray
                        print(FinalTodayDateArray.count)
                        
                        FinalThirdArray.addObjects(from: FinalTodayDateArray as! [Any])
                        self.FinalDataDisplay.addObjects(from: WorkerServicestoday_orderArray as! [Any])


                    }
                   
                    
                    if WorkerServicestomorrow_order.count == 0
                    {
                         FinalTomooroowDateArray =  NSMutableArray()
                        print(FinalTomooroowDateArray.count)
                        FinalThirdArray.addObjects(from: FinalTomooroowDateArray as! [Any])
                        self.FinalDataDisplay.addObjects(from: FinalTomooroowDateArray as! [Any])


                    }
                    else
                    {
                         FinalTomooroowDateArray = (WorkerServicestomorrow_order.value(forKey: "date_time") as! NSArray).mutableCopy() as! NSMutableArray
                        print(FinalTomooroowDateArray)
                         FinalThirdArray.addObjects(from: FinalTomooroowDateArray as! [Any])
                        self.FinalDataDisplay.addObjects(from: WorkerServicestomorrow_order as! [Any])

                    }
                    
                    
                    if WorkerServicesweek_order.count == 0
                    {
                         FinalWeekDateArray =  NSMutableArray()
                        print(FinalWeekDateArray.count)
                        FinalThirdArray.addObjects(from: FinalWeekDateArray as! [Any])
                         self.FinalDataDisplay.addObjects(from: FinalWeekDateArray as! [Any])
                        
                    }
                    else
                    {
                         FinalWeekDateArray = (WorkerServicesweek_order.value(forKey: "date_time") as! NSArray).mutableCopy() as! NSMutableArray
                        FinalThirdArray.addObjects(from: FinalWeekDateArray as! [Any])
                         self.FinalDataDisplay.addObjects(from: WorkerServicesweek_order as! [Any])

                        print(FinalWeekDateArray.count)
                    }
                    
                    print(FinalThirdArray)
                    print(FinalThirdArray.count)
                    print(self.FinalDataDisplay)
                    print(self.FinalDataDisplay.count)
                    */
                    
                    /*
 
                     2018-11-01 00:17:00",
                     "2018-11-03 12:35:00",
                     "2018-11-03 12:35:00"
                    */
                    
                    //  self.WorkerServicesArray = TempArray.mutableCopy() as! NSMutableArray
                    
                 //  self.CalendarEventsArray = FinalThirdArray.mutableCopy() as! NSMutableArray
                   // self.tbl_view.reloadData()
                    
                    
                }
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
        if type == "user"
        {
            if (data_item.value(forKey: "push_type") as! String) == "2"
            {
                OrderListing()
            }
        }
        else
        {
            if (data_item.value(forKey: "push_type") as! String) == "1"
            {
                OrderListing()
            }
        }
        
       
        
        completionHandler([.alert, .sound])
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    
    //MARK:- BUTTON ACTIONS
    
    
    
    
    @IBAction func Btn_Handler_Notifications(_ sender: Any)
    {
        let Push = WorkerNotificationsVC()
        self.navigationController?.pushViewController(Push, animated: true)
        
    }
    
}
