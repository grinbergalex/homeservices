//
//  ChatVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 17/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import UserNotifications




class ChatVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UNUserNotificationCenterDelegate
{
    //MARK:- OUTETS
    @IBOutlet weak var tbl_view: UITableView!
    
    @IBOutlet weak var Lbl_No_Messages_Found: UILabel!
    
    //MARK:- VAIRABLES
     var type = String()
    var apiToken = String()
    var Token =  String()
    var ChatListArray = NSMutableArray()
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        type = ApiUtillity.sharedInstance.getUserData(key: "type")
        print(type)
        

        self.Lbl_No_Messages_Found.isHidden = true
        
        tbl_view.register(ChatViewXIB.self, forCellReuseIdentifier: "ChatViewXIB")
        tbl_view.register(UINib(nibName: "ChatViewXIB", bundle: nil), forCellReuseIdentifier: "ChatViewXIB")
        
        tbl_view.register(ChatBoldXIB.self, forCellReuseIdentifier: "ChatBoldXIB")
        tbl_view.register(UINib(nibName: "ChatBoldXIB", bundle: nil), forCellReuseIdentifier: "ChatBoldXIB")
        
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            UNUserNotificationCenter.current().delegate = self
            
            if type == "user"
            {
                ChatThereadlist()
                ChatCount()
            }
            else
            {
                ChatThereadlist()
                ChatCount()
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Connection", ForNavigation: self.navigationController!)
            return
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.Lbl_No_Messages_Found.isHidden = true
        
       
        
//        if type == "user"
//        {
//            ChatThereadlist()
//        }
//        else
//        {
//            ChatThereadlist()
//        }
        ChatThereadlist()
        ChatCount()
        
    }
    
    //    MARK:- TABLEVIEW METHOD
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ChatListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
        
        
        
        print(self.ChatListArray.object(at: indexPath.row)as! NSDictionary)
        
        let id1 = ApiUtillity.sharedInstance.getUserData(key: "id")
        
        let name = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "name") as! String
        
        let profile_image = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "profile_image") as! String
        let dCreatedDate = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "dCreatedDate") as! String
        
          let FinalDisplay = self.UTCToLocal(date: dCreatedDate)
        
        let eStatus = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "eStatus") as! String
        //eStatus
        
        let iReceiverId  = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iReceiverId") as! String
        let iSenderId = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iSenderId") as! String
        
        if iReceiverId == id1
        {
            if eStatus == "read"
            {
                
                 let cell:ChatViewXIB = tableView.dequeueReusableCell(withIdentifier: "ChatViewXIB", for: indexPath) as! ChatViewXIB
                
                
                cell.lbl_for_message.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
                cell.lbl_for_user_name.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
                
                //cell.lbl_for_message.font = UIFont(name: "ProximaNova-Bold ", size: 17.0)
                cell.lbl_for_user_name.text = "\(name)"
                cell.user_image_view.kf.indicatorType = .activity
                cell.user_image_view.kf.setImage(with: URL(string: profile_image ))
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date (from: FinalDisplay)
                
                let String1 = date!.timeAgoSinceNow
                print(String1)
                cell.Lbl_Time_Duration.text = "\(String1)"
                
                return cell
                
            }
            else
            {
                //ProximaNova-Semibold 17.0
              
                
                let cell:ChatBoldXIB = tableView.dequeueReusableCell(withIdentifier: "ChatBoldXIB", for: indexPath) as! ChatBoldXIB
                
                
              //  cell.lbl_for_message.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
                //cell.lbl_for_user_name.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
                
                //cell.lbl_for_message.font = UIFont(name: "ProximaNova-Bold ", size: 17.0)
                cell.Lbl_User_name.text = "\(name)"
                cell.imageview_user.kf.indicatorType = .activity
                cell.imageview_user.kf.setImage(with: URL(string: profile_image ))
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date (from: FinalDisplay)
                
                let String1 = date!.timeAgoSinceNow
                print(String1)
                cell.Lbl_Time_Duration.text = "\(String1)"
                
                return cell
                
            }
            
        }
        else
            
            
        {
            
             let cell:ChatViewXIB = tableView.dequeueReusableCell(withIdentifier: "ChatViewXIB", for: indexPath) as! ChatViewXIB
            
            
            cell.lbl_for_message.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
            cell.lbl_for_user_name.font = UIFont(name: "ProximaNova-Regular", size: 17.0)
            
            //cell.lbl_for_message.font = UIFont(name: "ProximaNova-Bold ", size: 17.0)
            cell.lbl_for_user_name.text = "\(name)"
            cell.user_image_view.kf.indicatorType = .activity
            cell.user_image_view.kf.setImage(with: URL(string: profile_image ))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date (from: FinalDisplay)
            
            let String1 = date!.timeAgoSinceNow
            print(String1)
            cell.Lbl_Time_Duration.text = "\(String1)"
            
            return cell

        }
        
        
      
      

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if type == "user"
        {
            
            if self.ChatListArray.count == 0
            {
                
            }
            else
            {
                let Push = UserChatDetailVC()
                Push.Order_id = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iOrderId") as! String
                Push.name = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "name") as! String
                Push.thread_id = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iThreadId") as! String
                self.navigationController?.pushViewController(Push, animated: true)
            }
            
           
        }
        else
        {
            
            if self.ChatListArray.count == 0
            {
                
            }
            else
            {
                let Push = UserChatDetailVC()
                Push.Order_id = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iOrderId") as! String
                Push.name = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "name") as! String
                Push.thread_id = (self.ChatListArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "iThreadId") as! String
                self.navigationController?.pushViewController(Push, animated: true)
            }
           
        }
    }
    
    
    
    
    //MARK:- ALL FUNCTIONS
    
    //user/thread_unread_count

    
    func ChatCount()
    {
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/thread_unread_count"), method: .post, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let UserData = dict.value(forKey: "data") as! NSDictionary
                    
                    
                    print(UserData)
                    var Badecountdisplay = Int()
                    
                    let disp = UserData.value(forKey: "count") as! String
                    
                    Badecountdisplay = Int(disp)!
                    
                    
                    print(Badecountdisplay)
                    
                    
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        
                        if Badecountdisplay == 0
                        {
                            tabItem.badgeValue = ""
                            tabItem.badgeColor = UIColor.clear
                        }
                        else
                            
                        {
                            tabItem.badgeValue = "\(Badecountdisplay)"
                            tabItem.badgeColor = UIColor.darkGray
                        }
                        
                        
                    }
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    
                    
                    
                }
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
    }
    
    
    func ChatThereadlist()
    {
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
       
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/thread_list"), method: .post, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                     let UserData = dict.value(forKey: "data") as! NSArray
                    
                    if UserData.count == 0
                    {
                   
                       
                        self.Lbl_No_Messages_Found.isHidden = false
                        self.tbl_view.isHidden = true
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    }
                    else
                    {
                        self.Lbl_No_Messages_Found.isHidden = true
                        self.tbl_view.isHidden = false
                        self.ChatListArray = UserData.mutableCopy() as! NSMutableArray
                        self.tbl_view.reloadData()
                        
                      
                        }
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                    }
                    
                
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
    }
    
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    
    //    MARK:- NOTIFICATION
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
        
        if type == "user"
        {
            if (data_item.value(forKey: "push_type") as! String) == "2"
            {
                ChatThereadlist()
                ChatCount()
            }
        }
        else
        {
            if (data_item.value(forKey: "push_type") as! String) == "1"
            {
                ChatThereadlist()
                ChatCount()
            }
        }
        
        completionHandler([.alert, .sound])
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Notifications(_ sender: Any)
    {
        let Push = WorkerNotificationsVC()
        self.navigationController?.pushViewController(Push, animated: true)
        
    }
    
    
}
