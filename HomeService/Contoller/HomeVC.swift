//
//  HomeVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 17/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import Kingfisher
import UserNotifications




class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITabBarDelegate,CLLocationManagerDelegate,UNUserNotificationCenterDelegate
{
    //MARK:- OUTLETS
    
    var window: UIWindow?

    @IBOutlet weak var Lbl_Title: UILabel!
    @IBOutlet weak var Lbl_No_Records: UILabel!
    @IBOutlet weak var collection_view: UICollectionView!
    
    //MARK:- VARIABLES
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var currentlatitude =  Double()
    var currentlogitude =  Double()
    var currentLC = CLLocationCoordinate2D()
    var latitude = String()
    var logitude = String()
    var UserServicesArray = NSMutableArray()
    var WorkerServicestoday_orderArray = NSArray()
    var WorkerServicestomorrow_order = NSArray()
    var WorkerServicesweek_order = NSArray()
    var apiToken = String()
    var Token =  String()
    
    
    let ncObserver = NotificationCenter.default
    var type = String()
    
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        
        return _locationManager
    }()
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  self.navigationController?.navigationBar.isHidden = true
        
        
       
        self.collection_view.isHidden = true
        self.Lbl_No_Records.isHidden = true
        
        self.locManager.delegate = self
        self.locManager.startUpdatingLocation()
        self.currentLC = CLLocationCoordinate2DMake(23.032331,72.56208)
        print(self.currentLC)
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways)
        {
            currentLocation = locManager.location
            print(currentLocation)
            if currentLocation == nil {
                currentlatitude = 23.032331
                currentlogitude = 72.56208
            }
            else
            {
                currentlatitude = (currentLocation.coordinate.latitude)
                currentlogitude = (currentLocation.coordinate.longitude)
            }
            
        }
        
        currentLC = CLLocationCoordinate2DMake(currentlatitude,currentlogitude)

        
        
        UNUserNotificationCenter.current().delegate = self
        
        

        
        
        
        type = ApiUtillity.sharedInstance.getUserData(key: "type") 
        print(type)
        
        if type == "user"
        {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 20, left: 16, bottom: 10, right: 2)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
            self.collection_view!.collectionViewLayout = layout
            self.Lbl_Title.text = "Workers Search"
        }
        else
        {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            self.collection_view!.collectionViewLayout = layout
            self.Lbl_Title.text = "Activity"
        }
 
        self.collection_view.register(UINib(nibName: "HomeCollecationXIB", bundle: nil), forCellWithReuseIdentifier: "HomeCollecationXIB")
        
        self.collection_view.register(UINib(nibName: "WorkerHomeDayNameXIB", bundle: nil), forCellWithReuseIdentifier: "WorkerHomeDayNameXIB")
        
        self.collection_view.register(UINib(nibName: "WorkerHomeOrderDetailsXIB", bundle: nil), forCellWithReuseIdentifier: "WorkerHomeOrderDetailsXIB")
        
        self.collection_view.register(UINib(nibName: "NoOrdersFoundCell", bundle: nil), forCellWithReuseIdentifier: "NoOrdersFoundCell")
        
        
     //    ncObserver.addObserver(self, selector: #selector(self.badgeCount), name: Notification.Name("badgecount"), object: nil)
        
        self.collection_view.reloadData()
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            
            
           if type == "user"
           {
               // HomeUserService()
            }
            else
           {
                //HomeWorkerService()
            }
            
            
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Connection", ForNavigation: self.navigationController!)
            return
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.Lbl_No_Records.isHidden = true
        
   //     self.navigationController?.navigationBar.isHidden = true

        
        if type == "user"
        {
            HomeUserService()
        }
        else
        {
            HomeWorkerService()
        }
        
        
        
        
    }
    
    //MARK:- ALL FUNCTIONS
    
    
    func Threadlist()
    {
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/thread_unread_count"), method: .post, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let UserData = dict.value(forKey: "data") as! NSDictionary
                    
                 
                        print(UserData)
                        var Badecountdisplay = Int()
                        
                        let disp = UserData.value(forKey: "count") as! String
                        
                        Badecountdisplay = Int(disp)!
                       
                        
                        print(Badecountdisplay)
                        
                        
                        if let tabItems = self.tabBarController?.tabBar.items {
                            // In this case we want to modify the badge number of the third tab:
                            let tabItem = tabItems[2]
                            
                            if Badecountdisplay == 0
                            {
                                tabItem.badgeValue = ""
                                tabItem.badgeColor = UIColor.clear
                            }
                            else
                            
                            {
                                tabItem.badgeValue = "\(Badecountdisplay)"
                                tabItem.badgeColor = UIColor.darkGray
                            }
                            
                           
                        }
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                    
                    
                }
                    
                else if StatusCode==401
                {
                  
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
    }
    
    func HomeWorkerService()
    {
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
         let paramiter = ["limit":"","offset":""] as [String : Any]
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        print(paramiter)
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/activate_order"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                 print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let Tempdict = dict.value(forKey: "data") as! NSDictionary
                   
                    
                    
                    self.WorkerServicestoday_orderArray = Tempdict.value(forKey: "today_order") as! NSArray
                    
                    self.WorkerServicestomorrow_order = Tempdict.value(forKey: "tomorrow_order") as! NSArray
                    
                    self.WorkerServicesweek_order = Tempdict.value(forKey: "week_order") as! NSArray
                    
                    
                    if self.WorkerServicesweek_order.count == 0 && self.WorkerServicestomorrow_order.count == 0 && self.WorkerServicestoday_orderArray.count == 0
                    {
                        self.Lbl_No_Records.isHidden = false
                        self.collection_view.isHidden = true
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    }
                    else
                    {
                        self.Lbl_No_Records.isHidden = true
                        self.collection_view.isHidden = false
                        
                        self.collection_view.reloadData()
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                    }
                    
                    self.Threadlist()
                      //  self.WorkerServicesArray = TempArray.mutableCopy() as! NSMutableArray
                    
                        
                        
                        
                    }
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
        
    }
    
    
    
    func HomeUserService()
    {
        
        
        Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        apiToken = "Bearer \(Token)"
        
        let paramiter = ["latitude":"\(currentlatitude)", "longitude":"\(currentlogitude)","search":"","limit":"","offset":""] as [String : Any]
        
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        let headers = ["Vauthtoken":apiToken]
        
        print(paramiter)
        ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
        Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/get_worker_list"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
            //            debugPrint(response)
            if let json = response.result.value {
                let dict:NSDictionary = (json as? NSDictionary)!
                // print(dict)
                //                                print(response)
                
                let StatusCode = dict.value(forKey: "status") as! Int
                
                if StatusCode==200
                {
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    let TempArray = dict.value(forKey: "data") as! NSArray
                    
                    var TempInt = Int()
                    TempInt = TempArray.count
                    
                    if (dict.value(forKey: "data") as! NSArray).count == 0
                        
                    {
                        self.Lbl_No_Records.isHidden = false
                        self.collection_view.isHidden = true
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                    }
                    else
                    {
                        
                        self.Lbl_No_Records.isHidden = true
                        self.collection_view.isHidden = false
                        
                        self.UserServicesArray = TempArray.mutableCopy() as! NSMutableArray
                        self.collection_view.reloadData()
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        
                    }
                    
                    
                    self.Threadlist()
                    
                    
                }
                    
                else if StatusCode==401
                {
                    ApiUtillity.sharedInstance.dismissSVProgressHUD()
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    let push =  LoginVC()
                    self.navigationController?.pushViewController(push, animated: true)
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                    let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                    
                    
                }
                
            }
            else {
                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                
            }
        }
    }
    
    
    @objc func badgeCount()
    {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = ""
            tabItem.badgeColor = UIColor.clear
        }
        
    }
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    
    //MARK:- ALL FUNCTIONS
    
    @objc func ORDERNOW(sender:UIButton)
    {
        print(self.UserServicesArray.object(at: sender.tag)as! NSDictionary)
        let Tempdict = (self.UserServicesArray.object(at: sender.tag)as! NSDictionary)
        
        
        
     //   let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserNewProfileVC") as! UserNewProfileVC
        //let Push = WorkerProfileVC()
        let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserNewProfileVC") as! UserNewProfileVC
        Push.WorkerProfileDict = Tempdict.mutableCopy() as! NSMutableDictionary
        let nav:UINavigationController = UINavigationController(rootViewController: Push)
        nav.isNavigationBarHidden = true
        self.window?.rootViewController = nav
       
        self.navigationController?.pushViewController(Push, animated: true)
      
        
    }
    
    //    MARK:- NOTIFICATION
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let data_item = notification.request.content.userInfo as NSDictionary
        print(data_item)
        
        if (data_item.value(forKey: "push_type") as! String) == "1"
        {
            if type == "user"
            {
                
            }
            else
            {
                HomeWorkerService()
            }
        }
        
        completionHandler([.alert, .sound])
    }
    
    
    // MARK:- COLLECTIONVIEW METHODS
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        if type == "user"
        {
            return 1
        }
        else
        {
            return 6
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if type == "user"
        {
            return self.UserServicesArray.count
        }
        else
        {
            if section == 0
            {
                return 1
            }
            else if section == 1
            {
                
                if self.WorkerServicestoday_orderArray.count == 0
                {
                    return 1
                }
                else
                {
                    return self.WorkerServicestoday_orderArray.count

                }
                
            }
            else if section == 2
            {
                
               return 1
                
             
            }
            else if section == 3
            {
                
                if self.WorkerServicestomorrow_order.count == 0
                {
                    return 1
                }
                else
                {
                    return self.WorkerServicestomorrow_order.count

                }
                
            }
            else if section == 4
            {
                return 1
            }
            else
            {
                if self.WorkerServicesweek_order.count == 0
                {
                    return 1
                }
                else
                {
                    return self.WorkerServicesweek_order.count
                    
                }
            }
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if type == "user"
        {
            let cell:HomeCollecationXIB = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollecationXIB", for: indexPath) as! HomeCollecationXIB
            
            print(self.UserServicesArray.object(at: indexPath.row) as! NSDictionary)
            
            let distance = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "distance") as! String
            let myDouble = Double(distance)
            print(myDouble)
            let text = String(format: "%.0f", arguments: [myDouble!])
            print(text)
            
            let Display = Int(text)
            
            if Display! > 999
            {
                    cell.Lbl_Distance.text = "\(text)" + " K"
            }
            else
            {
                cell.Lbl_Distance.text = "\(text)" + " KM away"
            }
            
          
            
            
            
            
            let gen_rattings = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "gen_rattings") as! String
            let name = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as! String
            
            let online = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "online") as! String
            
            let proficiency  = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "proficiency") as! String
            let profile_image = (self.UserServicesArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "profile_image") as! String
            
            
            if online == "1"
            {
                cell.Lbl_Online.text = "Online"
                cell.imageview_online.image = UIImage(named: "online")
            }
            else
            {
                cell.Lbl_Online.text = "Offline"
                cell.imageview_online.image = UIImage(named: "offline")
            }
            
            
            
            cell.Lbl_Gen_Ratings.text = "\(gen_rattings)" + "/5"
            cell.lbl_for_worker_name.text = "\(name)"
            cell.lbl_for_sub_title.text = "\(proficiency)"
            cell.main_image_view.kf.indicatorType = .activity
           cell.main_image_view.kf.setImage(with: URL(string: profile_image ))
            
            cell.main_view.layer.cornerRadius = 5.0
            cell.main_view.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "A9A9A9").cgColor
            cell.main_view.layer.shadowOpacity = 4
            cell.main_view.layer.shadowOffset = CGSize.zero
            cell.main_view.layer.shadowRadius = 2
            cell.main_view.clipsToBounds = true
            cell.main_view.layer.masksToBounds = false
            cell.main_view.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.main_view.layer.borderWidth = 0.1
            
            
            
            cell.btn_for_order_now.roundCorners([.bottomLeft , .bottomRight], radius: 5)
            
            cell.Lbl_Distance.roundCorners([.topLeft , .topRight], radius: 5)
            
            
            
            cell.btn_for_order_now.tag = indexPath.row
           
            cell.btn_for_order_now.addTarget(self,action:#selector(self.ORDERNOW(sender:)), for: .touchUpInside)
            
            
            
            //distance
            //gen_rattings
            //name
            //online
            //proficiency
            //profile_image
            
            return cell
        }
        else
        {
            
           
            
            let cell1:WorkerHomeDayNameXIB = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerHomeDayNameXIB", for: indexPath) as! WorkerHomeDayNameXIB
            
            let cell2:WorkerHomeOrderDetailsXIB = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerHomeOrderDetailsXIB", for: indexPath) as! WorkerHomeOrderDetailsXIB
            
            let cell3:NoOrdersFoundCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoOrdersFoundCell", for: indexPath) as! NoOrdersFoundCell
            
            
            cell3.View_Backgound.layer.cornerRadius = 5.0
            cell3.View_Backgound.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell3.View_Backgound.layer.shadowOpacity = 4
            cell3.View_Backgound.layer.shadowOffset = CGSize.zero
            cell3.View_Backgound.layer.shadowRadius = 2
            cell3.View_Backgound.clipsToBounds = true
            cell3.View_Backgound.layer.masksToBounds = false
            cell3.View_Backgound.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell3.View_Backgound.layer.borderWidth = 1.0
           
            
            cell2.View_Background.layer.cornerRadius = 5.0
            cell2.View_Background.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell2.View_Background.layer.shadowOpacity = 4
            cell2.View_Background.layer.shadowOffset = CGSize.zero
            cell2.View_Background.layer.shadowRadius = 2
            cell2.View_Background.clipsToBounds = true
            cell2.View_Background.layer.masksToBounds = false
            cell2.View_Background.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell2.View_Background.layer.borderWidth = 1.0
            
            
                if indexPath.section == 0
                {
                    cell1.Lbl_Date.text = "Today Orders"
                    return cell1
                }
                else if indexPath.section == 1
                {
                    
                    if self.WorkerServicestoday_orderArray.count == 0
                    {
                        return cell3
                    }
                    else
                    {
                        
                        print(self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary)
                        
                        let order_id = (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "order_id") as! String
                        
                        let date_time = (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "date_time") as! String
                        
                        
                        
                        let first_name = (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "first_name") as! String
                        
                        let last_name = (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "last_name") as! String
                        
                        let worker_status = (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "worker_status") as! String
                        
                        let FinalDatadisplay = self.UTCToLocal(date: date_time)
                        
                        //worker_status pending
                        
                        if worker_status == "complete"
                        {
                            cell2.imageview_check.isHidden = false
                        }
                        else
                        {
                            cell2.imageview_check.isHidden = true
                        }
                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: FinalDatadisplay)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        cell2.Lbl_Customer.text = "Customer:" + " " + "\(first_name)" + " " + "\(last_name)"
                        cell2.Lbl_Order_date.text = "Order date: " + "\(resultString)"
                        cell2.Lbl_Order_Time.text = "\(order_id)"
                        
                        return cell2
                    }
                }
                    
                    else if indexPath.section == 2
                    {
                        cell1.Lbl_Date.text = "Tomorrow Orders"
                        
                        
                        
                        return cell1
                    }
                    
                    
                
                else if indexPath.section == 3
                {
                    
                    if self.WorkerServicestomorrow_order.count == 0
                    {
                        return cell3
                    }
                    else
                    {
                        print(self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary)
                        
                        let order_id = (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "order_id") as! String
                        
                        let date_time = (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "date_time") as! String
                        
                        let first_name = (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "first_name") as! String
                        
                        let last_name = (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "last_name") as! String
                        
                        let worker_status = (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "worker_status") as! String
                        
                        let FinalDatadisplay = self.UTCToLocal(date: date_time)

                        
                        //worker_status pending
                        
                        if worker_status == "pending"
                        {
                            cell2.imageview_check.isHidden = true
                        }
                        else
                        {
                            cell2.imageview_check.isHidden = false
                        }
                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: FinalDatadisplay)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        cell2.Lbl_Customer.text = "Customer: " + " " + "\(first_name)" + " " + "\(last_name)"
                        cell2.Lbl_Order_date.text = "Order date:" + "\(resultString)"
                        cell2.Lbl_Order_Time.text = "\(order_id)"
                        
                        return cell2
                    }
                    
                    
                   
                }
                else if indexPath.section == 4
                {
                    cell1.Lbl_Date.text = "Week Orders"
                    return cell1
                }
                else
                {
                    
                    if self.WorkerServicesweek_order.count == 0
                    {
                        return cell3
                    }
                    else
                    {
                        print(self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary)
                        
                        let order_id = (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "order_id") as! String
                        
                        let date_time = (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "date_time") as! String
                        
                        let first_name = (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "first_name") as! String
                        
                        let last_name = (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "last_name") as! String
                        
                        let worker_status = (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "worker_status") as! String
                        
                        
                        let FinalDatadisplay = self.UTCToLocal(date: date_time)

                        
                        //worker_status pending
                        
                        if worker_status == "pending"
                        {
                            cell2.imageview_check.isHidden = true
                        }
                        else
                        {
                            cell2.imageview_check.isHidden = false
                        }
                        
                        let inputFormatter = DateFormatter()
                        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let showDate = inputFormatter.date(from: FinalDatadisplay)
                        inputFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                        let resultString = inputFormatter.string(from: showDate!)
                        print(resultString)
                        
                        cell2.Lbl_Customer.text = "Customer:" + " " + "\(first_name)" + " " + "\(last_name)"
                        cell2.Lbl_Order_date.text = "Order date: " + "\(resultString)"
                        cell2.Lbl_Order_Time.text = "\(order_id)"
                        
                        
                        return cell2
                    }
                    
                    
                    
                }
            
        }
        
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if type == "user"
        {
            //return CGSize(width: (UIScreen.main.bounds.size.width - 10)/2, height: 250)
             return CGSize(width: ((UIScreen.main.bounds.size.width)/2)-15, height: 210)

        }
        else
        {
            if indexPath.section == 0
            {
                return CGSize(width: UIScreen.main.bounds.size.width, height: 30)
            }
            else if indexPath.section == 1
            {
                  return CGSize(width: UIScreen.main.bounds.size.width, height: 100)
               
            }
            else if indexPath.section == 2
            {
               return CGSize(width: UIScreen.main.bounds.size.width, height: 30)
            }
            else if indexPath.section == 3
            {
                return CGSize(width: UIScreen.main.bounds.size.width, height: 100)
            }
            else if indexPath.section == 4
            {
               return CGSize(width: UIScreen.main.bounds.size.width, height: 30)
            }
            else
            {
                  return CGSize(width: UIScreen.main.bounds.size.width, height: 100)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if type == "user"
        {
            
            if self.UserServicesArray.count == 0
            {
                
            }
            else
            {
                print(self.UserServicesArray.object(at: indexPath.row)as! NSDictionary)
                let Tempdict = (self.UserServicesArray.object(at: indexPath.row)as! NSDictionary)
                
                let Push = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserNewProfileVC") as! UserNewProfileVC
                Push.WorkerProfileDict = Tempdict.mutableCopy() as! NSMutableDictionary
                let nav:UINavigationController = UINavigationController(rootViewController: Push)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                
                self.navigationController?.pushViewController(Push, animated: true)
                
            }
            
          
            
        }
        else
            
            
        {
            
            print(indexPath.row)
            print(indexPath.section)
            
            if indexPath.section == 1
            {
                
                if self.WorkerServicestoday_orderArray.count == 0
                {
                    
                }
                else
                {
                    let id =  (self.WorkerServicestoday_orderArray.object(at: indexPath.row)as! NSDictionary).value(forKey: "id") as! String
                    
                    let Push = WorkerOrderCompleted()
                    Push.Order_id = "\(id)"
                    self.navigationController?.pushViewController(Push, animated: true)
                    
                }
                
                
                
            }
            else if indexPath.section == 3
            {
                
                if self.WorkerServicestomorrow_order.count == 0
                {
                    
                }
                else
                {
                    let id =  (self.WorkerServicestomorrow_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "id") as! String
                    
                    let Push = WorkerOrderCompleted()
                    Push.Order_id = "\(id)"
                    self.navigationController?.pushViewController(Push, animated: true)
                }
                
                
                
            }
            else if indexPath.section == 5
            {
                
                if self.WorkerServicesweek_order.count == 0
                {
                    
                }
                else
                {
                    
                    let id =  (self.WorkerServicesweek_order.object(at: indexPath.row)as! NSDictionary).value(forKey: "id") as! String
                    
                    let Push = WorkerOrderCompleted()
                    Push.Order_id = "\(id)"
                    self.navigationController?.pushViewController(Push, animated: true)
                }
                
                
            }
            
            
            
           
            
        }
    }
    //MARK:- BUTTON ACTIONS
    
    
    @IBAction func Btn_Handler_Notifications(_ sender: Any)
    {
        let Push = WorkerNotificationsVC()
        self.navigationController?.pushViewController(Push, animated: true)
        
        
        
    }
    

}


extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}



