//
//  tabbarVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 17/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit

class tabbarVC: UITabBarController{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for:.normal)
        //
        //        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for:.selected)
        //
        //        tabBar.tintColor = UIColor.white
        //
        
        
        let Home: UITabBarItem? = tabBar.items?[0]
        let HomeImage = UIImage(named: "worker")?.withRenderingMode(.alwaysOriginal)
        let HomeUnselectedImage = UIImage(named: "worker_unselect")?.withRenderingMode(.alwaysOriginal)
        Home?.selectedImage = HomeImage
        Home?.image = HomeUnselectedImage
        
        let timerer: UITabBarItem? = tabBar.items?[1]
        let FavouritesImage = UIImage(named: "calendar")?.withRenderingMode(.alwaysOriginal)
        let FavouritesUnselectedImage = UIImage(named: "calendar_unselect")?.withRenderingMode(.alwaysOriginal)
        timerer?.selectedImage = FavouritesImage
        timerer?.image = FavouritesUnselectedImage
        
        let Profile: UITabBarItem? = tabBar.items?[2]
        let ProfileImage = UIImage(named: "chat")?.withRenderingMode(.alwaysOriginal)
        let ProfileUnselectedImage = UIImage(named: "chat_unselect")?.withRenderingMode(.alwaysOriginal)
        Profile?.selectedImage = ProfileImage
        Profile?.image = ProfileUnselectedImage
        
        let Settings: UITabBarItem? = tabBar.items?[3]
        let SettingsImage = UIImage(named: "profile")?.withRenderingMode(.alwaysOriginal)
        let SettingsUnselectedImage = UIImage(named: "profile_unselect")?.withRenderingMode(.alwaysOriginal)
        Settings?.selectedImage = SettingsImage
        Settings?.image = SettingsUnselectedImage
        
        // Do any additional setup after loading the view.

    }
    
}
