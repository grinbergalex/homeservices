//
//  ProfileVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 17/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI
import Foundation

class ProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITabBarDelegate,MFMailComposeViewControllerDelegate
{
    
    var edit_name = String()
    var edit_email = String()
    var edit_mobile_number = String()
    var edit_address = String()
     var type = String()
    var apiToken = String()
    var Token =  String()
    
    
    @IBOutlet weak var tbl_view: UITableView!
    
       let notification = NotificationCenter.default
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tbl_view.register(ProfileImageXIB.self, forCellReuseIdentifier: "ProfileImageXIB")
        tbl_view.register(UINib(nibName: "ProfileImageXIB", bundle: nil), forCellReuseIdentifier: "ProfileImageXIB")
        
        tbl_view.register(ProfileCellXIB.self, forCellReuseIdentifier: "ProfileCellXIB")
        tbl_view.register(UINib(nibName: "ProfileCellXIB", bundle: nil), forCellReuseIdentifier: "ProfileCellXIB")
        
      
        type = ApiUtillity.sharedInstance.getUserData(key: "type")
        
        
        type = ApiUtillity.sharedInstance.getUserData(key: "type")
        print(type)
        
        self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
        self.apiToken = "Bearer \(self.Token)"
        
        
        
        //        MARK:- NOTIFICATION CALLED
         notification.post(name: Notification.Name("badgecount"), object: nil)
         
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        edit_name = ApiUtillity.sharedInstance.getUserData(key: "name")
        edit_email = ApiUtillity.sharedInstance.getUserData(key: "email")
        edit_mobile_number = ApiUtillity.sharedInstance.getUserData(key: "mobile")
        edit_address = ApiUtillity.sharedInstance.getUserData(key: "address")
        
        self.tbl_view.reloadData()
    }

    
    //    MARK:- TABLEVIEW METHOD
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            if type == "user"
            {
                return 7

            }
            else
            {
                return 8

            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell:ProfileImageXIB = tableView.dequeueReusableCell(withIdentifier: "ProfileImageXIB", for: indexPath) as! ProfileImageXIB
            
            
            let image_url = ApiUtillity.sharedInstance.getUserData(key: "profile_image")
            cell.user_pf_image_view.kf.indicatorType = .activity
            cell.user_pf_image_view.kf.setImage(with: URL(string: image_url))
            
            cell.user_pf_image_view.layer.cornerRadius = 40
            cell.user_pf_image_view.layer.shadowColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
           cell.user_pf_image_view.layer.shadowOpacity = 4
            cell.user_pf_image_view.layer.shadowOffset = CGSize.zero
            cell.user_pf_image_view.layer.shadowRadius = 4
            cell.user_pf_image_view.clipsToBounds = true
            //cell.user_pf_image_view.layer.masksToBounds = false
            cell.user_pf_image_view.layer.borderColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9CC1D4").cgColor
            cell.user_pf_image_view.layer.borderWidth = 3.0
            
            
            cell.lbl_for_name.text = ApiUtillity.sharedInstance.getUserData(key: "name")
             cell.lbl_for_email_address.text = ApiUtillity.sharedInstance.getUserData(key: "email")
            cell.lbl_for_mobile_number.text = ApiUtillity.sharedInstance.getUserData(key: "mobile")
            
            
            return cell
        }
        else
        {
            
            
            if self.type == "user"
            {
                let cell:ProfileCellXIB = tableView.dequeueReusableCell(withIdentifier: "ProfileCellXIB", for: indexPath) as! ProfileCellXIB
                if indexPath.row == 0
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Edit Profile"
                }
                else if indexPath.row == 1
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Change Password"
                }
               
                else if indexPath.row == 2
                {
                    cell.image_for_arrow.isHidden = true
                    cell.image_for_switch.isHidden = false
                    cell.lbl_for_name.text = "Notifications"
                    
                    let online = ApiUtillity.sharedInstance.getUserData(key: "notification")
                    
                    if online == "1"
                    {
                        cell.image_for_switch.image = UIImage(named: "switch")
                    }
                    else
                    {
                        cell.image_for_switch.image = UIImage(named: "New_switch")
                    }
                    
                    
                }
                else if indexPath.row == 3
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "About us"
                }
                else if indexPath.row == 4
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Contact us"
                }
                    else if indexPath.row == 5
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Terms And Privacy Policy"
                    
                }
                    
                else
                {
                    cell.image_for_arrow.isHidden = true
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Logout"
                    cell.lbl_for_name.textColor = UIColor.red
                }
                return cell
            }
            else
            {
                let cell:ProfileCellXIB = tableView.dequeueReusableCell(withIdentifier: "ProfileCellXIB", for: indexPath) as! ProfileCellXIB
                if indexPath.row == 0
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Edit Profile"
                }
                else if indexPath.row == 1
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Change Password"
                }
                else if indexPath.row == 2
                {
                    cell.image_for_arrow.isHidden = true
                    cell.image_for_switch.isHidden = false
                    
                    
                    //notification = 1;
                    //online = 1;
                    
                    let online = ApiUtillity.sharedInstance.getUserData(key: "online")
                    
                    if online == "1"
                    {
                        cell.image_for_switch.image = UIImage(named: "switch")
                        cell.lbl_for_name.text = "Online"
                    }
                    else
                    {
                        cell.image_for_switch.image = UIImage(named: "New_switch")
                        cell.lbl_for_name.text = "Offline"
                    }
                    
                    
                }
                else if indexPath.row == 3
                {
                    cell.image_for_arrow.isHidden = true
                    cell.image_for_switch.isHidden = false
                    cell.lbl_for_name.text = "Notifications"
                    
                    let online = ApiUtillity.sharedInstance.getUserData(key: "notification")
                    
                    if online == "1"
                    {
                        cell.image_for_switch.image = UIImage(named: "switch")
                    }
                    else
                    {
                        cell.image_for_switch.image = UIImage(named: "New_switch")
                    }
                    
                    
                }
                else if indexPath.row == 4
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "About us"
                }
                else if indexPath.row == 5
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Contact us"
                }
                    
                else if indexPath.row == 6
                {
                    cell.image_for_arrow.isHidden = false
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Terms And Privacy Policy"
                    
                }
                    
                else
                {
                    cell.image_for_arrow.isHidden = true
                    cell.image_for_switch.isHidden = true
                    cell.lbl_for_name.text = "Logout"
                    cell.lbl_for_name.textColor = UIColor.red
                }
                return cell
            }
            
            
          
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 190
        }
        else
        {
            return 40
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            if type == "user"
            {
                let push = EditProfileVC()
                self.navigationController?.pushViewController(push, animated: true)
            }
            else
            {
                let push = WorkerEditProfileVC()
                self.navigationController?.pushViewController(push, animated: true)
            }
           
        }
            
            if self.type == "user"
            {
                 if indexPath.row == 1
                {
                    let push = ChangePasswordVC()
                    self.navigationController?.pushViewController(push, animated: true)
                }
                    
                else if indexPath.row == 2
                {
                    //notifcation
                    
                    
                    // online
                    var notification = String()
                    notification = ApiUtillity.sharedInstance.getUserData(key: "notification")
                    
                    if notification == "1"
                    {
                        notification = "0"
                    }
                    else
                    {
                        notification = "1"
                        
                    }
                    
                    let headers = ["Vauthtoken":apiToken]
                    
                    let paramiter = ["notification":notification] as [String : Any]
                    print(paramiter)
                    ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
                    Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/notification_on_off"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                        //            debugPrint(response)
                        if let json = response.result.value {
                            let dict:NSDictionary = (json as? NSDictionary)!
                            print(dict)
                            
                            let StatusCode = dict.value(forKey: "status") as! Int
                            if StatusCode==200
                            {
                                self.MyProfile()
                                
                            }
                            else if StatusCode==401
                            {
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                let push =  LoginVC()
                                self.navigationController?.pushViewController(push, animated: true)
                                
                                
                                
                                
                            }
                            else
                            {
                                
                                
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                
                            }
                            
                        }
                        else {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                            
                        }
                    }
                    
                    
                }
                    
                    
                    
                    
                else if indexPath.row == 3
                {
                    let Push = WorkerAboutVC()
                    self.navigationController?.pushViewController(Push, animated: true)
                    
                }
                else if indexPath.row == 4
                {
                    //  let Push = WorkerProfileVC()
                    // self.navigationController?.pushViewController(Push, animated: true)
                    
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients([""])
                        mail.setMessageBody("", isHTML: false)
                        
                        present(mail, animated: true)
                    } else
                    {
                        // show failure alert
                    }
                    
                }
                else if indexPath.row == 5
                 {
                    let Push = TermsAndNewVC()
                    self.navigationController?.pushViewController(Push, animated: true)
                }
                
                
                if indexPath.row == 6
                {
                    
                    let uiAlert = UIAlertController(title: "Home Services", message: "Are You Sure Want To Logout?", preferredStyle: UIAlertController.Style.alert)
                    self.present(uiAlert, animated: true, completion: nil)
                    
                    uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        if ApiUtillity.sharedInstance.isReachable()
                        {
                            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
                            self.apiToken = "Bearer \(self.Token)"
                            let headers = ["Vauthtoken":self.apiToken]
                            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
                            
                            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/logout"), method: .get, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                                debugPrint(response)
                                if let json = response.result.value {
                                    let dict:NSDictionary = (json as? NSDictionary)!
                                    
                                    let StatusCode = dict.value(forKey: "status") as! Int
                                    
                                    if StatusCode==200
                                    {
                                        let SuccessDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                        let TempData = NSDictionary()
                                        ApiUtillity.sharedInstance.setUserData(data: TempData)
                                        let Home = LoginVC()
                                        self.navigationController?.pushViewController(Home, animated: true)
                                        
                                        
                                    }
                                    else if StatusCode==401
                                    {
                                        
                                        
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        UserDefaults.standard.set(false, forKey:"loggedin")
                                        
                                        
                                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                        
                                        
                                        let Home:LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! LoginVC
                                        self.navigationController?.pushViewController(Home, animated: true)
                                        
                                    }
                                        
                                        
                                    else if StatusCode==412
                                    {
                                        
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                        
                                        
                                    }
                                        
                                    else
                                    {
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        
                                        
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                        
                                        
                                    }
                                    
                                }
                                else {
                                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                                }
                            }
                        }
                        else
                        {
                            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
                            return
                        }
                        
                    }))
                    
                    uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                        
                    }))
                    
                }
            }
                else
            {
                
                 if indexPath.row == 1
                {
                    let push = ChangePasswordVC()
                    self.navigationController?.pushViewController(push, animated: true)
                }
                    
                else if indexPath.row == 2
                {
                    // online
                    var online = String()
                    online = ApiUtillity.sharedInstance.getUserData(key: "online")
                    
                    if online == "1"
                    {
                        online = "0"
                    }
                    else
                    {
                        online = "1"
                        
                    }
                    
                    let headers = ["Vauthtoken":apiToken]
                    
                    let paramiter = ["online":online] as [String : Any]
                    print(paramiter)
                    ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
                    Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/online_offline"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                        //            debugPrint(response)
                        if let json = response.result.value {
                            let dict:NSDictionary = (json as? NSDictionary)!
                            print(dict)
                            
                            let StatusCode = dict.value(forKey: "status") as! Int
                            if StatusCode==200
                            {
                                self.MyProfile()
                                
                            }
                            else if StatusCode==401
                            {
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                let push =  LoginVC()
                                self.navigationController?.pushViewController(push, animated: true)
                                
                                
                                
                                
                            }
                            else
                            {
                                
                                
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                
                            }
                            
                        }
                        else {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                            
                        }
                    }
                    
                    
                }
                    
                else if indexPath.row == 3
                {
                    //notifcation
                    
                    
                    // online
                    var notification = String()
                    notification = ApiUtillity.sharedInstance.getUserData(key: "notification")
                    
                    if notification == "1"
                    {
                        notification = "0"
                    }
                    else
                    {
                        notification = "1"
                        
                    }
                    
                    let headers = ["Vauthtoken":apiToken]
                    
                    let paramiter = ["notification":notification] as [String : Any]
                    print(paramiter)
                    ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
                    Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/notification_on_off"), method: .post, parameters: paramiter, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                        //            debugPrint(response)
                        if let json = response.result.value {
                            let dict:NSDictionary = (json as? NSDictionary)!
                            print(dict)
                            
                            let StatusCode = dict.value(forKey: "status") as! Int
                            if StatusCode==200
                            {
                                self.MyProfile()
                                
                            }
                            else if StatusCode==401
                            {
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                let push =  LoginVC()
                                self.navigationController?.pushViewController(push, animated: true)
                                
                                
                                
                                
                            }
                            else
                            {
                                
                                
                                ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                
                            }
                            
                        }
                        else {
                            ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later.")
                            
                        }
                    }
                    
                    
                }
                    
                    
                    
                    
                else if indexPath.row == 4
                {
                    let Push = WorkerAboutVC()
                    self.navigationController?.pushViewController(Push, animated: true)
                    
                }
                else if indexPath.row == 5
                {
                    //  let Push = WorkerProfileVC()
                    // self.navigationController?.pushViewController(Push, animated: true)
                    
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients([""])
                        mail.setMessageBody("", isHTML: false)
                        
                        present(mail, animated: true)
                    } else
                    {
                        // show failure alert
                    }
                    
                }
                
                else if indexPath.row == 6
                 {
                    let Push = TermsAndNewVC()
                    self.navigationController?.pushViewController(Push, animated: true)
                }
                if indexPath.row == 7
                {
                    
                    let uiAlert = UIAlertController(title: "Home Services", message: "Are You Sure Want To Logout?", preferredStyle: UIAlertController.Style.alert)
                    self.present(uiAlert, animated: true, completion: nil)
                    
                    uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        if ApiUtillity.sharedInstance.isReachable()
                        {
                            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
                            self.apiToken = "Bearer \(self.Token)"
                            let headers = ["Vauthtoken":self.apiToken]
                            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
                            
                            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/logout"), method: .get, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                                debugPrint(response)
                                if let json = response.result.value {
                                    let dict:NSDictionary = (json as? NSDictionary)!
                                    
                                    let StatusCode = dict.value(forKey: "status") as! Int
                                    
                                    if StatusCode==200
                                    {
                                        let SuccessDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                        let TempData = NSDictionary()
                                        ApiUtillity.sharedInstance.setUserData(data: TempData)
                                        let Home = LoginVC()
                                        self.navigationController?.pushViewController(Home, animated: true)
                                        
                                        
                                    }
                                    else if StatusCode==401
                                    {
                                        
                                        
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        UserDefaults.standard.set(false, forKey:"loggedin")
                                        
                                        
                                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                                        
                                        
                                        let Home:LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! LoginVC
                                        self.navigationController?.pushViewController(Home, animated: true)
                                        
                                    }
                                        
                                        
                                    else if StatusCode==412
                                    {
                                        
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                        
                                        
                                    }
                                        
                                    else
                                    {
                                        
                                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                                        
                                        
                                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                                        
                                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                                        
                                        
                                    }
                                    
                                }
                                else {
                                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                                }
                            }
                        }
                        else
                        {
                            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
                            return
                        }
                        
                    }))
                    
                    uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                        
                    }))
                    
                }
        }
        
        
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func Btn_Handler_Notifications(_ sender: Any)
    {
        let Push = WorkerNotificationsVC()
        self.navigationController?.pushViewController(Push, animated: true)
        
    }
    
    //MARK:- ALL FUNCTIONS
    
    func MyProfile()
    {
        if ApiUtillity.sharedInstance.isReachable()
        {
            self.Token = ApiUtillity.sharedInstance.getUserData(key: "vAuthToken")
            self.apiToken = "Bearer \(self.Token)"
            let headers = ["Vauthtoken":self.apiToken]
            ApiUtillity.sharedInstance.showSVProgressHUD(text: "Loading...")
            
            Alamofire.request(ApiUtillity.sharedInstance.API(Join: "user/my_profile"), method: .get, parameters: nil, encoding: URLEncoding.default,headers:headers).responseJSON { response in
                debugPrint(response)
                if let json = response.result.value {
                    let dict:NSDictionary = (json as? NSDictionary)!
                    
                    let StatusCode = dict.value(forKey: "status") as! Int
                    
                    if StatusCode==200
                    {
                        let user_Data = dict.value(forKey: "data") as! NSDictionary
                        ApiUtillity.sharedInstance.setUserData(data: user_Data)
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        self.tbl_view.reloadData()
                        
                        
                    }
                    else if StatusCode==401
                    {
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        UserDefaults.standard.set(false, forKey:"loggedin")
                        
                        
                        UserDefaults.standard.removeObject(forKey: "USER_DATA")
                        UserDefaults.standard.removeObject(forKey: "vAuthToken")
                        UserDefaults.standard.removeObject(forKey: "Fb_Login")
                        // UserDefaults.standard.removeObject(forKey: "DEVICE_TOKEN")
                        ApiUtillity.sharedInstance.dismissSVProgressHUD()
                        
                        
                        let Home = LoginVC()
                        self.navigationController?.pushViewController(Home, animated: true)
                        
                    }
                        
                        
                    else if StatusCode==412
                    {
                        
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                        
                    else
                    {
                        
                        let ErrorDic:NSDictionary = dict.value(forKey: "message") as! NSDictionary
                        
                        
                        let ErrorMessage = ErrorDic.value(forKey: "error") as! String
                        
                        ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: ErrorMessage)
                        
                        
                    }
                    
                }
                else {
                    ApiUtillity.sharedInstance.dismissSVProgressHUDWithError(error: "Server Error.Please Try Again Later")
                }
            }
        }
        else
        {
            ApiUtillity.sharedInstance.showErrorMessage(Title:"Home Service", SubTitle: "Please Check Your Internet Conncetion", ForNavigation: self.navigationController!)
            return
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
