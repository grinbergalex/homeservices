//
//  thirdVC.swift
//  HomeService
//
//  Created by jignesh kasundra on 18/10/18.
//  Copyright © 2018 jignesh kasundra. All rights reserved.
//

import UIKit

class thirdVC: UIViewController {

    @IBOutlet weak var View_Status: UIView!
    @IBOutlet weak var btn_for_let_start: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ApiUtillity.sharedInstance.setCornurRadius(obj: btn_for_let_start, cornurRadius: 10, isClipToBound: true, borderColor: "ffffff", borderWidth: 0)

        
        View_Status.backgroundColor = ApiUtillity.sharedInstance.getColorIntoHex(Hex: "9dcce2")
        
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btn_let_start(_ sender: Any)
    {
        let push = LoginVC()
        self.navigationController?.pushViewController(push, animated: true)
    }
    
}
